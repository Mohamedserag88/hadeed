<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'hadeed' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '=)8Q~gQWA{MC7XE;&e7%}$3F=)/TBEJ&[O}!GF85 ZXb@>P*wmF1lf}+U.)[!,T:' );
define( 'SECURE_AUTH_KEY',  'GI53P>eR!`h,:V@O?c;.NP9w}.;JLtb+I<(`)L1SE^(pb^R6)]_o7JicrIf*$wNN' );
define( 'LOGGED_IN_KEY',    '$U61W,IY3=`5k[yz(2~jz04{P#Kd0XRD$c:H.h$X4SH0t5pWmRV*&?e3sx|!?PW>' );
define( 'NONCE_KEY',        '`I4+WC(`>G <}lj~*5%CwDy}Z$Y$gq(,,EN>j`Ph F08^;^QwMwHncMO[k!/{Ztd' );
define( 'AUTH_SALT',        '=p% %F]8`W113$L6Ru>g/a)~Hf,|^U~0:C8pS{25BMB^7 WzAlyO1?vjP]ILroP~' );
define( 'SECURE_AUTH_SALT', 'lVbSn^E*gbMM_z;qdg^8!bY^pVL5>U7NtY)wQ?4A#-J$V&/0H%vG_]LFQy!8<J4M' );
define( 'LOGGED_IN_SALT',   'e}?;CWYk,G`a~:?h?tO}{uHg{0,/GmAVl=kth,By^[[:idqD]bG>cmK<#3<L,XHI' );
define( 'NONCE_SALT',       '^9H<fem*!CwI0~-|U(`,%9aw>tk84CUgB:6h1At+Q/}##lL`uh%XIfNC=Y<_jQY5' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
