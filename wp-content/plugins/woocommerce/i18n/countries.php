<?php
/**
 * Countries
 *
 * Returns an array of countries and codes.
 * Country codes and names should follow the Unicode CLDR recommendation (http://cldr.unicode.org/translation/country-names).
 *
 * @package WooCommerce\i18n
 * @version 3.8.0
 */

defined( 'ABSPATH' ) || exit;

return array(
	
	'EG' => __( 'Egypt', 'woocommerce' ),
	'JO' => __( 'Jordan', 'woocommerce' ),
	'SA' => __( 'Saudi Arabia', 'woocommerce' ),
	'KW' => __( 'Kuwait', 'woocommerce' ),
	'UAE' => __( 'United Arab Emirates', 'woocommerce' ),

);
