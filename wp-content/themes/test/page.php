<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package storefront
 */

get_header(); ?>


<section class="featured-section">
<div class="auto-container">

<div class="">
			<?php
			while ( have_posts() ) :
				the_post();
?>
			
			<div class="col-12">
			<div class="sec-title">
                            <h2><?php the_title(); ?></h2>
						</div>
						</div>
						   <!--Upper Section-->
						   <div class="upper-section">
                    <div class="">

                        <div class="column col-12">
                            <div class="text">
                                <?php the_content(); ?>
								
			</div>
			</div>
			</div>
<?php
			endwhile; // End of the loop.
			?>
			
		</div>	
		</div>	
</section>

<?php
// do_action( 'storefront_sidebar' );
get_footer();
