<?php get_header(); 

while ( have_posts() ) :
    the_post();
    global $product;

    
   
?>


    <div class="container py-5" style="margin-top:5%">
        <div class="row">
            <div class="col-lg-5">
                <?php
                    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $product->id ), 'single-post-thumbnail' );

                ?>
                <img src="<?php echo $image[0]; ?>" id="img_preview" width="100%" style="max-height: 542;max-width: 532;">
                <div class="row">
                    <?php 

                        $attachment_ids = $product->get_gallery_image_ids();

                        foreach( $attachment_ids as $attachment_id ) 
                        {
                            // Display the image URL
                            $Original_image_url = wp_get_attachment_url( $attachment_id );

                            // Display Image instead of URL
                            $fullImage =  wp_get_attachment_image($attachment_id, 'full');
                            ?>

                            <div class="col-lg-3 col-3 pt-2 " >
                                <img src="<?php echo $Original_image_url; ?>" onclick="imgPreview(this.src)"  class="float-right img" width="90%" height="90%" >
                            </div>

                            <?php
                        }

                    ?>
                    
                </div>
            </div>
            <div class="col-lg-7">
                <div>
                    <p>
                        <?php
                            // category;
                            $term_list = wp_get_post_terms($product->id,'product_cat',array('fields'=>'all'));
                            $count = count($term_list);
                            foreach(array_reverse($term_list) as $key => $term){
                                $cat_id = (int)$term->term_id;
                                $cat_name= (string)$term->name;
                                ?>
                                    <a href="<?php echo get_term_link ($cat_id, 'product_cat');?>" class="bankGothFont text-dark"><?php echo $cat_name; ?></a>  
                                <?php
                                if($key != $count-1)
                                    echo " > ";
                            } wp_reset_query();
                        
                        ?>                       
                    </p>
                    <div class="row">
                        <div class="col-lg-8">
                            <h3 class="bankGothFont"><?php the_title(); ?></h3>
                            <p class="bankGothFont text-danger pt-3"><img src="<?php bloginfo('template_url'); ?>/images/ambulance.svg"> FREE NEXT DAY DELIVERY  </p>
                        </div>
                        <div class="col-lg-4">
                        
                                <?php if( $product->is_on_sale() && !empty($product->get_sale_price()) && !empty($product->get_regular_price())) { ?>

                                <h1 class="text-danger font-weight-bolder" style="font-family: square;"><?php echo woocommerce_price($product->get_sale_price());?></h1>
                                <h5 class="bankGothFontOpacity">WAS <del> <?php echo woocommerce_price($product->get_regular_price()); ?> </del></h5>
                            
                            <?php }else{ ?>

                                <h1 class="text-danger font-weight-bolder" style="font-family: square;"><?php echo  woocommerce_price($product->get_price());?></h1>

                            <?php } ?>

                        </div>
                    </div><hr>

                    <?php if($product->is_type('variable')){ ?>

                        <h5 class="bankGothFont">SELECT SIZE</h5>
                        <div class="row">
                            <div class="col-lg-9">
                                <input name="size" id="size" type="hidden">
                                <ul class="list-group pt-2 selecSize list-group-horizontal border-0">
                                    <?php 
                                        $sizes = ['large' => 'L'];
                                        foreach( $product->get_variation_attributes() as $taxonomy => $term_slugs ){
                                            $attr_name = get_taxonomy( $taxonomy )->labels->singular_name; // name

                                            if(strtolower($attr_name) == 'size'){
                                                foreach($term_slugs as $key => $term){
                                            ?>
                                                <li id="<?php echo $term; ?>" onclick="activeSize(event)" class="list-group-item bankGothFont"><?php echo $term; ?></li>

                                            <?php
                                                }
                                            }
                                        }
                                    ?>
                                </ul>
                            </div>
                            <div class="col-lg-3">
                                <h5 class="bankGothFont pt-4">size chart</h5>
                            </div>
                        </div>

                        <hr>

                        <h5 class="bankGothFont">SELECT COLOR</h5>
                        <div class="row">
                            <div class="col-lg-9">
                                <input name="color" id="color" type="hidden">
                                <ul class="list-group selectColor pt-2 list-group-horizontal border-0">
                                    <?php 

                                        foreach( $product->get_variation_attributes() as $taxonomy => $term_slugs ){
                                            $attr_name = get_taxonomy( $taxonomy )->labels->singular_name; // name

                                            if(strtolower($attr_name) == 'color'){
                                                foreach($term_slugs as $term){  
                                            ?>
                                                <li id="<?php echo $term; ?>" onclick="activeColor(event)" class="list-group-item ml-4" style="background-color:<?php echo $term; ?>"></li>

                                            <?php
                                                }
                                            }
                                        }
                                    ?>
                                </ul>
                            </div>
                        </div>
                        <hr>

                    <?php } ?>

                    <h5 class="bankGothFont">PRODUCT DETAILS</h5>
                    <p class="bankGothFontOpacity"><?php echo $product->get_short_description(); ?></p>
                    <div class="row">
                        <div class="col-lg-5">
                            <a href="<?php echo $product->add_to_cart_url(); ?>" class="btn btnContinue btn-block text-left pt-2">Add To Cart <i class="fa fa-long-arrow-alt-right d-flex float-right pt-2"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container pt-4">
        <div class="jumbotron p-0">
            <div class="row">
                <div class="col-lg-7 col-md-7 col-sm-12 col-12">
                    <div class="pt-5 px-5">
                        <h4 class="commit">PRODUCT DETAILS</h4>
                        <p class="pTextShoping pt-4"><?php echo get_the_content(); ?> </p>
                    </div>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-12 col-12">
                    <?php
                        if(get_field('product')['single_product_block_img']){
                            $single_product_block_img = get_field('product')['single_product_block_img'];
                        }else{
                            $single_product_block_img =  get_bloginfo('template_url')."/images/single_product_block_img.png";
                        }
                    ?>
                    <img src="<?php echo $single_product_block_img; ?> "  style="max-height: 486px;max-width: 475px;">
                </div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="jumbotron">
        <?php
            if(get_field('product')['single_product_cover_image']){
                $single_product_cover_image = get_field('product')['single_product_cover_image'];
            }else{
                $single_product_cover_image =  get_bloginfo('template_url')."/images/single_product_cover_image.png";
            }
        ?>
            <img class="d-block mx-auto" 
                src="<?php echo $single_product_cover_image; ?> ">
        </div>
    </div>
    <div class="container pt-5">
        <div class="collection">
            <h4 class="entire">COLLECTION</h4>
            <h3 class="commit">NEW ARRIVALS</h3>
        </div>
        <div class="swiper-container container pb-5 pt-5"><br>
            <div class="swiper-wrapper">
            <?php
                $args = array(
                    'posts_per_page'   => 10,
                    'orderby'          => 'date',
                    'order'            => 'DESC',
                    'post_type'        => 'product'
                ); 

                $random_products = get_posts( $args );

                foreach ( $random_products as $post ) : setup_postdata( $post ); 
                global $product;
                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $product->id ), 'single-post-thumbnail' );
            ?>
                <div class="swiper-slide">
                    <div class="row py-3 justify-content-around">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12 pt-3">
                            <div class="text-center newArrival">
                                <img src="<?php echo $image[0]; ?>" class="mx-auto d-block" height="105px">
                                <?php if ( $product->is_on_sale() ){?>
                                    <p class="top-right">SALE</p>
                                <?php } ?>
                                <p class="commit text-center mb-0"><?php the_title(); ?></p>
                                <a href="<?php the_permalink(); ?>" class="text-danger text-center salary"><?php echo woocommerce_price($product->get_price()); ?></a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach; 
                wp_reset_postdata();
                ?>
                <div class="swiper-slide">
                    <div class="row py-3 justify-content-around">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12 pt-3">
                            <div class="text-center newArrival">
                                <img src="<?php bloginfo('template_url'); ?>/images/shortLg.png" class="mx-auto d-block" height="105px">
                                <p class="top-right">NEW</p>
                                <p class="commit text-center mb-0">men’s workout tee fit</p>
                                <a href="#" class="text-danger text-center salary">795 EGP</a>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="row py-3 justify-content-around">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12 pt-3">
                            <div class="text-center newArrival">
                                <img src="<?php bloginfo('template_url'); ?>/images/shorts.png" class="mx-auto d-block" height="105px">
                                <p class="top-right">NEW</p>
                                <p class="commit text-center mb-0">men’s workout tee fit</p>
                                <a href="#" class="text-danger text-center salary">795 EGP</a>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="row py-3 justify-content-around">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12 pt-3">
                            <div class="text-center newArrival">
                                <img src="<?php bloginfo('template_url'); ?>/images/shose.png" class="mx-auto d-block" height="105px">
                                <p class="top-right">NEW</p>
                                <p class="commit text-center mb-0">men’s workout tee fit</p>
                                <a href="#" class="text-danger text-center salary">795 EGP</a>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="row py-3 justify-content-around">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12 pt-3">
                            <div class="text-center newArrival">
                                <img src="<?php bloginfo('template_url'); ?>/images/Tshirt.png" class="mx-auto d-block" height="105px">
                                <p class="top-right">NEW</p>
                                <p class="commit text-center mb-0">men’s workout tee fit</p>
                                <a href="#" class="text-danger text-center salary">795 EGP</a>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="row py-3 justify-content-around">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12 pt-3">
                            <div class="text-center newArrival">
                                <img src="<?php bloginfo('template_url'); ?>/images/skirt.png" class="mx-auto d-block" height="105px">
                                <p class="top-right">NEW</p>
                                <p class="commit text-center mb-0">men’s workout tee fit</p>
                                <a href="#" class="text-danger text-center salary">795 EGP</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="row py-3 justify-content-around">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12 pt-3">
                            <div class="text-center newArrival">
                                <img src="<?php bloginfo('template_url'); ?>/images/shortLg.png" class="mx-auto d-block" height="105px">
                                <p class="top-right">NEW</p>
                                <p class="commit text-center mb-0">men’s workout tee fit</p>
                                <a href="#" class="text-danger text-center salary">795 EGP</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Add Pagination -->
            <div class="swiper-pagination">
                <img src="<?php bloginfo('template_url'); ?>/images/next.svg">
            </div>
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
        </div><br>
	</div>
<?php
    endwhile; wp_reset_query();

get_footer();
?>
<script>
        $(window).on("scroll", function () {
            if ($(window).scrollTop() > 20) {
                $(".header").addClass("active");
                // $(".changeLogo").addA
                $('.navbar .brandShop img').attr('src', templateUrl+'images/Artboardcopy5.png');
                $('.nav-link').css("color", "black");
            } else {
                //remove the background property so it comes transparent again (defined in your css)
                $(".header").removeClass("active");
                $('.navbar .brandShop img').attr('src', templateUrl+'images/hadidRed.png');
                $('.nav-link').css("color", "white");
            }
        });

        function imgPreview(img) {
                        
            $('#img_preview').attr('src', img);
        }

        function activeColor(evt){
            $(".bg-selectedC").removeClass("bg-selectedC");
            $(evt.target).addClass("bg-selectedC");                
            $("#color").val(evt.target.innerText);
        }

        function activeSize(evt){
            $(".bg-selected").removeClass("bg-selected");
            $(evt.target).addClass("bg-selected");                
            $("#size").val(evt.target.innerText);
        }

        
    </script>