<?php

add_action( 'phpmailer_init', 'send_smtp_email' );
function send_smtp_email( $phpmailer ) {
    $phpmailer->isSMTP(); //switch to smtp
    $phpmailer->Host = 'pencilsolutions.com';
    $phpmailer->SMTPAuth = true;
    $phpmailer->Port = '465';
    $phpmailer->Username = 'support@pencilproduction.com';
    $phpmailer->Password = 'pencil@2020';
    $phpmailer->SMTPSecure = 'ssl';
    $phpmailer->From = 'contact@pencilsolutions.com';
    $phpmailer->FromName='Pencil Solutions';
}


if (function_exists('add_theme_support'))
    add_theme_support('post-thumbnails');

    add_image_size( 'custom-size', 300, 300, true ); //resize, crop in functions.php

function custom_excerpt_more($more) {
    return ' ... ';
}

add_filter('excerpt_more', 'custom_excerpt_more');


if (function_exists('register_nav_menus')) {
    register_nav_menus(
            array(
                'main-menu' => 'Main Menu',
            )
    );
}

//register settings
function theme_settings_init() {
    register_setting('theme_settings', 'theme_settings');
}

//add settings page to menu
function add_settings_page() {
    add_menu_page(__('Theme Settings'), __('Contact Info'), 'manage_options', 'settings', 'theme_settings_page');
}

//add actions
add_action('admin_init', 'theme_settings_init');
add_action('admin_menu', 'add_settings_page');

//define your variables
$color_scheme = get_the_category();

//start settings page
function theme_settings_page() {

    if (!isset($_REQUEST['updated']))
        $_REQUEST['updated'] = false;

//get variables outside scope
    global $color_scheme;
    ?>

    <div>

        <div id="icon-options-general"></div>
        <h2><?php _e('Contact Info') //your admin panel title         ?></h2>

        <?php
//show saved options message
        if (false !== $_REQUEST['updated']) :
            ?>
            <div><p><strong><?php _e('Options saved'); ?></strong></p></div>
        <?php endif; ?>

        <form method="post" action="options.php">

            <?php settings_fields('theme_settings'); ?>
            <?php $options = get_option('theme_settings'); ?>

            <table style="border-collapse:separate; border-spacing: 0 1em;">

                <tr valign="top">
                    <th scope="row"><?php _e('Call time'); ?></th>
                    <td><input id="theme_settings[calltime]" type="text" size="36" name="theme_settings[calltime]" value="<?php esc_attr_e($options['calltime']); ?>" />
                        <label for="theme_settings[calltime]"><?php _e(''); ?></label></td>   
                </tr>
                
                <tr valign="top">
                    <th scope="row"><?php _e('Phone'); ?></th>
                    <td><input id="theme_settings[phone]" type="text" size="36" name="theme_settings[phone]" value="<?php esc_attr_e($options['phone']); ?>" />
                        <label for="theme_settings[phone]"><?php _e(''); ?></label></td>
                </tr>
                
                 <tr valign="top">
                    <th scope="row"><?php _e('Whatsapp'); ?></th>
                    <td><input id="theme_settings[whatsapp]" type="text" size="36" name="theme_settings[whatsapp]" value="<?php esc_attr_e($options['whatsapp']); ?>" />
                        <label for="theme_settings[whatsapp]"><?php _e(''); ?></label></td>
                </tr>

                <tr valign="top">
                    <th scope="row"><?php _e('Facebook'); ?></th>
                    <td><input id="theme_settings[facebook]" type="text" size="36" name="theme_settings[facebook]" value="<?php esc_attr_e($options['facebook']); ?>" />
                        <label for="theme_settings[facebook]"><?php _e(''); ?></label></td>
                </tr>

                <tr valign="top">
                    <th scope="row"><?php _e('Twitter'); ?></th>
                    <td><input id="theme_settings[twitter]" type="text" size="36" name="theme_settings[twitter]" value="<?php esc_attr_e($options['twitter']); ?>" />
                        <label for="theme_settings[twitter]"><?php _e(''); ?></label></td>
                </tr>

                <tr valign="top">
                    <th scope="row"><?php _e('Youtube'); ?></th>
                    <td><input id="theme_settings[youtube]" type="text" size="36" name="theme_settings[youtube]" value="<?php esc_attr_e($options['youtube']); ?>" />
                        <label for="theme_settings[youtube]"><?php _e(''); ?></label></td>
                </tr>

            </table>

            <p><input name="submit" id="submit" value="Save Changes" type="submit"></p>
        </form>

    </div><!-- END wrap -->

    <?php
}

add_post_type_support( 'page', 'excerpt' );


function cc_mime_types($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
  }
  add_filter('upload_mimes', 'cc_mime_types');

  
  // Limit except length to 125 characters.
// tn limited excerpt length by number of characters
function get_excerpt( $count ) {
    $permalink = get_permalink($post->ID);
    $excerpt = get_the_content();
    $excerpt = strip_tags($excerpt);
    $excerpt = substr($excerpt, 0, $count);
    $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
   // $excerpt = '<p>'.$excerpt.'... <a href="'.$permalink.'">Read More</a></p>';
    return $excerpt;
    }

    /**
 * Remove product data tabs
 */
add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );

function woo_remove_product_tabs( $tabs ) {

    unset( $tabs['description'] );      	// Remove the description tab
    unset( $tabs['reviews'] ); 			// Remove the reviews tab
    unset( $tabs['additional_information'] );  	// Remove the additional information tab

    return $tabs;
}


if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", 11);
function my_jquery_enqueue() {
   wp_deregister_script('jquery');
   wp_register_script('jquery', "https://code.jquery.com/jquery-2.2.4.js", false, null);
   wp_enqueue_script('jquery');
}

function child_theme_slug_setup() {
    load_child_theme_textdomain( 'parent-theme-slug', get_stylesheet_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'child_theme_slug_setup' );


/**
 * Change number of related products output
 */ 
function woo_related_products_limit() {
    global $product;
      
      $args['posts_per_page'] = 4;
      return $args;
  }
  add_filter( 'woocommerce_output_related_products_args', 'jk_related_products_args', 20 );
    function jk_related_products_args( $args ) {
      $args['posts_per_page'] = 4; // 4 related products
      $args['columns'] = 2; // arranged in 2 columns
      return $args;
  }


  /**
 * Attributes shortcode callback.
 */
function so_39394127_attributes_shortcode( $atts ) {

    global $product;

    if( ! is_object( $product ) || ! $product->has_attributes() ){
        return;
    }

    // parse the shortcode attributes
    $args = shortcode_atts( array(
        'attributes' => array_keys( $product->get_attributes() ), // by default show all attributes
    ), $atts );

    // is pass an attributes param, turn into array
    if( is_string( $args['attributes'] ) ){
        $args['attributes'] = array_map( 'trim', explode( '|' , $args['attributes'] ) );
    }

    // start with a null string because shortcodes need to return not echo a value
    $html = '';

    if( ! empty( $args['attributes'] ) ){

        foreach ( $args['attributes'] as $attribute ) {

            // get the WC-standard attribute taxonomy name
            $taxonomy = strpos( $attribute, 'pa_' ) === false ? wc_attribute_taxonomy_name( $attribute ) : $attribute;

            if( taxonomy_is_product_attribute( $taxonomy ) ){

                // Get the attribute label.
                $attribute_label = wc_attribute_label( $taxonomy );

                // Build the html string with the label followed by a clickable list of terms.
                // heads up that in WC2.7 $product->id needs to be $product->get_id()
                $html .= get_the_term_list( $product->id, $taxonomy, '<li class="bullet-arrow">' . $attribute_label . ': ' , ', ', '</li>' );   
            }

        }

        // if we have anything to display, wrap it in a <ul> for proper markup
        // OR: delete these lines if you only wish to return the <li> elements
        if( $html ){
            $html = '<ul class="product-attributes">' . $html . '</ul>';
        }

    }

    return $html;
}
add_shortcode( 'display_attributes', 'so_39394127_attributes_shortcode' );

add_filter( 'wc_order_is_editable', 'wc_make_processing_orders_editable', 10, 2 );
function wc_make_processing_orders_editable( $is_editable, $order ) {
    if ( $order->get_status() == 'processing' ) {
        $is_editable = true;
    }

    return $is_editable;
}


if( ! function_exists('product_attributes_count') ) {
    function product_attributes_count( $atts, $content = null ) {
        // Attributes
        $atts = shortcode_atts( array(
            'id' => '',
        ), $atts, 'attr_count' );

        $product = wc_get_product($atts['id']);

        // Only for variable products
        if( ! $product->is_type('variable')) return '';

        $results = array();
        // Loop through product attributes for variations for this variable product
        foreach( $product->get_variation_attributes() as $taxonomy => $term_slugs ){
            $attr_name = get_taxonomy( $taxonomy )->labels->singular_name; // name
            $count = count($term_slugs); // terms count
            // Formatting
            $results[] = '<span class="'.$taxonomy.'">' . $attr_name . ': ' . $count . '</span>';
        }
        // Output a coma separated string
        return implode(', ', $results);
    }
    add_shortcode("attr_count", "product_attributes_count");
}


add_action( 'init', 'woocommerce_clear_cart_url' );
function woocommerce_clear_cart_url() {
	if ( isset( $_GET['clear-cart'] ) ) {
		global $woocommerce;
		$woocommerce->cart->empty_cart();
	}
}


