<?php get_header(); 

?>
    <!-- =========================================================== -->
    <div class="ctegoryHead">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 pb-5">
                    <div class="introduction pt-5 pb-5">
                        <h5 class="pt-4">SHOP</h5>
                        <h2 class="pt-3">Shop LIKE A PROFESSIONAL ATHLETE</h2>
                        <p><a href="#">Categories </a> / <a href="#"> ALL </a>/</p>
                        <h6 class="pt-3 pb-3">That’s why we have a wide range of designs – from classic wooden display cabinets to bright blue locker types and slim shoe cabinets – to make room for your things and match your style.</h6>
                    </div><br><br>
                </div>
            </div>
        </div>
    </div>
    <!-- ================================================================ -->
    <div class="jumbotron bg-white mb-0">
        <div class="container">
            <h3 class="commit text-center">OUR BOLDEST COLLECTIONS</h3>
            <p class="entire text-center">your entire life is on the line, try to take it more lightly</p>
            <?php
                $i = 1;
                $args = array(
                        'taxonomy'     => 'product_cat',
                        'orderby'      => 'id',
                        'order'        => 'ASC',
                        'show_count'   => 0,
                        'pad_counts'   => 0,
                        'hierarchical' => 1,
                        'title_li'     => '',
                        'hide_empty'   => 0 ,
                        'parent'
                );
                $all_categories = get_categories( $args );
                
                        
            ?> 
            <div class="row pt-5">
                <div class="col-lg-7 pt-4">
                    <?php 
                    foreach ($all_categories as $key => $cat) { 
                        $thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true ); 
                        $image = wp_get_attachment_url( $thumbnail_id );
                        if($key == 0){
                           
                        ?>
                            <div class="cards2">
                                <a href="<?php echo get_term_link ($cat->term_id, 'product_cat');?>">
                                    <img src="<?php echo $image; ?>" width="100%" height="443px">
                                
                                <div class="bottom-right2 text-left">
                                    <h3 class="commit text-white mb-0"><?php echo $cat->name; ?></h3>
                                    <p class="text-white"><?php echo $cat->description; ?></p>
                                </div>
                                </a>
                            </div>
                        <?php }
                        if($key == 1){ 
                        ?>
                            <div class="cards2 pt-4">
                            <a href="<?php echo get_term_link ($cat->term_id, 'product_cat');?>">
                                <img src="<?php echo $image; ?>" width="100%" height="231px">
                                <div class="bottom-right text-left">
                                    <h3 class="commit text-white mb-0"><?php echo $cat->name; ?></h3>
                                    <p class="text-white"><?php echo $cat->description; ?></p>
                                </div>
                            </a>
                            </div> 
                        <?php } 
                    } ?>
                </div>
                <?php
                foreach ($all_categories as $key => $cat) { 
                    $thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true ); 
                    $image = wp_get_attachment_url( $thumbnail_id );
                
                if($key == 2){  ?>
                <div class="col-lg-5">
                    <div class="cards2 pt-4">
                        <a href="<?php echo get_term_link ($cat->term_id, 'product_cat');?>">

                        <img src="<?php echo $image; ?>" width="100%" height="700px">
                        <div class="bottom-right text-left">
                            <h3 class="commit text-white mb-0"><?php echo $cat->name; ?></h3>
                            <p class="text-white"><?php echo $cat->description; ?></p>
                        </div>
                        </a>
                    </div>
                </div>
                <?php }  } ?>
            </div>
            <div class="row pt-4">
                <?php  
                    foreach ($all_categories as $key => $cat) { 
                    $thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true ); 
                    $image = wp_get_attachment_url( $thumbnail_id );
                        if($key != 0 && $key != 1 && $key != 2 && strtolower($cat->name) != 'others'){
                ?>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-12 pt-2">
                        <div class="cards">
                        <a href="<?php echo get_term_link ($cat->term_id, 'product_cat');?>">

                            <img src="<?php echo $image; ?>" width="100%" height="100%">
                            <div class="bottom-right text-left">
                                <h3 class="commit text-white mb-0"><?php echo $cat->name; ?></h3>
                                <p class="text-white"><?php echo $cat->description; ?></p>
                            </div>
                        </a>
                        </div>
                    </div>
                <?php
                        }
                    } 
                ?>
            </div>
        </div>
    </div>

    <!-- ============================================================================ -->
    <!-- <div class="footer">
    <div class="jumbotron bg-white jumbotronFooter mb-0">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6 pt-3">
                        <div class="">
                            <h6>MEN’S COLLECTION</h6>
                            <p><a href="#">Hoodies</a></p>
                            <p><a href="#">Shirts</a></p>
                            <p><a href="#">Gymwear</a></p>
                            <p><a href="#">Shorts</a></p>
                            <p><a href="#">Equipment</a></p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6 pt-3">
                        <div class="">
                            <h6>WOMEN’S COLLECTION</h6>
                            <p><a href="#">Tank Tops</a></p>
                            <p><a href="#">Shirts</a></p>
                            <p><a href="#">Sweatshirts</a></p>
                            <p><a href="#">Shops</a></p>
                            <p><a href="#">Training Gear</a></p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6 pt-3">
                        <div class="">
                            <h6> ABOUT US </h6>
                            <p><a href="#">Hoodies</a></p>
                            <p><a href="#">Shirts</a></p>
                            <p><a href="#">Gymwear</a></p>
                            <p><a href="#">Shorts</a></p>
                            <p><a href="#">Equipment</a></p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6 pt-3">
                        <div class="">
                            <h6>ATHLETES</h6>
                            <p><a href="#">Tank Tops</a></p>
                            <p><a href="#">Shirts</a></p>
                            <p><a href="#">Sweatshirts</a></p>
                            <p><a href="#">Shops </a></p>
                            <p><a href="#">Training Gear</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   -->
    <div class="container pt-5">
        <div class="collection">
            <h4 class="entire">COLLECTION</h4>
            <h3 class="commit">BEST SELLERS</h3>
        </div>
        <div class="swiper-container container pb-5 pt-5"><br>
            <div class="swiper-wrapper">
            <?php
                $args = array(
                    'posts_per_page'   => 10,
                    'orderby'          => 'date',
                    'order'            => 'DESC',
                    'post_type'        => 'product'
                ); 

                $random_products = get_posts( $args );

                foreach ( $random_products as $post ) : setup_postdata( $post ); 
                global $product;
                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $product->id ), 'single-post-thumbnail' );
            ?>
                <div class="swiper-slide">
                    <div class="row py-3 justify-content-around">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12 pt-3">
                            <div class="text-center newArrival">
                                <img src="<?php echo $image[0]; ?>" class="mx-auto d-block" height="105px">
                                <?php if ( $product->is_on_sale() ){?>
                                    <p class="top-right">SALE</p>
                                <?php } ?>
                                <p class="commit text-center mb-0"><?php the_title(); ?></p>
                                <a href="<?php the_permalink(); ?>" class="text-danger text-center salary"><?php echo woocommerce_price($product->get_price()); ?></a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach; 
                wp_reset_postdata();
                ?>
            </div>
            <!-- Add Pagination -->
            <div class="swiper-pagination">
                <img src="<?php bloginfo('template_url'); ?>/images/next.svg">
            </div>
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
        </div><br>
	</div>

    <!---------------------------- Footer --------------------------------->
    <?php get_footer(); ?>
    <script>
        var swiper = new Swiper('.swiper-container', {
        slidesPerView: 4,
        spaceBetween: 30,
        freeMode: true,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
            },
        breakpoints: {
            // when window width is <= 499px
            320: {
                slidesPerView: 1,
                spaceBetweenSlides: 20
            },
            700:{
                slidesPerView: 2,
                spaceBetweenSlides: 50
            },
            // when window width is <= 999px
            999: {
                slidesPerView: 4,
                spaceBetweenSlides: 30
            }
        }
        });
        $(window).on("scroll", function() {
        if($(window).scrollTop() > 20) {
            $(".header").addClass("active");
            // $(".changeLogo").addA
            $('.chekOutNav .brand2 img').attr('src','<?php bloginfo('template_url'); ?>/images/Artboardcopy5.png');
            $('.nav-link').css("color","black");
            } else {
                //remove the background property so it comes transparent again (defined in your css)
            $(".header").removeClass("active");
            
            $('.navbar .brand2 img').attr('src','<?php bloginfo('template_url'); ?>/images/Artboardcopy5.png');
            
            $('.nav-link').css("color","white");
            }
        });
    </script>

</body>

</html>