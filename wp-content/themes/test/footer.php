
    
    <!---------------------------- Footer --------------------------------->
    <div class="footer">
        <div class="jumbotron jumbotronFooter mb-0">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6 pt-3">
                        <div class="">
                            <h6>MEN’S COLLECTION</h6>
                            <p><a href="#">Hoodies</a></p>
                            <p><a href="#">Shirts</a></p>
                            <p><a href="#">Gymwear</a></p>
                            <p><a href="#">Shorts</a></p>
                            <p><a href="#">Equipment</a></p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6 pt-3">
                        <div class="">
                            <h6>WOMEN’S COLLECTION</h6>
                            <p><a href="#">Tank Tops</a></p>
                            <p><a href="#">Shirts</a></p>
                            <p><a href="#">Sweatshirts</a></p>
                            <p><a href="#">Shops</a></p>
                            <p><a href="#">Training Gear</a></p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6 pt-3">
                        <div class="">
                            <h6> ABOUT US </h6>
                            <p><a href="#">Hoodies</a></p>
                            <p><a href="#">Shirts</a></p>
                            <p><a href="#">Gymwear</a></p>
                            <p><a href="#">Shorts</a></p>
                            <p><a href="#">Equipment</a></p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6 pt-3">
                        <div class="">
                            <h6>ATHLETES</h6>
                            <p><a href="#">Tank Tops</a></p>
                            <p><a href="#">Shirts</a></p>
                            <p><a href="#">Sweatshirts</a></p>
                            <p><a href="#">Shops </a></p>
                            <p><a href="#">Training Gear</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="jumbotron jumbotronFooter2">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 pt-2">
                        <p class="had"> <img src="<?php bloginfo('template_url'); ?>/images/footerLogo.png"> </p>
                    </div>
                    <div class="col-lg-2 pt-3">
                        <p><a href="#" class="footLink">MEN’S COLLECTION</a></p>
                    </div>
                    <div class="col-lg-2 pt-3">
                        <p><a href="#" class="footLink">WOMEN’S COLLECTION</a></p>
                    </div>
                    <div class="col-lg-2 pt-3">
                        <p class="text-center"><a href="#" class="footLink">ABOUT US</a></p>
                    </div>
                    <div class="col-lg-2 pt-3">
                        <p><a href="#" class="footLink">SHOP NOW</a></p>
                    </div>
                    <div class="col-lg-2 pt-1">
                        <a href="#" class="shopNow d-flex justify-content-center">SHOP NOW</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/package/js/swiper.min.js"></script>
    <!-- <script src="<?php bloginfo('template_url'); ?>/js/file.js"></script> -->
    <script src="<?php bloginfo('template_url'); ?>/js/wow.min.js"></script>
    <script> new WOW().init(); </script>

<?php //wp_footer() ?>

<script>

$(document).ready(function(){
    $('.navbar .navbar-brand img').attr('src',templateUrl+'/images/Artboardcopy5.png');
    $('.navbar .brandShop img').attr('src',templateUrl+'/images/hadid-logo-black.svg');
    $('.navbar-nav .nav-item .nav-link img:first').attr('src',templateUrl+'/images/language-black.svg');
    $('.navbar-nav .nav-item .nav-link .imgShop').attr('src',templateUrl+'/images/ArtboardRed.png');
    $('.navbar').css("box-shadow","0px 0px 6px 0 rgba(3,3,3,1)");
    $('.nav-link').css("color","black");
    $('.navbar').css("background-color","white");



});

function imgPreview(img) {
    
    $('#img_preview').attr('src', img);
}

function activeColor(evt){
    // alert(evt.target.id);
    $("#pa_color").val(evt.target.id);

    $(".bg-selectedC").removeClass("bg-selectedC");
    $(evt.target).addClass("bg-selectedC");                
    $("#color").val(evt.target.innerText);
}

function activeSize(evt){
    // alert(evt.target.innerText);
    $("#pa_size").val(evt.target.innerText);
    $(".bg-selected").removeClass("bg-selected");
    $(evt.target).addClass("bg-selected");                
    $("#size").val(evt.target.innerText);
}

$('.minus-btn').on('click', function (e) {
            e.preventDefault();
            var $this = $(this);
            var $input = $this.closest('div').find('input');
            var value = parseInt($input.val());

            if (value > 1) {
                value = value - 1;
            } else {
                value = 0;
            }

            $input.val(value);

        });

        $('.plus-btn').on('click', function (e) {
            e.preventDefault();
            var $this = $(this);
            var $input = $this.closest('div').find('input');
            var value = parseInt($input.val());

            if (value < 100) {
                value = value + 1;
            } else {
                value = 100;
            }

            $input.val(value);
        });

        $('.like-btn').on('click', function () {
            $(this).toggleClass('is-active');
        });

$(document).ready(function(){

    $('.btnAddress').click(function(){
        // $('.formDisplay').show(300);
        $("#test").show();
    });
});


var timeout;
jQuery( function( $ ) {
	$('.woocommerce').on('change', 'input.qty', function(){
 
		if ( timeout !== undefined ) {
			clearTimeout( timeout );
		}
 
		timeout = setTimeout(function() {
			$("[name='update_cart']").trigger("click");
		}, 1000 ); // 1 second delay, half a second (500) seems comfortable too
 
	});
} );

var timeout;
jQuery( function( $ ) {
	$('.woocommerce').on('change', 'input.qty', function(){
 
		if ( timeout !== undefined ) {
			clearTimeout( timeout );
		}
 
		timeout = setTimeout(function() {
			$("[name='update_cart']").trigger("click");
		}, 1000 ); // 1 second delay, half a second (500) seems comfortable too
 
	});
} );

</script>

</body>


</html>