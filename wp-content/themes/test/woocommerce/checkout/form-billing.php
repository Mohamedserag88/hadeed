<?php
/**
 * Checkout billing information form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-billing.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 * @global WC_Checkout $checkout
 */

defined( 'ABSPATH' ) || exit;
?>
<div class="woocommerce-billing-fields">
	<?php if ( wc_ship_to_billing_address_only() && WC()->cart->needs_shipping() ) : ?>

		<h3><?php esc_html_e( 'Billing &amp; Shipping', 'woocommerce' ); ?></h3>

	<?php else : ?>

		<h3><?php //  esc_html_e( 'Billing details', 'woocommerce' ); ?></h3>

	<?php endif; ?>

	<?php do_action( 'woocommerce_before_checkout_billing_form', $checkout ); ?>

	<div class="woocommerce-billing-fields__field-wrapper">
		<?php
		$fields = $checkout->get_checkout_fields( 'billing' );
		// echo '<pre>';
		// print_r($fields);
		// echo '</pre>';
		// foreach ( $fields as $key => $field ) {
			// woocommerce_form_field( $key, $field, $checkout->get_value( $key ) );
			?>
			<div class="row pt-4 pb-3">
				<div class="col">
					<label for="First"><?php echo $fields['billing_first_name']['label']; ?></label>
					<input type="text" name="billing_first_name" value="<?php echo $checkout->get_value('billing_first_name'); ?>" class="form-control">
				</div>
				<div class="col">
					<label for="First"><?php echo $fields['billing_last_name']['label']; ?></label>
					<input type="text" name="billing_last_name" value="<?php echo $checkout->get_value('billing_last_name'); ?>" class="form-control">
				</div>
			</div>
			<div class="form-group">
				<label><?php echo $fields['billing_address_1']['label']; ?></label>
				<input type="text" name="billing_address_1" value="<?php echo $checkout->get_value('billing_address_1'); ?>" class="form-control">
			</div>
			<div class="form-group">
				<label><?php echo $fields['billing_address_2']['label']; ?></label>
				<input type="text" name="billing_address_2" value="<?php echo $checkout->get_value('billing_address_2'); ?>" class="form-control">
			</div>
			
			<div class="row pt-3">
				<div class="col">
					<label for="First"><?php echo $fields['billing_postcode']['label']; ?></label>
					<div class="input-group mb-3">
						<input type="text" name="billing_postcode" value="<?php echo $checkout->get_value('billing_postcode'); ?>"  class="form-control">
						<div class="input-group-append">
							<span class="input-group-text bg-white" id="basic-addon2">FIND</span>
						</div>
					</div>
				</div>
				<div class="col">
					<label for="First"><?php echo $fields['billing_state']['label']; ?></label>
					<input type="text"  name="billing_state" value="<?php echo $checkout->get_value('billing_state'); ?>" class="form-control">
				</div>
			</div>
			<div class="row">
				<div class="col">
					<label><?php echo $fields['billing_country']['label']; ?></label>
					<select class="form-control" name="billing_country">
						<option disabled selected>Selct Your Country</option>
						<option> Egypt </option>
					</select>
				</div>
				<div class="col">
					<label for="First"><?php echo $fields['billing_city']['label']; ?></label>
					<select class="form-control" name="billing_city">
						<option disabled selected>Selct Your City</option>
						<option> Cairo </option>
					</select>
				</div>
			</div>
			

			<div class="row pt-3">
				<div class="col">
					<label for="First"><?php echo $fields['billing_email']['label']; ?></label>
					<div class="input-group mb-3">
						<input type="email" name="billing_email" value="<?php echo $checkout->get_value('billing_email'); ?>" class="form-control">
						<div class="input-group-append">
							<span class="input-group-text bg-white" id="basic-addon2">FIND</span>
						</div>
					</div>
				</div>
				<div class="col">
					<label for="First"><?php echo $fields['billing_phone']['label']; ?></label>
					<input type="text" name="billing_phone" value="<?php echo $checkout->get_value('billing_phone'); ?>" class="form-control">
				</div>
			</div>

			
		<?php //}
		?>
	</div>

	<?php do_action( 'woocommerce_after_checkout_billing_form', $checkout ); ?>
</div>

<?php if ( ! is_user_logged_in() && $checkout->is_registration_enabled() ) : ?>
	<div class="woocommerce-account-fields">
		<?php if ( ! $checkout->is_registration_required() ) : ?>

			<p class="form-row form-row-wide create-account">
				<label class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox">
					<input class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox" id="createaccount" <?php checked( ( true === $checkout->get_value( 'createaccount' ) || ( true === apply_filters( 'woocommerce_create_account_default_checked', false ) ) ), true ); ?> type="checkbox" name="createaccount" value="1" /> 
					<span><?php esc_html_e( 'Create an account?', 'woocommerce' ); ?></span>
				</label>
			</p>

		<?php endif; ?>

		<?php do_action( 'woocommerce_before_checkout_registration_form', $checkout ); ?>

		<?php if ( $checkout->get_checkout_fields( 'account' ) ) : ?>

			<div class="create-account">
				<?php foreach ( $checkout->get_checkout_fields( 'account' ) as $key => $field ) : ?>
					<?php woocommerce_form_field( $key, $field, $checkout->get_value( $key ) ); ?>
				<?php endforeach; ?>
				<div class="clear"></div>
			</div>

		<?php endif; ?>

		<?php do_action( 'woocommerce_after_checkout_registration_form', $checkout ); ?>
	</div>
<?php endif; ?>

                            
                            
