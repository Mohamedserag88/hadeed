<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// do_action( 'woocommerce_before_checkout_form', $checkout );

// If checkout registration is disabled and not logged in, the user cannot checkout.
if ( ! $checkout->is_registration_enabled() && $checkout->is_registration_required() && ! is_user_logged_in() ) {
	echo esc_html( apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) ) );
	return;
}

?>

<form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">

	<div class="container py-5">
        <div class="row py-5">
            <div class="col-lg-8">
                <ul class="nav nav-pills nav-pills2 pt-5" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="pill" href="#customerInfo">01 CUSTOMER INFO</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="pill" href="#shopInfo"> 02 SHIPPING INFO </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="pill" href="#paymentSelection"> 03 PAYMENT SELECTION </a>
                    </li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content pb-3">
					<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

                    <div id="customerInfo" class="container tab-pane active"><br>
						<h4 class="commit pt-4">Customer Information</h4>
						
						<?php if ( $checkout->get_checkout_fields() ) : ?>
						<?php do_action( 'woocommerce_checkout_billing' ); ?>
                        <?php endif; ?>

                        <div class="row pt-3">
                            <div class="col-sm-6 col-12 pt-3">
                                <a href="#" class="back"><i class="fa fa-long-arrow-alt-left pr-2 mt-1"></i> Return to Shop</a>
                            </div>
                            <div class="col-sm-6 pt-3 col-12">
                                <a data-toggle="pill" href="#shopInfo" class="btn btnContinue btn-block text-left pt-2">CONTINUE <i class="fa fa-long-arrow-alt-right d-flex float-right pt-2"></i></a>
                            </div>
                        </div> 

					</div>
					
					<div id="shopInfo" class="container tab-pane fade"><br>
                        <?php 
                        // print_r
                        // $fields = $checkout->get_checkout_fields( 'shipping' );
                        // echo '<pre>';
                        // print_r($fields);
                        // echo '</pre>';
                        // foreach ( $fields as $key => $field ) {
                        // 	woocommerce_form_field( $key, $field, $checkout->get_value( $key ) );
                        // }
                        ?>
                        <div class="row pt-4 justify-content-around">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-12 pt-3">
                                <div class="card p-3">
                                    <h2><span><i class="fa fa-home"></i></span>
                                        <div class="custom-control custom-radio float-right">
                                            <input type="radio" class="custom-control-input" id="customRadio"
                                                name="example" value="customEx">
                                            <label class="custom-control-label" for="customRadio"></label>
                                        </div>
                                    </h2>
                                    <p class="commit mb-0">Moneer Kilany </p>
                                    <p class="commit mb-0"> Unit 2 Green Mount Retail </p>
                                    <p class="commit mb-0">Park</p>
                                    <p class="commit mb-0">Halifax, HX1 5QN</p>
                                    <p class="commit mb-0"> Tel: 0344 332 5931</p>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-12 pt-3">
                                <div class="card p-3">
                                    <h2><span><i class="fa fa-home"></i></span>
                                        <div class="custom-control custom-radio float-right">
                                            <input type="radio" class="custom-control-input" id="customRadio2"
                                                name="example" value="customEx2">
                                            <label class="custom-control-label" for="customRadio2"></label>
                                        </div>
                                    </h2>
                                    <p class="commit mb-0">Moneer Kilany </p>
                                    <p class="commit mb-0"> Unit 2 Green Mount Retail </p>
                                    <p class="commit mb-0">Park</p>
                                    <p class="commit mb-0">Halifax, HX1 5QN</p>
                                    <p class="commit mb-0"> Tel: 0344 332 5931</p>
                                </div>
                            </div>
                        </div>
                        
						<?php if ( $checkout->get_checkout_fields() ) : ?>
						<?php do_action( 'woocommerce_checkout_shipping' ); ?>
						<?php endif; ?>

						
                        <div class="row pt-4">
                            <div class="col-sm-6 col-12 pt-3">
                                <a href="#" class="back"><i class="fa fa-long-arrow-alt-left pr-2 mt-1"></i> Return to Shop</a>
                            </div>
                            <div class="col-sm-6 col-12 pt-2">
                                <a href="#paymentSelection" class="btn btnContinue btn-block text-left pt-2">CONTINUE <i class="fa fa-long-arrow-alt-right d-flex float-right pt-2"></i></a>
                            </div>
                        </div>
					</div>

					<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>

                    <div id="paymentSelection" class="container tab-pane fade"><br>
                       

                        <?php
                        // echo '<pre>';
                        // print_r(WC()->customer->get_billing_country());
                        // echo '</pre>';
                        // print_r(WC()->payment_gateways->payment_gateways());
                        // print_r(WC()->payment_gateways->get_available_payment_gateways());
                        if ( ! is_ajax() ) {
                            do_action( 'woocommerce_review_order_before_payment' );
                        }
                        if ( WC()->cart->needs_payment() ) : ?>
                                <?php
                                if ( ! empty(WC()->payment_gateways->get_available_payment_gateways() ) ) {
                                    foreach ( WC()->payment_gateways->get_available_payment_gateways() as $gateway ) {
                                        wc_get_template( 'checkout/payment-method.php', array( 'gateway' => $gateway ) );
                                    }
                                 } else {
                                     echo '<div class="alert alert-danger">' . apply_filters( 'woocommerce_no_available_payment_methods_message', WC()->customer->get_billing_country() ? esc_html__( 'Sorry, it seems that there are no available payment methods for your state. Please contact us if you require assistance or wish to make alternate arrangements.', 'woocommerce' ) : esc_html__( 'Please fill in your details above to see available payment methods.', 'woocommerce' ) ) . '</div>'; // @codingStandardsIgnoreLine
                                 }
                                ?>
                        <?php endif; ?>


                       <?php wc_get_template( 'checkout/terms.php' ); ?>

                        <?php do_action( 'woocommerce_review_order_before_submit' ); ?>

                        <?php echo apply_filters( 'woocommerce_order_button_html', '<button type="submit" class="button alt" name="woocommerce_checkout_place_order" id="place_order" value="' . esc_attr( $order_button_text ) . '" data-value="' . esc_attr( $order_button_text ) . '">' . esc_html( $order_button_text ) . '</button>' ); // @codingStandardsIgnoreLine ?>

                        <div class="row pt-3">
                            <div class="col-sm-6 col-12 pt-3">
                                <a href="#" class="back"><i class="fa fa-long-arrow-alt-left pr-2 mt-1"></i> Return to
                                    Shop</a>
                            </div>
                            <div class="col-sm-6 pt-3 col-12">

                                <button type="submit"  class="btn btnContinue btn-block text-left" name="woocommerce_checkout_place_order"
                                 id="place_order" value="' . esc_attr( $order_button_text ) . '" data-value="' . esc_attr( $order_button_text ) . '">
                                 CONTINUE</button>'
                            </div>
                        </div>

                        <?php do_action( 'woocommerce_review_order_after_submit' ); ?>

                        <?php wp_nonce_field( 'woocommerce-process_checkout', 'woocommerce-process-checkout-nonce' ); ?>
                        
                        <?php 
                        if ( ! is_ajax() ) {
                            do_action( 'woocommerce_review_order_after_payment' );
                        }
                        ?>
                           

                        
                    </div>
                </div>
			</div>

			<?php do_action( 'woocommerce_checkout_before_order_review_heading' ); ?>

            <div class="col-lg-4">
                <div class="shopCart mt-5 px-3 py-4">
                    <h4 class="commit">Shopping Cart <span class="badge badge-danger d-flext float-right"><?php echo WC()->cart->get_cart_contents_count(); ?></span></h4><hr>
					
					<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>

					<div id="order_review" class="woocommerce-checkout-review-order">
						<?php  do_action( 'woocommerce_checkout_order_review' ); ?>
					</div>

					<?php  do_action( 'woocommerce_checkout_after_order_review' ); ?>

                </div>
			</div>


        </div>
	</div>
	
</form>

<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>
