<?php
/**
 * Output a single payment method
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/payment-method.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce/Templates
 * @version     3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<!-- <li class="wc_payment_method payment_method_<?php echo esc_attr( $gateway->id ); ?>">
	<input id="payment_method_<?php echo esc_attr( $gateway->id ); ?>" type="radio" class="input-radio" name="payment_method" value="<?php echo esc_attr( $gateway->id ); ?>" <?php checked( $gateway->chosen, true ); ?> data-order_button_text="<?php echo esc_attr( $gateway->order_button_text ); ?>" />

	<label for="payment_method_<?php echo esc_attr( $gateway->id ); ?>">
		<?php echo $gateway->get_title(); /* phpcs:ignore WordPress.XSS.EscapeOutput.OutputNotEscaped */ ?> <?php echo $gateway->get_icon(); /* phpcs:ignore WordPress.XSS.EscapeOutput.OutputNotEscaped */ ?>
	</label>
	<?php if ( $gateway->has_fields() || $gateway->get_description() ) : ?>
		<div class="payment_box payment_method_<?php echo esc_attr( $gateway->id ); ?>" 
		<?php if ( ! $gateway->chosen ) : ?>style="display:none;"
			<?php endif;  ?>>
			<?php $gateway->payment_fields(); ?>
		</div>
	<?php endif; ?>
</li> -->

 						<div class="pl-4 pr-2 py-3" style="border: solid 1px #dde3e8">

                            <div class="custom-control custom-radio">
								<input type="radio" class="custom-control-input" checked <?php checked( $gateway->chosen, true ); ?>   value="<?php echo esc_attr( $gateway->id ); ?>"  name="payment_method"  id="payment_method_<?php echo esc_attr( $gateway->id ); ?>" 
								data-order_button_text="<?php echo esc_attr( $gateway->order_button_text ); ?>">

                                <label class="custom-control-label bankGothFont" for="custRadio">
									<!-- Mohamed’s Credit Card -->
									<?php echo $gateway->get_title(); /* phpcs:ignore WordPress.XSS.EscapeOutput.OutputNotEscaped */ ?> <?php echo $gateway->get_icon(); /* phpcs:ignore WordPress.XSS.EscapeOutput.OutputNotEscaped */ ?>
								</label> 

								<img src="<?php bloginfo('template_url'); ?>/images/visa.PNG" class="float-right">
								<?php if ( $gateway->has_fields() || $gateway->get_description() ) : ?>

								<p class="bankGothFontOpacity" <?php if ( ! $gateway->chosen ) : ?>style="display:none;"
								<?php endif;  ?> >
									<?php $gateway->payment_fields(); ?>
								</p>
								
								<?php endif; ?>

                            </div>

                        </div>
                        <!--<div class="pl-4 pr-2 py-3 mt-3" style="border: solid 1px #dde3e8">

                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="custRadio2" name="payment"
                                    value="credit">
                                <label class="custom-control-label bankGothFont" for="custRadio2"> Credit Card </label>
                                <img src="<?php bloginfo('template_url'); ?>/images/credit.PNG" class="float-right">
                                <p class="bankGothFontOpacity">Safe money transfer using your bank account. Visa,
                                    Maestro, Discover, American Express.</p>
                            </div>

                            <label><small class="bankGothFontOpacity"> CARD NUMBER </small></label>
                            <div class="input-group mb-3 formCheckOut">
                                <input type="text" class="form-control border-right-0"
                                    placeholder="0000 0000 0000 0000">
                                <div class="input-group-append">
                                    <span class="input-group-text bg-white" id="basic-addon2"><i
                                            class="fa fa-credit-card"></i></span>
                                </div>
                            </div>
                            <div class="row pt-3 formCheckOut">
                                <div class="col-md-4 col-12">
                                    <label>NAME ON CARD</label>
                                    <input type="text" class="form-control">
                                </div>
                                <div class="col-md-4 col-6">
                                    <label>EXPIRY DATE</label>
                                    <input type="date" class="form-control">
                                </div>
                                <div class="col-md-4 col-6">
                                    <label>CVV CODE</label>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control border-right-0">
                                        <div class="input-group-append">
                                            <span class="input-group-text bg-white" id="basic-addon2"><i
                                                    class="fa fa-question-circle"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->