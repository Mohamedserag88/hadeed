<?php
/**
 * Review order table
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/review-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.8.0
 */

defined( 'ABSPATH' ) || exit;
?>

	<?php
		do_action( 'woocommerce_review_order_before_cart_contents' );

		foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
			$_product = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );

			if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_checkout_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
				$image = wp_get_attachment_image_src(get_post_thumbnail_id($_product->id), 'single-post-thumbnail');

				?>
				<!-- <a href="<?php echo $_product->get_permalink($cart_item) ?>"></a> -->

				<div class="media  pt-2 pb-2">
						<img src="<?php echo $image[0]; ?>" class="mr-3" width="27%" height="27%">
					
					<div class="media-body">
							<h6 class="mt-1 commit">
								<?php echo apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;'; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
							</h6>
						
						<p>
							<?php echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
							<?php echo apply_filters( 'woocommerce_checkout_cart_item_quantity', ' <strong class="product-quantity">' . sprintf( '&times;&nbsp;%s', $cart_item['quantity'] ) . '</strong>', $cart_item, $cart_item_key ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
							<?php echo wc_get_formatted_cart_item_data( $cart_item ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
						</p>
					</div>
				</div>
				<?php
			}
		}

		do_action( 'woocommerce_review_order_after_cart_contents' );
	?>



<!-- coupon -->
<hr>
<p class="have">Have a Voucher? <span class="float-right"><i class="fa fa-chevron-down"></i></span></p>
<?php foreach ( WC()->cart->get_coupons() as $code => $coupon ) : ?>
		<h6><?php wc_cart_totals_coupon_label( $coupon ); ?></h6>
		<div><?php wc_cart_totals_coupon_html( $coupon ); ?></div>
<?php endforeach; ?>

<!-- subtotal -->
<hr>
<h6 class="subt">
<?php esc_html_e( 'Subtotal', 'woocommerce' ); ?> 
	<span class="float-right">
		<?php wc_cart_totals_subtotal_html(); ?>
	</span>
</h6>

<!-- shipping -->

<?php if ( WC()->cart->needs_shipping() && WC()->cart->show_shipping() ) : ?>
<hr>
<?php do_action( 'woocommerce_review_order_before_shipping' ); ?>

<h6 class="subt pt-2">
	 <span class="">
		<?php wc_cart_totals_shipping_html(); ?>
	</span>
</h6>

<?php do_action( 'woocommerce_review_order_after_shipping' ); ?>

<?php endif; ?>

<!-- fees -->
<?php foreach ( WC()->cart->get_fees() as $fee ) : ?>
<hr>
<h6 class="subt">
<?php echo esc_html( $fee->name ); ?> 
	<span class="float-right">
		<?php wc_cart_totals_fee_html( $fee ); ?>
	</span>
</h6><br>

<?php endforeach; ?>

<!-- tax -->
<?php if ( wc_tax_enabled() && ! WC()->cart->display_prices_including_tax() ) : ?>
	<hr>
	<?php if ( 'itemized' === get_option( 'woocommerce_tax_total_display' ) ) : ?>
		<?php foreach ( WC()->cart->get_tax_totals() as $code => $tax ) : // phpcs:ignore WordPress.WP.GlobalVariablesOverride.OverrideProhibited ?>
			<h6 class="subt">
			<?php echo esc_html( $tax->label ); ?>
				<span class="float-right">
				<?php echo wp_kses_post( $tax->formatted_amount ); ?>
				</span>
			</h6><br>
		<?php endforeach; ?>
	<?php else : ?>
		<h6 class="subt">
			<?php echo esc_html( WC()->countries->tax_or_vat() ); ?>	
				<span class="float-right">
					<?php wc_cart_totals_taxes_total_html(); ?>
				</span>
			</h6><br>
	<?php endif; ?>
<?php endif; ?>

<!-- total -->
<hr>
<?php do_action( 'woocommerce_review_order_before_order_total' ); ?>

	<h6 class="tot"><?php esc_html_e( 'Total', 'woocommerce' ); ?>
	<span class="float-right"><?php wc_cart_totals_order_total_html(); ?></span></h6> 

<?php do_action( 'woocommerce_review_order_after_order_total' ); ?>
