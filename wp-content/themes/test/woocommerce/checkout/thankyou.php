<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;
?>

<div class="woocommerce-order " style="margin-top:17%">

	<?php if ( $order ) :

		do_action( 'woocommerce_before_thankyou', $order->get_id() ); ?>

		<?php if ( $order->has_status( 'failed' ) ) : ?>

			<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed"><?php esc_html_e( 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.', 'woocommerce' ); ?></p>

			<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed-actions">
				<a href="<?php echo esc_url( $order->get_checkout_payment_url() ); ?>" class="button pay"><?php esc_html_e( 'Pay', 'woocommerce' ); ?></a>
				<?php if ( is_user_logged_in() ) : ?>
					<a href="<?php echo esc_url( wc_get_page_permalink( 'myaccount' ) ); ?>" class="button pay"><?php esc_html_e( 'My account', 'woocommerce' ); ?></a>
				<?php endif; ?>
			</p>

		<?php else : ?>

			<div class="card cardOrders mt-5">
				<div class="card-header bg-white py-0">
					<div class="row">
						<div class="col-lg-3 col-sm-4 col-12 border-right">
							<p class="pt-3 pb-1 bankGothFont">ORDER ID: <span class="text-danger"> <?php echo _x('#', 'hash before order number', 'woocommerce') . $order->get_order_number(); ?> </span class=""></p>
						</div>
						<div class="col-lg-3 col-sm-4 col-12">
							<p class="pt-3 pb-1 bankGothFont">PAID BY : <?php echo wp_kses_post( $order->get_payment_method_title() ); ?></p>
						</div>
						<div class="col-lg-6 col-sm-4 col-12 border-left">
							<p class="text-rightT pt-3 pb-1 bankGothFont">DATE: <span class="text-danger"> <?php echo esc_html(wc_format_datetime($order->get_date_created())); ?> </span></p>
						</div>
					</div>
				</div>
				
				<div class="card-body">
					<div class="row">
						<div class="col-lg-6">
							<p class="ml-2 bankGothFontOpacity">ITEMS</p>
							
							<?php
								if (!$order) {
									return;
								}

								$order_items  = $order->get_items(apply_filters('woocommerce_purchase_order_item_types', 'line_item'));                                                    
								foreach ($order_items as $item_id => $item) {
									$product = $item->get_product();
							?>
							<!-- <a href="<?php echo $product_permalink; ?>"> -->
							<div class="media pt-3">
								<img src="<?php echo wp_get_attachment_image_url( $product->get_image_id(), 'full' );?>" class="mr-3" width="13%" height="13%">
								<div class="media-body">

								<?php 
									$qty          = $item->get_quantity();
									$refunded_qty = $order->get_qty_refunded_for_item($item_id);

									if ($refunded_qty) {
										$qty_display = '<del>' . esc_html($qty) . '</del> <ins>' . esc_html($qty - ($refunded_qty * -1)) . '</ins>';
									} else {
										$qty_display = esc_html($qty);
									}
								?>
								
								<h6 class="mt-1 commit"><?php echo  $qty_display ." X " .   $item->get_name(); ?></h6>
								
								
								<p><?php echo $order->get_formatted_line_subtotal($item); ?></p>
								<?php
									// echo apply_filters( 'woocommerce_order_item_name', $product_permalink ? sprintf( '<a href="%s">%s</a>', $product_permalink, $item->get_name() ) : $item->get_name(), $item, $is_visible ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
									// echo apply_filters('woocommerce_order_item_quantity_html', ' <strong class="product-quantity">' . sprintf('&times;&nbsp;%s', $qty_display) . '</strong>', $item); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
								?>
								</div>
							</div>
							<!-- </a> -->

							<?php } ?>
							
						</div>
						<div class="col-lg-3">
							<p class="text-center bankGothFontOpacity">STATUS</p>
							<h5 class="py-5 text-center bankGothFont text-success"><?php echo  $order->get_status(); ?></h5>
						</div>
						<div class="col-lg-3 text-center">
							<p class="bankGothFontOpacity">Payment Method</p> 
							<!-- <h6 class="pt-4 bankGothFont">WEDNESDAY</h6> -->
							<h4 class="bankGothFont"> <?php echo esc_html(wc_format_datetime($order->get_date_created())); ?> </h4>
						</div>
					</div> 
				</div>
				
				<div class="card-footer bg-white">
					<div class="row">
						<div class="col-lg-9">
							<h6 class="pt-3 bankGothFont">ORDER GRAND TOTAL</h6>
						</div>
						<div class="col-lg-3 text-center">
							<p class="mb-0 bankGothFont">TOTAL</p>
							<h3 class="pt-0 bankGothFont">
								<?php  echo $order->get_formatted_order_total(); ?>
							</h3>
						</div>
					</div>
				</div>
			</div>

		<?php endif; ?>

		<?php do_action( 'woocommerce_thankyou_' . $order->get_payment_method(), $order->get_id() ); ?>
		<?php do_action( 'woocommerce_thankyou', $order->get_id() ); ?>

	<?php else : ?>

		<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', esc_html__( 'Thank you. Your order has been received.', 'woocommerce' ), null ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>

	<?php endif; ?>

</div>


		