<!DOCTYPE html>
<html lang="en">

<head>
    <script type="text/javascript">
    var templateUrl = '<?= get_bloginfo("template_url"); ?>';
    </script>
    <?php //wp_head(); ?>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
    <meta name="generator" content="WordPress <?php bloginfo('version'); ?>" />
    <title><?php bloginfo('name'); ?> <?php wp_title(); ?></title>
    <link rel="pingback" href="<?php bloginfo('pingback url'); ?>" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">
    <link rel="icon" type="<?php bloginfo('template_url'); ?>/image/ico" href="<?php bloginfo('template_url'); ?>/images/footer.png">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/package/css/swiper.min.css">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/style.css">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/animate.min.css">
    
    <?php
        if ( get_bloginfo('language')=='ar' )    {
            ?>
               <link href="<?php bloginfo('template_url'); ?>/css/bootstrap-rtl.css" rel="stylesheet">
            <?php

        } else {
            ?>
                <link href="<?php bloginfo('template_url'); ?>/css/bootstrap.css" rel="stylesheet">    
            <?php
        }
    ?> 
</head>

<body>

    <!--------------------- // Header // ---------------------------->
    <div class="container">
    <header>
        <nav class="navbar navbar-expand-lg navbar-light header fixed-top">
            <?php //storefront_site_title_or_logo(); ?>
            <a class="navbar-brand" href="<?php bloginfo('siteurl'); ?>"><img class="changeLogo" src="<?php bloginfo('template_url'); ?>/images/had1.png"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fas fa-align-justify text-dark"></i>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">

                        <?php
                           
                            $term_list = get_terms(['taxonomy' => 'product_cat','order' => 'DESC' , 'orderby' => 'id','hide_empty' => false, 'parent' => 0]);

                            foreach($term_list as $key => $term){
                                if($key < 6 ){
                                $cat_id = (int)$term->term_id;
                                $cat_name= (string)$term->name;
                        ?>
                                <li class="nav-item <?php if($key == 0){ echo 'active'; } ?>">
                                    <a class="nav-link" href="<?php echo get_term_link ($cat_id, 'product_cat');?>"> <?php echo strtoupper($cat_name); ?> <span class="sr-only">(current)</span></a>
                                </li>
                        <?php
                                
                            } } wp_reset_query();
                        
                        ?>
                </ul>
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a href="#" class="nav-link pr-3">
                            <img src="<?php bloginfo('template_url'); ?>/images/language.svg">
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?php echo get_bloginfo('siteurl'); ?>/cart/" class="nav-link">
                            <img class="imgShop" src="<?php bloginfo('template_url'); ?>/images/Artboardwhite.png">
                        </a>
                        <span class="badge badge-danger dangerrr"><?php echo WC()->cart->get_cart_contents_count(); ?></span>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    </div>
    <div class="container-fluid p-0">
        <div id="demo" class="carousel slide carousel-fade" data-ride="carousel">
            <ul class="carousel-indicators">
                <li data-target="#demo" data-slide-to="0" class="active"></li>
                <li data-target="#demo" data-slide-to="1"></li>
            </ul>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="<?php bloginfo('template_url'); ?>/images/bg-main.jpg" class="imgBackSlider">
                    <div class="carousel-caption">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-lg-5 col-md-6">
                                    <img src="<?php bloginfo('template_url'); ?>/images/slider2.jpg" class="imgInnerSlider">
                                    <h1>UNLEASH</h1>
                                    <h2>YOUR INNER BEAST</h2>
                                    <p>
                                        <button class="btn btnShopHeader py-3">SHOP NOW</button>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="<?php bloginfo('template_url'); ?>/images/bg.jpg" class="imgBackSlider">
                    <div class="carousel-caption">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-lg-5 col-md-6">
                                    <img src="<?php bloginfo('template_url'); ?>/images/slider1-.jpg" class="imgInnerSlider imgInnerSlider2" >
                                    <h1>UNLEASH</h1>
                                    <h2>YOUR INNER BEAST</h2>
                                    <p>
                                        <button class="btn btnShopHeader py-3">SHOP NOW</button>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="carousel-item">
                    <img src="<?php bloginfo('template_url'); ?>/images/bg-main.jpg" class="img-fluid" width="100%">
                    <div class="carousel-caption">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-lg-5 col-md-6">
                                    <img src="<?php bloginfo('template_url'); ?>/images/slider2.jpg" class="imgInnerSlider">
                                    <h1>UNLEASH</h1>
                                    <h2>YOUR INNER BEAST</h2>
                                    <p>
                                        <button class="btn btnShopHeader py-3">SHOP NOW</button>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
            </div>
            <a class="carousel-control-prev" href="#demo" data-slide="prev">
                <img src="<?php bloginfo('template_url'); ?>/images/previous.svg">
            </a>
            <a class="carousel-control-next" href="#demo" data-slide="next">
                <img src="<?php bloginfo('template_url'); ?>/images/next.svg">
            </a>
        </div>
    </div>

    <!-- =========================================================== -->

    

    <div class="container pt-5">
        <div class="collection">
            <h4 class="entire">COLLECTION</h4>
            <h3 class="commit">NEW ARRIVALS</h3>
        </div>
        <div class="swiper-container container pb-5 pt-5"><br>
            <div class="swiper-wrapper">
            <?php
                $args = array(
                    'posts_per_page'   => 10,
                    'orderby'          => 'date',
                    'order'            => 'DESC',
                    'post_type'        => 'product'
                ); 

                $random_products = get_posts( $args );

                foreach ( $random_products as $post ) : setup_postdata( $post ); 
                global $product;
                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $product->id ), 'single-post-thumbnail' );
                ?>
                <div class="swiper-slide">
                    <div class="row py-3 justify-content-around">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12 pt-3">
                            <div class="text-center newArrival">
                                <img src="<?php echo $image[0]; ?>" class="mx-auto d-block" height="105px">
                                <?php if ( $product->is_on_sale() ){?>
                                    <p class="top-right">SALE</p>
                                <?php } ?>
                                <p class="commit text-center mb-0"><?php the_title(); ?></p>
                                <a href="<?php the_permalink(); ?>" class="text-danger text-center salary">
                                    <?php echo woocommerce_price($product->get_price()); ?>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; 
            wp_reset_postdata();
            ?>
                
            </div>
            <!-- Add Pagination -->
            <div class="swiper-pagination">
                <img src="<?php bloginfo('template_url'); ?>/images/next.svg">
            </div>
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
        </div><br>
    </div>
    <!-- =============================== category ================================= -->

    <?php
        
        $args = array(
                'taxonomy'     => 'product_cat',
                'orderby'      => 'id',
                'order'        => 'ASC',
                'show_count'   => 0,
                'pad_counts'   => 0,
                'hierarchical' => 1,
                'title_li'     => '',
                'hide_empty'   => 0 
        );
        $all_categories = get_categories( $args );
        
         if(count($all_categories) > 0){       
    ?> 
    
    <div class="jumbotron bg-white mb-0">
        <div class="container">
            <h3 class="commit">PICK THE RIGHT GEAR FOR YOU</h3>
            <p class="entire">your entire life is on the line, try to take it more lightly</p>
            <div class="row pt-4">
            <?php 
            foreach ($all_categories as $key => $cat) { 

                $thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true ); 
                $image = wp_get_attachment_url( $thumbnail_id ); 
                if($key != 0 && $key != 1 && $key != 2 && strtolower($cat->name) != 'others'){
 
                ?>      

                <div class="col-lg-4 col-md-4 col-sm-6 col-12 pt-2">
                <a href="<?php echo get_term_link ($cat->term_id, 'product_cat');?>">
                    <div class="cards">
                        <img src="<?php echo $image; ?>" width="100%" height="100%">
                        <div class="bottom-right">
                            <h3 class="commit text-white text-center mb-0 pr-3"><?php echo $cat->name; ?></h3>
                            <p class="text-white text-center pr-4"><?php echo $cat->description; ?></p>
                        </div>
                    </div></a>
                </div>

                <?php } } ?>
            </div>
            <p class="text-center pt-5">
                <a href="<?php bloginfo('siteurl'); ?>/category" class="btn bg-white shopButton">SHOP ALL CATEGORIES</a>
            </p>
        </div>
    </div>

    <?php }?>
    <!-- ============================================================= -->

    <div class="jumbotron boxShirt">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <h3 class="commit">EXPECT NOTHING BUT THE BEST</h3>
                    <p class="entire">your entire life is on the line, try to take it more lightly</p>
                    <div class="row pt-3">
                        <div class="col-lg-6">
                            <p class="pText">By asking his London shoemaker to make a shorter boot that would be easier
                                to wear with trousers and to switch from polished to waxed calfskin leather, a stylish
                                waterproof boot was created.</p>
                        </div>
                        <div class="col-lg-6">
                            <p class="pText">the goal was to make a shorter boot that would be easier to wear with
                                trousers and to switch from proof boot was created.</p>
                        </div>
                    </div>
                    <div class="row pt-4">
                        <div class="col-lg-3 col-md-4 col-6">
                            <img src="<?php bloginfo('template_url'); ?>/images/check.svg" class="pt-4">
                            <p class="commit pt-2 text-left clear">QUALITY <br> VERIFIED</p>
                        </div>
                        <div class="col-lg-3 col-md-4 col-6">
                            <img src="<?php bloginfo('template_url'); ?>/images/drop.svg" class="pt-4">
                            <p class="commit pt-2 text-left">SWEAT <br> RESISTANT</p>
                        </div>
                        <div class="col-lg-3 col-md-4 col-6">
                            <img src="<?php bloginfo('template_url'); ?>/images/wash.svg" class="pt-4">
                            <p class="commit pt-2 text-left">WASHABLE <br> MATERIAL</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <img src="<?php bloginfo('template_url'); ?>/images/img.svg" class="imgCross"><br>
                    <img src="<?php bloginfo('template_url'); ?>/images/shirts.png" class="imgShirt">
                </div>
            </div>
        </div>
    </div>

    <!-- ============================================================== -->

    <div class="jumbotron bg-white">
        <div class="container">
            <div class="collection">
                <h3 class="commit text-center">OUR BOLDEST COLLECTIONS</h3>
                <p class="entire text-center">your entire life is on the line, try to take it more lightly</p>
            </div>
            <ul class="nav nav-pills pt-4" role="tablist">

            <?php
                $i = 1;
                $args = array(
                        'taxonomy'     => 'product_cat',
                        'orderby'      => 'id',
                        'order'        => 'ASC',
                        'show_count'   => 0,
                        'pad_counts'   => 0,
                        'hierarchical' => 1,
                        'title_li'     => '',
                        'hide_empty'   => 0
                );
                $all_categories = get_categories( $args );
                foreach ($all_categories as $cat) {
                    if($cat->parent != 0 || $cat->slug == 'others'){
                    $category_id = $cat->term_id;
                    ?>
                        <li class="nav-item">
                            <a class="nav-link <?php if($i == 1) echo 'active'; ?>" data-toggle="pill" href="#<?php echo $cat->slug; ?>"><?php echo strtoupper($cat->name); ?></a>
                        </li>
                    <?php
                        $i = $i +1;
                    }
                }
            ?>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content pb-3">
            <?php
                $i = 1;
                foreach ($all_categories as $cat) {
                    if($cat->parent != 0 || $cat->slug == 'others'){
                    $category_id = $cat->term_id;
                    ?>
                <div id="<?php echo $cat->slug; ?>" class="container tab-pane <?php if($i == 1) echo 'active'; ?>"><br>
                    <div class="row py-3 justify-content-around">
                        <?php 
                        $args = [
                            'post_type' => 'product',
                             // 'stock' => 1,
                            'posts_per_page' => 5,
                            'product_cat' => $cat->slug, 
                            'orderby' =>'date',
                            'order' => 'desc' 
                        ];
                        $query = new WP_query($args);
							if($query->have_posts()){
								while($query->have_posts()){
									$query->the_post();
                                    global $product;
                                    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $product->id ), 'single-post-thumbnail' );

                        ?>
                        <div class="col-lg-2 col-md-4 col-sm-6 col-6 pt-3">
                            <div class="text-center newArrival">
                                <img src="<?php echo $image[0]; ?>" class="mx-auto d-block" height="105px">
                                <?php if ( $product->is_on_sale() ){?>
                                    <p class="top-right">SALE</p>
                                <?php } ?>
                                <p class="commit text-center mb-0"><?php the_title(); ?></p>
                                <a href="<?php the_permalink(); ?>" class="text-danger text-center salary"><?php echo  $product->get_price_html(); ?></a>
                            </div>
                        </div>
                        <?php
                                
							} 
							wp_reset_query();
						}
						?>
                    </div>
                </div>
                <?php
                    $i = $i +1;
                    }
                }
            ?>
            </div>
            <p class="text-center pt-5">
                <a href="<?php bloginfo('siteurl'); ?>/category" class="btn bg-white shopButton">SHOP ALL CATEGORIES</a>
            </p>
        </div>
    </div>

    <div class="jumbotron boxCouch mb-0">
        <div class="container">
            <div class="row justify-content-around" dir="rtl">
                <div class="col-lg-2">
                    <img src="<?php bloginfo('template_url'); ?>/images/bitmap.svg" class="imgEdit">
                </div>
                <div class="col-lg-7">
                    <div class="text-center pt-4">
                        <p><img src="<?php bloginfo('template_url'); ?>/images/egypt.png" alt="Egypt"></p>
                        <h2 class="commit pt-2">COMMITTED TO QUALITY</h2>
                        <h6 class="pb-4 brand">PROUD BRAND - QUALITY GYM CLOTHING - MADE IN EGYPT</h6>
                        <button class="btn btnShop text-white">START SHOPPING NOW</button>
                    </div>
                </div>
                <div class="col-lg-3">
                    <img class="couchEdit" src="<?php bloginfo('template_url'); ?>/images/couch.png" height="503px">
                </div>
            </div>
        </div>
    </div>

    <!-- ============================================================================ -->

    <!---------------------------- Footer --------------------------------->
    <div class="footer">
        <div class="jumbotron jumbotronFooter mb-0">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6 pt-3">
                        <div class="">
                            <h6>MEN’S COLLECTION</h6>
                            <p><a href="#">Hoodies</a></p>
                            <p><a href="#">Shirts</a></p>
                            <p><a href="#">Gymwear</a></p>
                            <p><a href="#">Shorts</a></p>
                            <p><a href="#">Equipment</a></p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6 pt-3">
                        <div class="">
                            <h6>WOMEN’S COLLECTION</h6>
                            <p><a href="#">Tank Tops</a></p>
                            <p><a href="#">Shirts</a></p>
                            <p><a href="#">Sweatshirts</a></p>
                            <p><a href="#">Shops</a></p>
                            <p><a href="#">Training Gear</a></p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6 pt-3">
                        <div class="">
                            <h6> ABOUT US </h6>
                            <p><a href="#">Hoodies</a></p>
                            <p><a href="#">Shirts</a></p>
                            <p><a href="#">Gymwear</a></p>
                            <p><a href="#">Shorts</a></p>
                            <p><a href="#">Equipment</a></p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6 pt-3">
                        <div class="">
                            <h6>ATHLETES</h6>
                            <p><a href="#">Tank Tops</a></p>
                            <p><a href="#">Shirts</a></p>
                            <p><a href="#">Sweatshirts</a></p>
                            <p><a href="#">Shops </a></p>
                            <p><a href="#">Training Gear</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="jumbotron jumbotronFooter2">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 pt-2">
                        <p class="had"> <img src="<?php bloginfo('template_url'); ?>/images/footerLogo.png"> </p>
                    </div>
                    <div class="col-lg-2 pt-3">
                        <p><a href="#" class="footLink">MEN’S COLLECTION</a></p>
                    </div>
                    <div class="col-lg-2 pt-3">
                        <p><a href="#" class="footLink">WOMEN’S COLLECTION</a></p>
                    </div>
                    <div class="col-lg-2 pt-3">
                        <p class="text-center"><a href="#" class="footLink">ABOUT US</a></p>
                    </div>
                    <div class="col-lg-2 pt-3">
                        <p><a href="#" class="footLink">SHOP NOW</a></p>
                    </div>
                    <div class="col-lg-2 pt-1">
                        <a href="#" class="shopNow d-flex justify-content-center">SHOP NOW</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.15.0/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/package/js/swiper.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/file.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/wow.min.js"></script>
    <script> new WOW().init(); </script>
    <!-- <script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script> -->

</body>

</html>
<script>
$(window).on("scroll", function() {
if($(window).scrollTop() > 20) {
    $(".header").addClass("active");
    // $(".changeLogo").addA
    $('.navbar .navbar-brand img').attr('src',templateUrl+'/images/Artboardcopy5.png');
    $('.navbar .brandShop img').attr('src',templateUrl+'/images/hadid-logo-black.svg');
    $('.navbar-nav .nav-item .nav-link img:first').attr('src',templateUrl+'/images/language-black.svg');
    $('.navbar-nav .nav-item .nav-link .imgShop').attr('src',templateUrl+'/images/ArtboardRed.png');
    $('.navbar').css("box-shadow","0px 0px 6px 0 rgba(3,3,3,1)");
    $('.nav-link').css("color","black");
    } 
    else if(window.innerWidth <= 780){
        $('.navbar').css("background-color","white");
        $('.nav-link').css("color","black");
        $('.navbar-brand img').attr('src',templateUrl+'/images/Artboardcopy5.png');
        $('.navbar-nav .nav-item .nav-link img:first').attr('src',templateUrl+'/images/language-black.svg');
        $('.navbar-nav .nav-item .nav-link .imgShop').attr('src',templateUrl+'/images/ArtboardRed.png');
    }
    else {
        //remove the background property so it comes transparent again (defined in your css)
    $(".header").removeClass("active");
    $('.navbar .navbar-brand img').attr('src',templateUrl+'/images/had1.png');
    $('.navbar .brandShop img').attr('src',templateUrl+'/images/ad.png');
    $('.navbar-nav .nav-item .nav-link img:first').attr('src',templateUrl+'/images/language.svg');
    $('.navbar-nav .nav-item .nav-link .languageBlack').attr('src',templateUrl+'/images/language-black.svg');
    $('.navbar-nav .nav-item .nav-link .imgShop').attr('src',templateUrl+'/images/Artboardwhite.png');
    $('.nav-link').css("color","white");
    $('.navbar').css("box-shadow","none");
    }
});
</script>
<?php //wp_footer(); ?>
