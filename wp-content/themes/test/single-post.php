<?php
/*
 * Template Name: recipe page
 * Template Post Type: post, page, product
 */
  
 get_header();  ?>

<section class="featured-section">
<div class="auto-container">

<div class="">
			<?php
			while ( have_posts() ) :
				the_post();
?>
			
			<div class="col-12">
			<div class="sec-title">
                            <h2><?php the_title(); ?></h2>
						</div>
						</div>
						
						   <!--Upper Section-->
						   <div class="upper-section">
                    <div class="">

                        <div class="column col-12">
                            <div class="text">
                                <?php the_content(); ?>
								
			</div>
			</div>
			</div>
<?php
			endwhile; // End of the loop.
			?>
			
		</div>	

<div class="clearfix"></div>

		<section class="related products">

<h2><?php _e( 'Related Products', 'parent-theme-slug' ); ?></h2>

<?php 

$posts = get_field('related_products');

if( $posts ): ?>
<div class="row">
<?php foreach( $posts as $p ): // variable must NOT be called $post (IMPORTANT) ?>
 <!--Featured Block-->
 <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12">
			<div class="featured-block recipe">
				<div class="inner-box">
					<div class="lower-content">
						<div class="image">
							<a href="<?php echo get_permalink( $p->ID ); ?>">
							<?php 
								$thumb_id = get_post_thumbnail_id( $p->ID );
								if ( '' != $thumb_id ) {
									$thumb_url  = wp_get_attachment_image_src( $thumb_id, array( 300, 300), true );
									$image      = $thumb_url[0];
								}
							?>
							<img style="width: 50px;height:50px" src="<?php echo $image ?>" class="img-responsive" alt="" />
							<?php // echo the_post_thumbnail( array(300,300), $p->ID ); ?>
						   
							</a>
						</div>
					</div>
					<div class="upper-content">
						<h3>
						 <?php echo get_the_title( $p->ID ); ?>
						</h3>

						<div class="margin-20 text-center">
							<a href="<?php echo get_permalink( $p->ID ); ?>" class="button theme-btn btn-style-one center-block"><?php _e( 'Order Now', 'parent-theme-slug' ); ?></a>
						</div>
					</div>
				</div>
			</div>

			</div>

<?php endforeach; ?>
</div>
<?php endif; ?>


</section>

		</div>	
</section>

<?php
// do_action( 'storefront_sidebar' );
get_footer();
