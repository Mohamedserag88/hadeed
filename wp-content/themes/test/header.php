<!DOCTYPE html>
<html lang="en">

<head>
    <script type="text/javascript">
    var templateUrl = '<?= get_bloginfo("template_url"); ?>';
    </script>
    <?php //wp_head(); ?>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
    <meta name="generator" content="WordPress <?php bloginfo('version'); ?>" />
    <title><?php bloginfo('name'); ?> <?php wp_title(); ?></title>
    <link rel="pingback" href="<?php bloginfo('pingback url'); ?>" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">
    <link rel="icon" type="<?php bloginfo('template_url'); ?>/image/ico" href="<?php bloginfo('template_url'); ?>/<?php bloginfo('template_url'); ?>/images/footer.png">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/package/css/swiper.min.css">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/style.css">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/animate.min.css">
    <?php
    
        if ( get_bloginfo('language')=='ar' )    {
            ?>
               <!-- <link href="<?php bloginfo('template_url'); ?>/css/bootstrap-rtl.css" rel="stylesheet"> -->
            <?php

        } else {
            ?>
                <!-- <link href="<?php bloginfo('template_url'); ?>/css/bootstrap.css" rel="stylesheet">     -->
            <?php
        }
    ?> 
    <style>
        #test{
            display:none;
        }
    .sec-title > h2{
        display: none;
    }
    </style>
</head>

<body>
    <!--------------------- // Header // ---------------------------->
    <div class="container" style="margin-bottom:20px">
        <header>
            <nav class="navbar navbar-expand-lg navbar-light header mb-5 pt-2 sticky-top">
                <a class="navbar-brand brand2" href="<?php bloginfo('siteurl'); ?>"><img class="pt-2" src="<?php bloginfo('template_url'); ?>/images/Artboardcopy5.png"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fas fa-align-justify text-dark"></i>
                </button>
    
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        
                    <?php
                           
                           $term_list = get_terms(['taxonomy' => 'product_cat','order' => 'DESC' , 'orderby' => 'id','hide_empty' => false, 'parent' => 0]);

                           foreach($term_list as $key => $term){
                               if($key < 6 ){
                               $cat_id = (int)$term->term_id;
                               $cat_name= (string)$term->name;
                       ?>
                               <li class="nav-item <?php if($key == 0){ echo 'active'; } ?>">
                                   <a class="nav-link" href="<?php echo get_term_link ($cat_id, 'product_cat');?>"> <?php echo strtoupper($cat_name); ?> <span class="sr-only">(current)</span></a>
                               </li>
                       <?php
                               
                           } } wp_reset_query();
                       
                       ?>
                    </ul>
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a href="#" class="nav-link pr-3">
                                <img src="<?php bloginfo('template_url'); ?>/images/language-black.svg">
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?php echo get_site_url(); ?>/cart/" class="nav-link">
                                <img class="imgShop" src="<?php bloginfo('template_url'); ?>/images/Artboardwhite.png">
                            </a>
                            <span class="badge badge-danger dangerrr"><?php echo WC()->cart->get_cart_contents_count(); ?></span>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
    </div>

    <?php wc_print_notices(); ?>
