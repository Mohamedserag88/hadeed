
            <?php
                //get all categories by parent category name
                function get_product_subcategories_list( $category_slug ){
                    $terms_html = array();
                    $taxonomy = 'product_cat';
                    // Get the product category (parent) WP_Term object
                    $parent = get_term_by( 'slug', $category_slug, $taxonomy );
                    // Get an array of the subcategories IDs (children IDs)
                    $children_ids = get_term_children( $parent->term_id, $taxonomy );
                    // Loop through each children IDs
                    $i = 1;
                    foreach($children_ids as $children_id){
                        $term = get_term( $children_id, $taxonomy ); // WP_Term object
                        $term_link = get_term_link( $term, $taxonomy ); // The term link
                        if ( is_wp_error( $term_link ) ) $term_link = '';
                        // Set in an array the html formated subcategory name/link

                        // category image
                        // $thumb_id = get_woocommerce_term_meta( $term->term_id, 'thumbnail_id', true );
                        // $term_img = wp_get_attachment_url(  $thumb_id );

                        //category name
                        

                        // category description
                        // echo $term->description;

                        // //category link
                        // echo esc_url( $term_link ); 

                        // echo implode( ', ', $terms_html );

                        ?>
                        
                        <li class="nav-item">
                            <a class="nav-link <?php if($i == 1) echo 'active'; ?>" data-toggle="pill" href="#<?php echo $category_slug; ?>"><?php echo strtoupper($term->name); ?></a>
                        </li>
                
                        <?php
                        $i = $i + 1;
                    }
                    ?>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="pill" href="#others"> OTHERS </a>
                    </li>
                    <?php
                }
                get_product_subcategories_list( 'others' ); 

            ?>