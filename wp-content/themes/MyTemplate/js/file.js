var swiper = new Swiper('.swiper-container', {
    slidesPerView: 4,
    // spaceBetween: 15,
    freeMode: true,
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    breakpoints: {
        // when window width is <= 499px
        320: {
            slidesPerView: 1,
            spaceBetweenSlides: 20
        },
        700: {
            slidesPerView: 2,
            spaceBetweenSlides: 50
        },
        // when window width is <= 999px
        999: {
            slidesPerView: 4,
            spaceBetweenSlides: 15
        }
    }
});


// (function () {
//     if (window.innerWidth <= 780) {
//         $('.navbar').css("background-color", "white");
//         $('.nav-link').css("color", "black");
//         $('.navbar-brand img').attr('src', templateUrl + '/images/Artboardcopy5.png');
//         $('.navbar-nav .nav-item .nav-link img:first').attr('src', templateUrl + '/images/language-black.svg');
//         $('.navbar-nav .nav-item .nav-link .imgShop').attr('src', templateUrl + '/images/ArtboardRed.png');
//     }

//     // $(".header").addClass("active");
//     // // $(".changeLogo").addA
//     // $('.navbar .navbar-brand img').attr('src', templateUrl + '/images/Artboardcopy5.png');
//     // $('.navbar .brandShop img').attr('src', templateUrl + '/images/hadid-logo-black.svg');
//     // $('.navbar-nav .nav-item .nav-link img:first').attr('src', templateUrl + '/images/language-black.svg');
//     // $('.navbar-nav .nav-item .nav-link .imgShop').attr('src', templateUrl + '/images/ArtboardRed.png');
//     // $('.navbar').css("box-shadow", "0px 0px 6px 0 rgba(3,3,3,1)");
//     // $('.nav-link').css("color", "black");

// })();

$(window).on("scroll", scrollDownHandler);
scrollDownHandler();


window.setTimeout(function () {
    $(".alert").fadeTo(500, 0).slideUp(500, function () {
        $(this).remove();
    });
}, 2000);

