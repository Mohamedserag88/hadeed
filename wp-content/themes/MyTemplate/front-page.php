<!DOCTYPE html>
<html lang="en">

<head>
    <script type="text/javascript">
    var templateUrl = '<?= get_bloginfo("template_url"); ?>';
    </script>
    <?php wp_head(); ?>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
    <meta name="generator" content="WordPress <?php bloginfo('version'); ?>" />
    <title><?php bloginfo('name'); ?> <?php wp_title(); ?></title>
    <link rel="pingback" href="<?php bloginfo('pingback url'); ?>" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">
    <link rel="icon" type="<?php bloginfo('template_url'); ?>/image/ico" href="<?php bloginfo('template_url'); ?>/images/footer.png">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/package/css/swiper.min.css">
    <link href="https://fonts.googleapis.com/css?family=Cairo:400,700&display=swap" rel="stylesheet">
    <?php 
        if(get_locale() == 'en_AU'){
    ?>
        <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/style.css">

    <?php
        }
        else{
    ?>
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/style_ar.css">

    <?php
        }
    ?>
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/animate.min.css">
    <style>
@font-face {
  font-family: "futura-bold";
  font-style: normal;
  font-weight: normal;
  font-stretch: normal;
  line-height: normal;
  src: url("<?php bloginfo('template_url'); ?>/fonts/FuturaBold.otf");
  src: local("futura-bold"), url("<?php bloginfo('template_url'); ?>/fonts/FuturaBold.otf") format("otf"),
    url("<?php bloginfo('template_url'); ?>/fonts/FuturaBold.otf") format("truetype");
}
@font-face {
  font-family: "futura-medium";
  font-style: normal;
  font-weight: normal;
  font-stretch: normal;
  line-height: normal;
  src: url("<?php bloginfo('template_url'); ?>/fonts/FuturaMedium.otf");
  src: local("futura-medium"), url("<?php bloginfo('template_url'); ?>/fonts/FuturaMedium.otf") format("otf"),
    url("<?php bloginfo('template_url'); ?>/fonts/FuturaMedium.otf") format("truetype");
}
@font-face {
  font-family: "BankGothicMedium";
  font-style: normal;
  font-weight: normal;
  font-stretch: normal;
  line-height: normal;
  src: url("<?php bloginfo('template_url'); ?>/fonts/BankGothicMedium.otf");
  src: local("BankGothicMedium"),
    url("<?php bloginfo('template_url'); ?>/fonts/BankGothicMedium.otf") format("otf"),
    url("<?php bloginfo('template_url'); ?>/fonts/BankGothicMedium.otf") format("truetype");
}
@font-face {
  font-family: "square";
  font-style: normal;
  font-weight: normal;
  font-stretch: normal;
  line-height: normal;
  src: url("<?php bloginfo('template_url'); ?>/fonts/Square.ttf");
  src: local("square"),
    url("<?php bloginfo('template_url'); ?>/fonts/Square.ttf") format("ttf"),
    url("<?php bloginfo('template_url'); ?>/fonts/Square.ttf") format("truetype");
}
</style>
   
</head>

<body>

    <!--------------------- // Header // ---------------------------->
    <div class="container" style="margin-top:-2rem">
    <header>
            <nav class="navbar navbar-expand-lg navbar-light header mb-5  sticky-top">
                <a class="navbar-brand brand2" href="<?php echo get_site_url(); ?>">
                <img src="<?php bloginfo('template_url'); ?>/images/had1.png"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fas fa-align-justify text-dark"></i>
                </button>
    
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav">
                    
                    <?php
                           
                           $term_list = get_terms(['taxonomy' => 'product_cat', 'hide_empty' => false, 'parent' => 0]);

                           foreach($term_list as $key => $term){
                               $cat_id = (int)$term->term_id;
                               $cat_name= (string)$term->name;
                       ?>
                               
                                <li class="nav-item dropdown newDropdown">
                                    <a class="nav-link newDropdowna" href="<?php echo esc_url(get_term_link ($cat_id, 'product_cat'))?>" data-toggle="dropdown">
                                        <?php echo strtoupper($cat_name); ?>
                                    </a>
                                    <div class="dropdown-menu bankGothFont" aria-labelledby="navbarDropdown">
                                    <?php
                           
                                        $chields = get_terms(['taxonomy' => 'product_cat','order' => 'DESC' , 'orderby' => 'id','hide_empty' => false, 'parent' => $cat_id]);

                                        foreach($chields as $key => $chield){
                                            $chield_id = (int)$chield->term_id;
                                            $chield_name= (string)$chield->name;
                                        ?>
                                            <a class="dropdown-item" href="<?php echo esc_url(get_term_link ($chield_id, 'product_cat'))?>"><?php echo strtoupper($chield_name); ?></a>
                                            
                                        <?php
                               
                                            } 
                                        
                                        ?>
                                    </div>
                                </li>
                       <?php
                               
                           } 
                       
                       ?>
                        <!-- <li class="nav-item">
                            <a class="nav-link" href="<?php echo get_site_url()."/category/"; ?>">CATEGORIES</a>
                        </li> -->
                        
                    </ul>
                    <ul class="navbar-nav navArabic">

                        
                        <?php 
                            if(get_locale() == 'en_AU'){
                        ?>
                        <li class="nav-item wg-li weglot-lang weglot-language flag-0 ar" data-code-language="ar">
                            <a data-wg-notranslate href="<?php echo get_site_url()."/ar/"; ?>" class="nav-link">
                                <img src="<?php bloginfo('template_url'); ?>/images/language.svg">
                            </a>
                        </li>
                        <?php
                            }
                            else{
                        ?>
                            <li class="nav-item wg-li weglot-lang weglot-language flag-0 en" data-code-language="en">
                                <a data-wg-notranslate href="<?php echo get_site_url()?>" class="nav-link enlishIcon" >
                                    <img src="<?php bloginfo('template_url'); ?>/images/languageEn.svg">
                                </a>
                            </li>
                        <?php        
                            }
                        ?>

                        <li class="nav-item d-sm-none">
                        <a href="<?php echo get_site_url()."/cart/"; ?> " class="nav-link">
                            <img class="imgShop" src="<?php bloginfo('template_url'); ?>/images/Artboardwhite.png">
                            </a>
                            <span class="badge badge-danger dangerrr"><?php echo WC()->cart->get_cart_contents_count(); ?></span>
                        </li>
                        <li class="nav-item dropdown d-none d-md-block">
                            <a href="#" class="nav-link dropdown-toggle drpDownH" id="navbarDropdown1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img class="imgShop" src="<?php bloginfo('template_url'); ?>/images/Artboardwhite.png">
                            </a>
                            <span class="badge badge-danger dangerrr"><?php echo WC()->cart->get_cart_contents_count(); ?></span>
                            <div class="dropdown-menu  dropdown-menu-right drpDowArabic" style="width: 266px"  aria-labelledby="navbarDropdown1">
                                <h5 class="commit px-3 pt-3">Shopping Cart </h4><hr>
                                <?php 
                                    if(WC()->cart->get_cart_contents_count()){
                                        
                                ?> 
                                        <?php
                                            foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
                                                $_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
                                                $product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );
                                        ?>
                                                <a class="dropdown-item px-2" href="#">
                                                    <div class="media">
                                                        <?php 
                                                            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $product_id ), 'single-post-thumbnail' );
                                                        ?> 
                                                        <img src="<?php echo $image[0]; ?>" class="mr-3" width="27%" height="27%">
                                                        <div class="media-body">
                                                            <h6 class="mt-1 commit text-wrap mb-0"><?php echo $_product->get_name(); ?></h6>
                                                            <p class="bankGothFont mb-0"><?php 
                                                                                            echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key ); // PHPCS: XSS ok.
                                                                                        ?>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    
                                                </a>
                                        <?php
                                            }
                                        ?>
                                        
                                        <h6 class="subt pt-2 px-3 txtMons">Subtotal <span class="float-right"><?php echo WC()->cart->get_cart_subtotal(); ?></span></h6>
                                        <!-- <h6 class="subt pt-2 px-3 txtMons">Shipping <span class="float-right"><?php echo WC()->cart->get_cart_total(); ?></span></h6><hr> -->
                                        <h6 class="tot px-3 font-weight-bold">Total <span class="float-right bankGothFont"><?php echo WC()->cart->get_cart_total(); ?></span></h6>
                                        <p class="px-3 pt-3">
                                            <a class="btn btnContinue  btn-block text-left" href="<?php echo get_permalink( woocommerce_get_page_id( 'checkout' ) )?>">CHECKOUT <i
                                                class="fa fa-long-arrow-alt-right d-flex float-right pt-1"></i></a>
                                        </p>
                                        <p class="text-center text-uppercase font-weight-bold"> <a href="<?php echo esc_url( wc_get_cart_url()); ?>" class="text-dark"> View cart </a></p>
                                <?php
                                    }
                                    else{
                                ?>
                                        <p class="px-3 pt-3">
                                            <a class="btn btnContinue  btn-block text-left" href="<?php echo get_permalink( woocommerce_get_page_id( 'shop' ) )?>">START SHOPPING <i
                                                class="fa fa-long-arrow-alt-right d-flex float-right pt-1"></i></a>
                                        </p>
                                <?php
                                    }
                                ?>
                            </div>
                        </li>
                        <li class="nav-item pt-1">

                        <?php if ( is_user_logged_in() ) { ?>

                            <li class="nav-item dropdown pt-1">
                                <a class="nav-link dropdown-toggle bankGothFont" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="badge badge-secondary pl-2 py-2"><?php echo substr($current_user->display_name, 0 , 1); ?></span> <?php echo $current_user->display_name; ?>
                                </a>
                                <?php
                                if(get_locale() == 'en_AU'){
                                    ?>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="<?php echo get_site_url()."/my-account/"; ?>"><i class="far fa-user-circle pr-2"></i> My Account </a>
                                    <a class="dropdown-item" href="<?php echo get_site_url()."/my-account/edit-address/"; ?>"><i class="fas fa-map-marker-alt pr-2"></i> My Address </a>
                                    <a class="dropdown-item" href="<?php echo get_site_url()."/my-account/orders/"; ?>"><i class="fa fa-cube pr-2"></i> My Orders</a>
                                    <a class="dropdown-item" href="<?php echo esc_url( wc_logout_url() ); ?>"><i class="fa fa-sign-out-alt pr-2"></i> Log Out</a>
                                </div>
                                <?php
                                }
                                else{
                                ?>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="<?php echo get_site_url()."/my-account/"; ?>"><i class="far fa-user-circle pr-2"></i> حسابى </a>
                                    <a class="dropdown-item" href="<?php echo get_site_url()."/my-account/edit-address/"; ?>"><i class="fas fa-map-marker-alt pr-2"></i> العنوان </a>
                                    <a class="dropdown-item" href="<?php echo get_site_url()."/my-account/orders/"; ?>"><i class="fa fa-cube pr-2"></i> طلباتى</a>
                                    <a class="dropdown-item" href="<?php echo esc_url( wc_logout_url() ); ?>"><i class="fa fa-sign-out-alt pr-2"></i> تسجيل الخروج</a>
                                </div>
                                <?php
                                }
                                ?>
                            </li>

                        <?php } else{ ?> 

                            <a class="nav-link" href="<?php echo get_site_url()."/my-account/"; ?>">LOGIN</a>

                        <?php } ?>

                        </li>
                        
                    </ul>

                </div>
            </nav>
        </header>
    </div>
    <div class="container-fluid p-0">
        <div id="demo" class="carousel slide carousel-fade" data-ride="carousel">
            <ul class="carousel-indicators">
                <li data-target="#demo" data-slide-to="0" class="active"></li>
                <li data-target="#demo" data-slide-to="1"></li>
            </ul>
            <div class="carousel-inner">
                <!-- <div class="carousel-item active">
                    <img src="<?php bloginfo('template_url'); ?>/images/bg-main.jpg" class="imgBackSlider">
                    <div class="carousel-caption">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-lg-5 col-md-6">
                                    <img src="<?php bloginfo('template_url'); ?>/images/slider2.jpg" class="imgInnerSlider">
                                    <h1>UNLEASH </h1>
                                    <h2>YOUR INNER BEAST</h2>
                                    <p>
                                        <a href="<?php echo get_site_url()."/shop/"; ?>" class="btn btnShopHeader py-3">SHOP NOW</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
                <div class="carousel-item active">
                    <img src="<?php bloginfo('template_url'); ?>/images/bg3.jpg" class="imgBackSlider">
                    <div class="carousel-caption">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-lg-5 col-md-6">
                                    <!-- <img src="<?php bloginfo('template_url'); ?>/images/slider2.jpg" class="imgInnerSlider">
                                    <h1>UNLEASH </h1>
                                    <h2>YOUR INNER BEAST</h2> -->
                                    <p>
                                        <a href="<?php echo get_site_url()."/shop/"; ?>" class="btn btnShopHeader py-3">SHOP NOW</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="<?php bloginfo('template_url'); ?>/images/bg.jpg" class="imgBackSlider">
                    <div class="carousel-caption">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-lg-5 col-md-6">
                                    <!-- <img src="<?php bloginfo('template_url'); ?>/images/slider1-.jpg" class="imgInnerSlider imgInnerSlider2" >
                                    <h1>UNLEASH</h1>
                                    <h2>YOUR INNER BEAST</h2> -->
                                    <p>
                                        <a href="<?php echo get_site_url()."/shop/"; ?>" class="btn btnShopHeader py-3">SHOP NOW</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- =========================================================== -->

    

    <div class="container pt-5">
        <div class="collection">
            <h4 class="entire">COLLECTION</h4>
            <h3 class="commit">NEW ARRIVALS</h3>
        </div>
        <div class="swiper-container container pb-5 pt-5"><br>
            <div class="swiper-wrapper">
            <?php
                $args = array(
                    'posts_per_page'   => 10,
                    'orderby'          => 'date',
                    'order'            => 'DESC'
                    // 'post__in'            => wc_get_featured_product_ids(),
                ); 

                $random_products = wc_get_featured_product_ids();//get_posts( $args );
                foreach ( $random_products as $post ) : setup_postdata( $post ); 
                global $product;
                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $product->id ), 'single-post-thumbnail' );
                $attachment_ids = $product->get_gallery_image_ids();
                $fullImage =  wp_get_attachment_url($attachment_ids[0], 'full') ? wp_get_attachment_url($attachment_ids[0], 'full'): $image;
                ?>
                <div class="swiper-slide">
                    <div class="row py-3 justify-content-around">
                        <div class="col-lg-11 col-md-12 col-sm-12 col-12 pt-3">
                            <a href="<?php the_permalink(); ?>"> 
                                <div class="text-center newArrival">
                                    <img src="<?php echo $image[0]; ?>" data-swapImage="<?php echo  $fullImage; ?>" class="mx-auto img-fluid d-block swapImage">
                                    <?php if ( $product->is_on_sale() ){?>
                                        <p class="top-right text-white">SALE</p>
                                    <?php } ?>
                                </div>
                            </a>
                            <p class="commit text-center mb-0 pt-3"><?php the_title(); ?></p>
                            <p class="text-center">
                                <a href="<?php the_permalink(); ?>" class="text-danger text-center salary">
                                    <?php // echo  $product->get_price_html(); ?>
                                    <?php echo woocommerce_price($product->get_price()); ?>
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
            <?php endforeach; 
            wp_reset_postdata();
            ?>
                
            </div>
            <!-- Add Pagination -->
            <div class="swiper-pagination">
                <img src="<?php bloginfo('template_url'); ?>/images/next.svg">
            </div>
            <div class="swiper-button-next"><img src="<?php bloginfo('template_url'); ?>/images/right.svg"></div>
            <div class="swiper-button-prev"><img src="<?php bloginfo('template_url'); ?>/images/left.svg"></div>
        </div><br>
    </div>


    <!-- =============================== category ================================= -->

    <?php
        
        $args = array(
                'taxonomy'     => 'product_cat',
                'orderby'      => 'id',
                'order'        => 'ASC',
                'show_count'   => 0,
                'pad_counts'   => 0,
                'hierarchical' => 1,
                'title_li'     => '',
                'hide_empty'   => 0 ,
                'parent'       =>0
        );
        $all_categories = get_categories( $args );
        
         if(count($all_categories) > 0){       
    ?> 
    
    <div class="jumbotron bg-white mb-0">
        <div class="container">
            <h3 class="commit">PICK THE RIGHT GEAR FOR YOU</h3>
            <p class="entire">your entire life is on the line, try to take it more lightly</p>
            <!-- <div class="row pt-5">
                <div class="col-lg-7 pt-4">
                    <?php 
                    foreach ($all_categories as $key => $cat) { 
                        $thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true ); 
                        $image = wp_get_attachment_url( $thumbnail_id );
                        if($key == 0){
                           
                        ?>
                            <div class="cards2">
                                <a href="<?php echo get_term_link ($cat->term_id, 'product_cat');?>">
                                    <img src="<?php echo $image; ?>" class="img-fluid">
                                
                                <div class="bottom-right2 text-left">
                                    <h3 class="commit text-white mb-0"><?php echo $cat->name; ?></h3>
                                    <p class="text-white"><?php echo mb_strimwidth($cat->description, 0, 100, "..."); ?></p>
                                </div>
                                </a>
                            </div>
                        <?php }
                        if($key == 1){ 
                        ?>
                            <div class="cards2 mt-3 pt-4">
                            <a href="<?php echo get_term_link ($cat->term_id, 'product_cat');?>">
                                <img src="<?php echo $image; ?>" class="img-fluid">
                                <div class="bottom-right text-left">
                                    <h3 class="commit text-white mb-0"><?php echo $cat->name; ?></h3>
                                    <p class="text-white"><?php echo mb_strimwidth($cat->description, 0, 100, "..."); ?></p>
                                </div>
                            </a>
                            </div> 
                        <?php } 
                    } ?>
                </div>
                <?php
                foreach ($all_categories as $key => $cat) { 
                    $thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true ); 
                    $image = wp_get_attachment_url( $thumbnail_id );
                
                if($key == 2){  ?>
                <div class="col-lg-5">
                    <div class="cards2 pt-4">
                        <a href="<?php echo get_term_link ($cat->term_id, 'product_cat');?>">

                        <img src="<?php echo $image; ?>" class="img-fluid">
                        <div class="bottom-right text-left">
                            <h3 class="commit text-white mb-0"><?php echo $cat->name; ?></h3>
                            <p class="text-white pr-2"><?php echo mb_strimwidth($cat->description, 0, 100, "..."); ?></p>
                        </div>
                        </a>
                    </div>
                </div>
                <?php }  } ?>
            </div> -->
            <?php
                $args = array(
                        'taxonomy'     => 'product_cat',
                        'orderby'      => 'id',
                        'order'        => 'ASC',
                        'show_count'   => 0,
                        'pad_counts'   => 0,
                        'hierarchical' => 1,
                        'title_li'     => '',
                        'hide_empty'   => 0 ,
                        'parent'       =>0
                );
                $all_categories = get_categories( $args );
                
            ?> 
            <div class="row pt-5">
                <div class="col-lg-7 pt-4">
                    <?php 
                    foreach ($all_categories as $key => $cat) { 
                        $thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true ); 
                        $image = wp_get_attachment_url( $thumbnail_id );
                        if($key == 0){
                           
                        ?>
                            <div class="cards2">
                                <a href="<?php echo get_term_link ($cat->term_id, 'product_cat');?>">
                                    <img src="<?php echo $image; ?>" class="img-fluid">
                                
                                <div class="bottom-right2 text-left">
                                    <h3 class="commit text-white mb-0"><?php echo $cat->name; ?></h3>
                                    <p class="text-white"><?php echo mb_strimwidth($cat->description, 0, 100, "..."); ?></p>
                                </div>
                                </a>
                            </div>
                        <?php }
                        if($key == 1){ 
                        ?>
                            <div class="cards2 mt-2 pt-4">
                            <a href="<?php echo get_term_link ($cat->term_id, 'product_cat');?>">
                                <img src="<?php echo $image; ?>" class="img-fluid">
                                <div class="bottom-right text-left">
                                    <h3 class="commit text-white mb-0"><?php echo $cat->name; ?></h3>
                                    <p class="text-white"><?php echo mb_strimwidth($cat->description, 0, 100, "..."); ?></p>
                                </div>
                            </a>
                            </div> 
                        <?php } 
                    } ?>
                </div>
                <?php
                foreach ($all_categories as $key => $cat) { 
                    $thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true ); 
                    $image = wp_get_attachment_url( $thumbnail_id );
                
                if($key == 2){  ?>
                <div class="col-lg-5">
                    <div class="cards2 pt-4">
                        <a href="<?php echo get_term_link ($cat->term_id, 'product_cat');?>">

                        <img src="<?php echo $image; ?>" class="img-fluid">
                        <div class="bottom-right text-left">
                            <h3 class="commit text-white mb-0"><?php echo $cat->name; ?></h3>
                            <p class="text-white pr-2"><?php echo mb_strimwidth($cat->description, 0, 100, "..."); ?></p>
                        </div>
                        </a>
                    </div>
                </div>
                <?php }  } ?>
            </div>
            <div class="row pt-4">
                <?php  
                    foreach ($all_categories as $key => $cat) { 
                    $thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true ); 
                    $image = wp_get_attachment_url( $thumbnail_id );
                        if($key != 0 && $key != 1 && $key != 2 && strtolower($cat->name) != 'others'){
                ?>



                        <div class="col-lg-12 col-md-12 col-12 pt-2">
                            <div class="cards">
                                <a href="<?php echo get_term_link ($cat->term_id, 'product_cat');?>">
                                    <img src="<?php echo $image; ?>" class="imgFull img-fluid">
                                    <div class="bottom-right text-left">
                                        <h3 class="commit text-white mb-0"><?php echo $cat->name; ?></h3>
                                        <p class="text-white"><?php echo mb_strimwidth($cat->description, 0, 100, "..."); ?></p>
                                    </div>
                                </a> 
                            </div>
                        </div>

                <?php
                        }
                    } 

                ?>
            </div>
            <p class="text-center pt-5">
                <a href="<?php  echo get_site_url() ?>/category" class="btn bg-white shopButton">SHOP ALL CATEGORIES</a>
            </p>
        </div>
    </div>

    <?php }?>
    <!-- ============================================================= -->

    <div class="jumbotron boxShirt">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-8">
                    <h3 class="commit">EXPECT NOTHING BUT THE BEST</h3>
                    <p class="entire">your entire life is on the line, try to take it more lightly</p>
                    <div class="row pt-3">
                        <div class="col-lg-6">
                            <p class="pText">By asking his London shoemaker to make a shorter boot that would be easier
                                to wear with trousers and to switch from polished to waxed calfskin leather, a stylish
                                waterproof boot was created.</p>
                        </div>
                        <div class="col-lg-6">
                            <p class="pText">the goal was to make a shorter boot that would be easier to wear with
                                trousers and to switch from proof boot was created.</p>
                        </div>
                    </div>
                    <div class="row pt-4 txtArabicRight">
                        <div class="col-lg-3 col-md-4 col-6">
                            <img src="<?php bloginfo('template_url'); ?>/images/check.svg" class="pt-4">
                            <p class="commit pt-2 text-left txtArabicRight clear">QUALITY <br> VERIFIED</p>
                        </div>
                        <div class="col-lg-3 col-md-4 col-6">
                            <img src="<?php bloginfo('template_url'); ?>/images/drop.svg" class="pt-4">
                            <p class="commit pt-2 text-left txtArabicRight">SWEAT <br> RESISTANT</p>
                        </div>
                        <div class="col-lg-3 col-md-4 col-6">
                            <img src="<?php bloginfo('template_url'); ?>/images/wash.svg" class="pt-4">
                            <p class="commit pt-2 text-left txtArabicRight">WASHABLE <br> MATERIAL</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <img src="<?php bloginfo('template_url'); ?>/images/img.svg" class="imgCross"><br>
                    <img src="<?php bloginfo('template_url'); ?>/images/shirts.png" class="imgShirt">
                </div>
            </div>
        </div>
    </div>

    <!-- ============================================================== -->

    <div class="jumbotron bg-white">
        <div class="container">
            <div class="collection">
                <h3 class="commit text-center">OUR BOLDEST COLLECTIONS</h3>
                <p class="entire text-center">your entire life is on the line, try to take it more lightly</p>
            </div>
            <ul class="nav nav-pills pt-4" role="tablist">

            <?php
                $i = 1;
                $args = array(
                        'taxonomy'     => 'product_cat',
                        'show_count'   => 0,
                        'pad_counts'   => 0,
                        'hierarchical' => 1,
                        'title_li'     => '',
                        'hide_empty'   => 0,
                        'parent'       =>0
                );
                $all_categories = get_categories( $args );
                foreach ($all_categories as $key => $cat) {
                    $category_id = $cat->term_id;
                    ?>
                        <li class="nav-item">
                            <a class="nav-link <?php if($i == 1) echo 'active'; ?>" data-toggle="pill" href="#<?php echo $cat->slug; ?>"><?php echo strtoupper($cat->name); ?></a>
                        </li>
                    <?php
                    $i = $i +1;
                }
                
            ?>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content pb-3">
            <?php
                $i = 1;
                foreach ($all_categories as $cat) {
                    $category_id = $cat->term_id;
                    ?>
                <div id="<?php echo $cat->slug; ?>" class="container tab-pane <?php if($i == 1) echo 'active'; ?>"><br>
                    <div class="row py-3 justify-content-around">
                        <?php 
                        $args = [
                            'post_type' => 'product',
                            'posts_per_page' => 5,
                            'product_cat' => $cat->slug, 
                            'orderby' =>'date',
                            'order' => 'desc' ,
                            'post__in'            => wc_get_featured_product_ids(),

                        ];
                        $query = new WP_query($args);
							if($query->have_posts()){
								while($query->have_posts()){
									$query->the_post();
                                    global $product;
                                    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $product->id ), 'single-post-thumbnail' );
                                    $attachment_ids = $product->get_gallery_image_ids();
                                    $fullImage =  wp_get_attachment_url($attachment_ids[0], 'full') ? wp_get_attachment_url($attachment_ids[0], 'full'): $image;
                        ?>
                        <div class="col-lg-3 col-md-6 col-sm-12 col-12 pt-3">
                            
                            <a href="<?php the_permalink(); ?>"> 
                                <div class="text-center newArrival">
                                    <img src="<?php echo $image[0]; ?>" data-swapImage="<?php echo  $fullImage; ?>" class="mx-auto img-fluid d-block swapImage">
                                    <?php if ( $product->is_on_sale() ){?>
                                        <p class="top-right text-white">SALE</p>
                                    <?php } ?>
                                </div>
                            </a>
                            <p class="commit text-center mb-0 pt-3"><?php the_title(); ?></p>
                            <p class="text-center">
                                <a href="<?php the_permalink(); ?>" class="text-danger text-center salary">
                                    <?php // echo  $product->get_price_html(); ?>
                                    <?php echo woocommerce_price($product->get_price()); ?>
                                </a>
                            </p>   
                        </div>
                        <?php
                                
							} 
							wp_reset_query();
						}
						?>
                    </div>
                </div>
                <?php
                    $i = $i +1;
                }
            ?>
            </div>
            <p class="text-center pt-5">
                <a href="<?php  echo get_site_url() ?>/category" class="btn bg-white shopButton">SHOP ALL CATEGORIES</a>
            </p>
        </div>
    </div>

    <div class="jumbotron boxCouch mb-0 pr-0 mr-0 pb-0">
        <div class="container-fluid">
            <div class="row justify-content-around" dir="rtl">
                <div class="col-lg-2">
                    <img src="<?php bloginfo('template_url'); ?>/images/bitmap.svg" class="imgEdit">
                </div>
                <div class="col-lg-7">
                    <div class="text-left divQuality">
                        <p><img src="<?php bloginfo('template_url'); ?>/images/egypt.png" alt="Egypt"></p>
                        <h2 class="commit pt-2">COMMITTED TO QUALITY</h2>
                        <h6 class="pb-4 brand" style="font-size:20px">PROUD BRAND - QUALITY GYM CLOTHING - MADE IN EGYPT</h6>
                        <a href="<?php echo get_site_url()."/shop/"; ?>" class="btn btnShop text-white mt-3">START SHOPPING NOW</a>
                    </div>
                    
                </div>
                <div class="col-lg-3">
                    <img class="img-fluid bottom-0 mx-auto d-block pt-4 imgFlipDumb" src="<?php bloginfo('template_url'); ?>/images/guybell.png">
                </div>
            </div>
        </div>
    </div>

    <!-- ============================================================================ -->

    <!---------------------------- Footer --------------------------------->


    <script>
function scrollDownHandler() {
    if ($(window).scrollTop() > 20) {
        console.log("donne")
        $(".header").addClass("active");
        // $(".changeLogo").addA
        $('.navbar .navbar-brand img').attr('src', templateUrl + '/images/Artboardcopy5.png');
        $('.navbar .brandShop img').attr('src', templateUrl + '/images/hadid-logo-black.svg');
        // $('.navbar-nav .nav-item .nav-link img:first').attr('src', templateUrl + '/images/language-black.svg');
        $('.navbar-nav .nav-item .nav-link .imgShop').attr('src', templateUrl + '/images/ArtboardRed.png');
        $('.navbar').css("box-shadow", "0px 0px 6px 0 rgba(3,3,3,1)");
        $('.nav-link').css("color", "black");
    } else {
        //remove the background property so it comes transparent again (defined in your css)
        $(".header").removeClass("active");
        $('.navbar .navbar-brand img').attr('src', templateUrl + '/images/had1.png');
        $('.navbar .brandShop img').attr('src', templateUrl + '/images/ad.png');
        // $('.navbar-nav .nav-item .nav-link img:first').attr('src', templateUrl + '/images/language.svg');
        $('.navbar-nav .nav-item .nav-link .languageBlack').attr('src', templateUrl + '/images/language-black.svg');
        $('.navbar-nav .nav-item .nav-link .imgShop').attr('src', templateUrl + '/images/Artboardwhite.png');
        $('.nav-link').css("color", "white");
        $('.navbar').css("box-shadow", "none");
    }
    if (window.innerWidth <= 780) {
        $('.navbar').css("background-color", "white");
        $('.nav-link').css("color", "black");
        $('.navbar-brand img').attr('src', templateUrl + '/images/Artboardcopy5.png');
        // $('.navbar-nav .nav-item .nav-link img:first').attr('src', templateUrl + '/images/language-black.svg');
        $('.navbar-nav .nav-item .nav-link .imgShop').attr('src', templateUrl + '/images/ArtboardRed.png');
    }
}
    </script>

    <?php
        get_footer( 'shop' );
    ?>

    
    <!-- <div id='fb-root'></div> -->
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    js = d.createElement(s); js.id = id;
    js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js#xfbml=1&version=v2.12&autoLogAppEvents=1';
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
    
  </script>
  <div class='fb-customerchat'
    attribution="wordpress"
    page_id='1411210735802384'
  >
</div>          

    <!-- <script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script> -->

</body>
</html>

