<?php get_header(); ?>
<div class="bankGothFont">
        <?php
			while ( have_posts() ) :
				the_post();
				get_template_part( 'content', 'page' );
			endwhile; // End of the loop.
		?>
</div>
<?php get_footer(); ?>