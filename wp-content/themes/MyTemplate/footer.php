
    
    <!---------------------------- Footer --------------------------------->
    <div class="footer">
        <div class="jumbotron jumbotronFooter mb-0">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 pt-3">
                        <div class="">
                            <p><a href="<?= site_url(); ?>/contact-us/">Contact Us</a></p>
                            <p><a href="<?= site_url(); ?>/about-us/">About Us</a></p>
                            <p><a href="<?= site_url(); ?>/privacy-policy/">Privacy Policy</a></p>
                            <p><a href="<?= site_url(); ?>/our-goals/">Our Goals</a></p>
                            <p><a href="<?= site_url(); ?>/our-first-collection/">Our First Collection</a></p>
                        </div>
                    </div>
                    
                    <div class="col-lg-3 col-md-3 col-sm-12 col-6 pt-3">
                        <div>
                            <?php
                                $term_list = get_terms(['taxonomy' => 'product_cat','order' => 'DESC' , 'orderby' => 'id','hide_empty' => false, 'parent' => 0]);

                           foreach($term_list as $key => $term){
                               $cat_id = (int)$term->term_id;
                               $cat_name= (string)$term->name;
                                echo '<p><a href="'.esc_url(get_term_link ($cat_id, 'product_cat')).'">'.$cat_name.'</a></p>';
                            }
                            ?>

                            
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-12 col-12 pt-3">
                        <div class="row">
                            <div class="socialsIcon">
                                <ul class="nav justify-content-around">
                                    <li class="nav-item">
                                        <a class="nav-link pr-0" href="#"><i class="fab fa-twitter fa-2x"></i></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link pr-2" href="#"><i class="fab fa-facebook fa-2x"></i></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link pr-2" href="#"><i class="fab fa-youtube fa-2x"></i></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link pr-2" href="#"><i class="fab fa-instagram fa-2x"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="footer-newsletter">
                            <?php echo do_shortcode('[newsletter]'); ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="jumbotron jumbotronFooter2">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 pt-2 have">
                        <p class="had"> <a href="<?= site_url(); ?>"><img src="<?php bloginfo('template_url'); ?>/images/footerLogo.png"></a> </p>
                    </div>
                    <div class="col-lg-2 pt-3">
                        <p><a href="<?= site_url(); ?>product-category/men" class="footLink">MEN’S COLLECTION</a></p>
                    </div>
                    <div class="col-lg-2 pt-3">
                        <p><a href="<?= site_url(); ?>product-category/women" class="footLink">WOMEN’S COLLECTION</a></p>
                    </div>
                    <div class="col-lg-2 pt-3">
                        <p class="text-center"><a href="<?= site_url(); ?>/about-us" class="footLink">ABOUT US</a></p>
                    </div>
                    <div class="col-lg-2 pt-1">
                        <a href="<?= site_url(); ?>" class="shopNow d-flex justify-content-center">SHOP NOW</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/package/js/swiper.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/jquery.elevatezoom.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/file.js"></script>


<?php wp_footer() ?>

<script>
$(document).on('mouseenter','.swapImage', function(e){
    e.preventDefault();
    let newImg = $(this).attr('data-swapImage');
    let oldImg = $(this).attr('src');
    $(this).attr('data-swapImage', oldImg);
    $(this).attr('src', newImg);
});
$(document).on('mouseleave','.swapImage', function(e){
    e.preventDefault();
    let newImg = $(this).attr('data-swapImage');
    let oldImg = $(this).attr('src');
    $(this).attr('data-swapImage', oldImg);
    $(this).attr('src', newImg);
});
jQuery(document).ready(function($){
        $( '.updateCart' ).prop( 'disabled', false );
});

var swiper = new Swiper('.swiper-container', {
    slidesPerView: 4,
    // spaceBetween: 15,
    freeMode: true,
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
        },
    breakpoints: {
        // when window width is <= 499px
        320: {
            slidesPerView: 1,
            spaceBetweenSlides: 20
        },
        700:{
            slidesPerView: 2,
            spaceBetweenSlides: 50
        },
        // when window width is <= 999px
        999: {
            slidesPerView: 4,
            spaceBetweenSlides: 15
        }
    }
    });


        



// (function(){
//         $('.navbar .navbar-brand img').attr('src',templateUrl+'/images/Asset2.svg');
//         $('.navbar .brandShop img').attr('src',templateUrl+'/images/hadid-logo-black.svg');
//         $('.navbar-nav .nav-item .nav-link .ArabicIcon img').attr('src',templateUrl+'/images/language-black.svg');
//         $('.navbar-nav .nav-item .englishIcon img').attr('src',templateUrl+'/images/languageEnBlack.svg');
//         $('.navbar-nav .nav-item .nav-link .imgShop').attr('src',templateUrl+'/images/ArtboardRed.png');
//         // $('.navbar').css("box-shadow","0px 0px 6px 0 rgba(3,3,3,1)");
//         $('.nav-link').css("color","black");
//         $('.navbar').css("background-color","white");
//         $('.navbar').css("box-shadow", "none");

//         $(window).on("scroll", function () {
//             if ($(window).scrollTop() > 10) {
//                 $('.navbar').css("box-shadow", "0px 0px 6px 0 rgba(3,3,3,1)");
//             } else {
//                 $('.navbar').css("box-shadow", "none");
//             }
//         });
// })();

function imgPreview(img) {
    var oldimg =$('#img_preview').attr('src');
    $('#img_preview').attr('src', img);
    $('.zoomWindow').css('background-image', 'url("'+img+'")');
    // var ez =   $('.img_preview').data('elevateZoom');	  
    // ez.swaptheimage(oldimg, img ); 
    
}

function activeColor(evt){
    // alert(evt.target.id);
    $("#pa_color").val(evt.target.id);

    $(".bg-selectedC").removeClass("bg-selectedC");
    $(evt.target).addClass("bg-selectedC");                
    $("#color").val(evt.target.innerText);
}

function activeSize(evt){
    // alert(evt.target.innerText);
    $("#pa_size").val(evt.target.innerText);
    $(".bg-selected").removeClass("bg-selected");
    $(evt.target).addClass("bg-selected");                
    $("#size").val(evt.target.innerText);
}

$('.minus-btn').on('click', function (e) {
    e.preventDefault();
    var $this = $(this);
    var $input = $this.closest('div').find('input');
    var value = parseInt($input.val());
    if (value > 1) { value = value - 1;
    } else { value = 0; }
    $input.val(value);
});

$('.plus-btn').on('click', function (e) {
    e.preventDefault();
    var $this = $(this);
    var $input = $this.closest('div').find('input');
    var value = parseInt($input.val());
    if (value < 100) { value = value + 1;
    } else { value = 100; }
    $input.val(value);
});

$('.like-btn').on('click', function () {
    $(this).toggleClass('is-active');
});


$(document).ready(function(){
    $('.btnAddress').click(function(){
        // $('.formDisplay').show(300);
        $("#test").show();
    });
    $('#orderby').on('change', function () {
        $("#formorderby").submit();
    });
});


var timeout;
 
jQuery( function( $ ) {
    $('.quantity input').on('change', function(){
        // $( '.woocommerce-cart-form :input[name="update_cart"]' ).prop( 'disabled', true );
	});
} );

       
    jQuery( function( $ ) {
        $('#form_filter input').on('change', function(){
    
            // alert($(this).val());
            url = updateQueryStringParameter(window.location.href,$(this).attr("name"), $(this).val());
            window.location = url;
    
        });
    } );


    $( function() {
        $( "#slider-range" ).slider({
            range: true,
            min: 1,
            max: 5000,
            values: [ 1, 4000 ],
            slide: function( event, ui ) {
                $( "#amount" ).val( "EGP " + ui.values[ 0 ] + " - EGP " + ui.values[ 1 ] );
              
            },
            stop: function(event, ui) {
                var url;
                url = updateQueryStringParameter(window.location.href,"min_price", ui.values[ 0 ] );
                url = updateQueryStringParameter(url,"max_price", ui.values[ 1 ]);
                window.location = url;
            }
        });

        $("#amount").val( "EGP " + $( "#slider-range" ).slider( "values", 0 ) +
        " - EGP " + $( "#slider-range" ).slider( "values", 1 ) ); 

    });

    $( function() {
        $( "#slider-range2" ).slider({
            range: true,
            min: 1,
            max: 5000,
            values: [ 1, 4000 ],
            slide: function( event, ui ) {
                $( "#amount2" ).val( "EGP " + ui.values[ 0 ] + " - EGP " + ui.values[ 1 ] );
              
            },
            stop: function(event, ui) {
                url = updateQueryStringParameter(window.location.href,"min_price", ui.values[ 0 ] );
                url = updateQueryStringParameter(url,"max_price", ui.values[ 1 ]);
                window.location = url;
            }
        });

        $("#amount2").val( "EGP " + $( "#slider-range2" ).slider( "values", 0 ) +
        " - EGP " + $( "#slider-range2" ).slider( "values", 1 ) ); 

    });



function updateQueryStringParameter(url, key, value) {
    if (!url) {
        uri = window.location.href;
    } else {
        uri = url;
    }

    var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    var separator = uri.indexOf('?') !== -1 ? "&" : "?";
    if(key != 'min_price' || key != 'max_price' ){
        if (uri.match(re)) {
            return uri + "," + value;
        }else {
            return uri + separator + key + "=" + value;
        }
    }else{

        if (uri.match(re)) {
            return uri.replace(re, '$1' + key + "=" + value + '$2');
        } else {
            return uri + separator + key + "=" + value;
        }
    }
}



</script>
<script>
    $('.img_preview').elevateZoom({
        zoomType: "inner",
        cursor: "crosshair",
        zoomWindowFadeIn: 500,
        zoomWindowFadeOut: 750
   }); 
   
//    $("#img_preview").elevateZoom({
//             gallery : "gallery_09",
//             galleryActiveClass: "active"
//     	  		}); 
            
  
//     $("#select").change(function(e){
//    var currentValue = $("#select").val();
  
	// Example of using Active Gallery
//   $('#gallery_09 a').removeClass('active').eq(currentValue-1).addClass('active');		
 
    // });
    window.setTimeout(function() {
        $(".alert").fadeTo(500, 0).slideUp(500, function(){
            $(this).remove(); 
        });
    }, 2000);

   
$(document).ready(function(){
    $('.newDropdowna,.dropdown-item').on('click',function(){
        console.log("clicked")
        let link = $(this).attr("href");
        window.location.replace(link);
    })
    $('.newDropdown').on('mouseenter',function(){
        $(this).find('.dropdown-menu').dropdown("show");
    })
    $('.newDropdown').on('mouseleave',function(){
        $(this).find('.dropdown-menu').dropdown("hide");
    })
})

</script>


<div id='fb-root'></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    js = d.createElement(s); js.id = id;
    js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js#xfbml=1&version=v2.12&autoLogAppEvents=1';
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>
  <div class='fb-customerchat'
    attribution="wordpress"
    page_id='102701601377134'
    theme_color='#fa3c4c'
    logged_in_greeting='Hi! How can we help you?'
    logged_out_greeting='Hi! How can we help you?'
  >
</div>


</body>


</html>