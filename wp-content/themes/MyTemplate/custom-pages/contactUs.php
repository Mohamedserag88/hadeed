<div class="container my-5 logArabic">
    <div class="row pt-5 justify-content-around">
        <div class="col-lg-5 wow fadeIn">
            <h5 class="bankGothFont">GET IN TOUCH</h5>
            <form class="formCheckOut pt-2" method="post">

                <div class="form-group">
                    <label>Address:</label>
                    <p> 835, Industrial Zone, Third Settlement, New Cairo, Egypt </p>
                </div>
                <div class="form-group">
                    <label>Phone:</label>
                    <p> +202-25724666 - +202-25724777 </p>
                </div>
                <div class="form-group">
                    <label>Mobile:</label>
                    <p> +201558808016 - : +201558808017 </p>
                </div>
                <div class="form-group">
                    <label>Email:</label>
                    <p> info@hadidx.com </p>
                </div>
                <div class="form-group">
                    <label>Opening Time:</label>
                    <p> From Saturday to Thursday. Open: 9:00 AM – Close: 5:00 PM </p>
                </div>

            </form>
        </div>
        <div class="col-lg-5">
            <img src="<?php bloginfo('template_url'); ?>/images/imgLogin.jpg" class="img-fluid wow fadeIn">
        </div>
    </div>
</div>