<div class="container my-5 logArabic">
    <div class="row pt-5 justify-content-around">
        <div class="col-lg-5 wow fadeIn">
            <h5 class="bankGothFont">About us – Who We Are</h5>
            <form class="formCheckOut pt-2" method="post">

                <div class="form-group">
                    <label>EXPECT NOTHING BUT THE BEST</label>
                    <p>
                        We Are an Arabic brand that use the finest material to craft a very comfortable yet trendy
                        sportswear with the highest technologies.
                        <br />
                        We have been keen to manufacture high quality products with reasonable price to everyone, so
                        that we can always keep being your first choice.
                    </p>
                </div>

            </form>
        </div>
        <div class="col-lg-5">
            <img src="<?php bloginfo('template_url'); ?>/images/imgLogin.jpg" class="img-fluid wow fadeIn">
        </div>
    </div>
</div>