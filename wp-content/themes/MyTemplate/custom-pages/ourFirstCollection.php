<div class="container my-5 logArabic">
    <div class="row pt-5 justify-content-around">
        <div class="col-lg-5 wow fadeIn">
            <h5 class="bankGothFont">Our First Collection</h5>
            <form class="formCheckOut pt-2" method="post">

                <div class="form-group">
                    <label>Workout With Style</label>
                    <p>
                        The first collection from HadidxHadid tailored carefully to match your needs.
                        Comfortable material & stylish designs.
                        We use the finest textures from cotton & polyester to reach the quintessence of the best
                        sportive outfits.
                        Each material is providing extraordinary experience by giving the feeling of comfort while
                        working out to keep you on the track of reaching a healthy fit body.
                    </p>
                </div>

            </form>
        </div>
        <div class="col-lg-5">
            <img src="<?php bloginfo('template_url'); ?>/images/imgLogin.jpg" class="img-fluid wow fadeIn">
        </div>
    </div>
</div>