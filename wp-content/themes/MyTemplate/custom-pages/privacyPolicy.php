<div class="container my-5 logArabic">
    <div class="row pt-5 justify-content-around">
        <div class="col-lg-5 wow fadeIn">
            <h5 class="bankGothFont">What personal data we collect and why we collect it ?</h5>
            <form class="formCheckOut pt-2" method="post">

                <div class="form-group">
                    <label>Cookies</label>
                    <p>
                        When you log in, we will set up several cookies to save your login information and your screen
                        display choices. .
                        Embedded content from other websites
                        Articles on this site may include embedded content (e.g. videos, images, articles, etc.).
                        Embedded content from other websites behaves in the exact same way as if the visitor has visited
                        the other website.
                        These websites may collect data about you, use cookies, embed additional third-party tracking,
                        and monitor your interaction with that embedded content, including tracking your interaction
                        with the embedded content if you have an account and are logged in to that website.
                    </p>
                </div>
                <div class="form-group">
                    <label>What rights you have over your data ?</label>
                    <p>
                        If you have an account on this site, or have left comments, you can request to receive an
                        exported file of the personal
                        data we hold about you, including any data you have provided to us. You can also request that we
                        erase any personal data
                        we hold about you. This does not include any data we are obliged to keep for administrative,
                        legal, or security
                        purposes.
                    </p>
                </div>
                <div class="form-group">
                    <label>Where we send your data ?</label>
                    <p>
                        Visitor comments may be checked through an automated spam detection service.
                    </p>
                </div>

            </form>
        </div>
        <div class="col-lg-5">
            <img src="<?php bloginfo('template_url'); ?>/images/imgLogin.jpg" class="img-fluid wow fadeIn">
        </div>
    </div>
</div>