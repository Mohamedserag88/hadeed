<!DOCTYPE html>
<html lang="en">

<head>
    <script type="text/javascript">
    var templateUrl = '<?= get_bloginfo("template_url"); ?>';
    var siteUrl = '<?= get_site_url(); ?>';
    </script>
    <?php wp_head(); ?>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
    <meta name="generator" content="WordPress <?php bloginfo('version'); ?>" />
    <title><?php bloginfo('name'); ?> <?php wp_title(); ?></title>
    <link rel="pingback" href="<?php bloginfo('pingback url'); ?>" />
    <link rel="icon" type="<?php bloginfo('template_url'); ?>/image/ico" href="<?php bloginfo('template_url'); ?>/images/footer.png">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,800,900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Cairo:400,700&display=swap" rel="stylesheet">
    <?php 
        $website_link = "hadidx";
        if(get_locale() == 'en_AU'){
    ?>
        <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/style.css">

    <?php
        }
        else{
    ?>
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/style_ar.css">

    <?php
        }
    ?>


<style>
@font-face {
  font-family: "futura-bold";
  font-style: normal;
  font-weight: normal;
  font-stretch: normal;
  line-height: normal;
  src: url("<?php bloginfo('template_url'); ?>/fonts/FuturaBold.otf");
  src: local("futura-bold"), url("<?php bloginfo('template_url'); ?>/fonts/FuturaBold.otf") format("otf"),
    url("<?php bloginfo('template_url'); ?>/fonts/FuturaBold.otf") format("truetype");
}
@font-face {
  font-family: "futura-medium";
  font-style: normal;
  font-weight: normal;
  font-stretch: normal;
  line-height: normal;
  src: url("<?php bloginfo('template_url'); ?>/fonts/FuturaMedium.otf");
  src: local("futura-medium"), url("<?php bloginfo('template_url'); ?>/fonts/FuturaMedium.otf") format("otf"),
    url("<?php bloginfo('template_url'); ?>/fonts/FuturaMedium.otf") format("truetype");
}
@font-face {
  font-family: "BankGothicMedium";
  font-style: normal;
  font-weight: normal;
  font-stretch: normal;
  line-height: normal;
  src: url("<?php bloginfo('template_url'); ?>/fonts/BankGothicMedium.otf");
  src: local("BankGothicMedium"),
    url("<?php bloginfo('template_url'); ?>/fonts/BankGothicMedium.otf") format("otf"),
    url("<?php bloginfo('template_url'); ?>/fonts/BankGothicMedium.otf") format("truetype");
}
@font-face {
  font-family: "square";
  font-style: normal;
  font-weight: normal;
  font-stretch: normal;
  line-height: normal;
  src: url("<?php bloginfo('template_url'); ?>/fonts/Square.ttf");
  src: local("square"),
    url("<?php bloginfo('template_url'); ?>/fonts/Square.ttf") format("ttf"),
    url("<?php bloginfo('template_url'); ?>/fonts/Square.ttf") format("truetype");
}
        #test{
            display:none;
        }
        .sec-title > h2{
            display: none;
        }
        .entry-header > h2{
            display: none;
        }
        body, .secondary-navigation a {
            color: unset;
        }
</style>


<script>
    function scrollDownHandler() {
        // if ($(window).scrollTop() > 20) {
        $(".header").addClass("active");
        // $(".changeLogo").addA
        $('.navbar .navbar-brand img').attr('src', templateUrl + '/images/Artboardcopy5.png');
        $('.navbar .brandShop img').attr('src', templateUrl + '/images/hadid-logo-black.svg');
        // $('.navbar-nav .nav-item .nav-link img:first').attr('src', templateUrl + '/images/language-black.svg');
        $('.navbar-nav .nav-item .nav-link .imgShop').attr('src', templateUrl + '/images/ArtboardRed.png');
        $('.navbar').css("box-shadow", "0px 0px 6px 0 rgba(3,3,3,1)");
        $('.nav-link').css("color", "black");
        // } else {
        //remove the background property so it comes transparent again (defined in your css)
        // $(".header").removeClass("active");
        // $('.navbar .navbar-brand img').attr('src', templateUrl + '/images/had1.png');
        // $('.navbar .brandShop img').attr('src', templateUrl + '/images/ad.png');
        // $('.navbar-nav .nav-item .nav-link img:first').attr('src', templateUrl + '/images/language.svg');
        // $('.navbar-nav .nav-item .nav-link .languageBlack').attr('src', templateUrl + '/images/language-black.svg');
        // $('.navbar-nav .nav-item .nav-link .imgShop').attr('src', templateUrl + '/images/Artboardwhite.png');
        // $('.nav-link').css("color", "white");
        // $('.navbar').css("box-shadow", "none");
        // }
        if (window.innerWidth <= 780) {
            $('.navbar').css("background-color", "white");
            $('.nav-link').css("color", "black");
            $('.navbar-brand img').attr('src', templateUrl + '/images/Artboardcopy5.png');
            // $('.navbar-nav .nav-item .nav-link img:first').attr('src', templateUrl + '/images/language-black.svg');
            $('.navbar-nav .nav-item .nav-link .imgShop').attr('src', templateUrl + '/images/ArtboardRed.png');
        }
    }
</script>

</head>

<body>

<?php 
    $home_url = site_url();
    if(get_locale() != 'en_AU'){
        $home_url = str_replace($website_link."/ar",$website_link,$home_url);
    }
?>
    <!--------------------- // Header // ---------------------------->
    <div class="container" style="margin-bottom:20px">
        <header>
            <nav class="navbar navbar-expand-lg navbar-light header pt-2 pb-2 mb-5 sticky-top">
                <a class="navbar-brand brand2" href="<?=$home_url?>"><img src="<?php bloginfo('template_url'); ?>/images/Asset2.svg"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fas fa-align-justify text-dark"></i>
                </button>
    
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav">
                        
                    <?php
                           $term_list = get_terms(['taxonomy' => 'product_cat', 'hide_empty' => false, 'parent' => 0]);
                           foreach($term_list as $key => $term){
                               $cat_id = (int)$term->term_id;
                               $cat_name= (string)$term->name;
                    ?>
                                <li class="nav-item dropdown newDropdown">
                                    <a class="nav-link dropdown-toggle newDropdowna" href="<?php echo esc_url(get_term_link ($cat_id, 'product_cat'))?>" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <?php echo strtoupper($cat_name); ?>
                                    </a>
                                    <div class="dropdown-menu bankGothFont" aria-labelledby="navbarDropdown">
                                    <?php
                           
                                        $chields = get_terms(['taxonomy' => 'product_cat','order' => 'DESC' , 'orderby' => 'id','hide_empty' => false, 'parent' => $cat_id]);

                                        foreach($chields as $key => $chield){
                                            $chield_id = (int)$chield->term_id;
                                            $chield_name= (string)$chield->name;
                                        ?>
                                            <a class="dropdown-item" href="<?php echo esc_url(get_term_link ($chield_id, 'product_cat'))?>"><?php echo strtoupper($chield_name); ?></a>
                                            
                                        <?php
                               
                                            } 
                                        
                                        ?>
                                    </div>
                                </li>
                       <?php } ?>

                    </ul>
                    <ul class="navbar-nav navArabic">
                        
                        <?php 
                            global $wp;
                            $current_url = $_SERVER['REQUEST_URI'];
                            if(get_locale() == 'en_AU'){
                                $current_url = str_replace($website_link, $website_link."/ar",$current_url);
                        ?>
                            <li class="nav-item wg-li weglot-lang weglot-language flag-0 ar" data-code-language="ar">
                                <a data-wg-notranslate href="<?=$current_url?>" class="nav-link ArabicIcon">
                                    <img src="<?php bloginfo('template_url'); ?>/images/language-black.svg">
                                </a>
                            </li>
                        <?php
                            }
                            else{
                                $current_url = str_replace($website_link."/ar",$website_link,$current_url);
                        ?>
                            <li class="nav-item wg-li weglot-lang weglot-language flag-0 en" data-code-language="en">
                                <a data-wg-notranslate href="<?=$current_url?>" class="nav-link englishIcon" >
                                    <img src="<?php bloginfo('template_url'); ?>/images/languageEnBlack.svg">
                                </a>
                            </li>
                        <?php        
                            }
                        ?>
                       
                        <li class="nav-item d-sm-none">
                            <a href="<?php echo esc_url( wc_get_cart_url()); ?>" class="nav-link">
                                <img class="imgShop" src="<?php bloginfo('template_url'); ?>/images/Artboardwhite.png">
                            </a>
                            <span class="badge badge-danger dangerrr"><?php echo WC()->cart->get_cart_contents_count(); ?></span>
                        </li>
                        <li class="nav-item dropdown d-none d-md-block">
                            <a href="#" class="nav-link dropdown-toggle drpDownH" id="navbarDropdown1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img class="imgShop" src="<?php bloginfo('template_url'); ?>/images/Artboardwhite.png">
                            </a>
                            <span class="badge badge-danger dangerrr"><?php echo WC()->cart->get_cart_contents_count(); ?></span>
                            <div class="dropdown-menu  dropdown-menu-right drpDowArabic" style="width: 266px"  aria-labelledby="navbarDropdown1">
                                <h5 class="commit px-3 pt-3">Shopping Cart </h4><hr>
                                <?php 
                                    if(WC()->cart->get_cart_contents_count()){  
                                ?> 
                                        <?php
                                            foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
                                                $_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
                                                $product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );
                                        ?>
                                                <a class="dropdown-item px-2" href="#">
                                                    <div class="media">
                                                        <?php 
                                                            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $product_id ), 'single-post-thumbnail' );
                                                        ?> 
                                                        <img src="<?php echo $image[0]; ?>" class="mr-3" width="27%" height="27%">
                                                        <div class="media-body">
                                                            <h6 class="mt-1 commit text-wrap mb-0"><?php echo $_product->get_name(); ?></h6>
                                                            <p class="bankGothFont mb-0"><?php 
                                                                                            echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key ); // PHPCS: XSS ok.
                                                                                        ?>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    
                                                </a>
                                        <?php
                                            }
                                        ?>
                                        
                                        <h6 class="subt tot pt-2 px-3 txtMons">Subtotal <span class="float-right"><?php echo WC()->cart->get_cart_subtotal(); ?></span></h6>
                                        <!-- <h6 class="subt pt-2 px-3 txtMons">Shipping <span class="float-right"><?php echo WC()->cart->get_cart_total(); ?></span></h6><hr> -->
                                        <h6 class="tot px-3 font-weight-bold">Total <span class="float-right bankGothFont"><?php echo WC()->cart->get_cart_total(); ?></span></h6>
                                        <p class="px-3 pt-3">
                                            <a class="btn btnContinue  btn-block text-left" href="<?php echo get_permalink( woocommerce_get_page_id( 'checkout' ) )?>">Checkout <i
                                                class="fa fa-long-arrow-alt-right d-flex float-right pt-1"></i></a>
                                        </p>
                                        <p class="text-center text-uppercase font-weight-bold"> <a href="<?php echo esc_url( wc_get_cart_url()); ?>" class="text-dark"> View cart </a></p>
                                <?php
                                    }
                                    else{
                                ?>
                                        <p class="px-3 pt-3">
                                           <a class="btn btnContinue  btn-block text-left" href="<?php echo get_permalink( woocommerce_get_page_id( 'shop' ) )?>">START SHOPPING <i
                                                class="fa fa-long-arrow-alt-right d-flex float-right pt-1"></i></a>
                                        </p>
                                <?php
                                    }
                                ?>
                            </div>
                        </li>
                        <!-- <li class="nav-item dropdown d-none d-md-block">
                            <a href="#" class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img class="imgShop" src="<?php bloginfo('template_url'); ?>/images/Artboardwhite.png">
                            </a>
                            <span class="badge badge-danger dangerrr"><?php echo WC()->cart->get_cart_contents_count(); ?></span>
                            <div class="dropdown-menu  dropdown-menu-right" style="width: 280px"  aria-labelledby="navbarDropdown">
                                <h5 class="commit px-3 pt-3">Shopping Cart </h4><hr>
                                
                                <p class="px-3 pt-3">
                                    <button class="btn btnContinue btn-block text-left">START SHOPPING <i
                                        class="fa fa-long-arrow-alt-right d-flex float-right pt-1"></i></button>
                                </p>
                            </div>
                        </li> -->
                        <li class="nav-item pt-1">

                        <?php if ( is_user_logged_in() ) {  global $current_user; ?>

                            <li class="nav-item dropdown pt-1">
                                <a class="nav-link dropdown-toggle bankGothFont text-dark" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="badge badge-secondary pl-2 py-2"><?php echo substr($current_user->display_name, 0 , 1); ?></span> <?php echo $current_user->display_name; ?>
                                </a>
                                <?php
                                if(get_locale() == 'en_AU'){
                                    ?>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="<?php echo get_site_url()."/my-account/"; ?>"><i class="far fa-user-circle pr-2"></i> My Account </a>
                                    <a class="dropdown-item" href="<?php echo get_site_url()."/my-account/edit-address/"; ?>"><i class="fas fa-map-marker-alt pr-2"></i> My Address </a>
                                    <a class="dropdown-item" href="<?php echo get_site_url()."/my-account/orders/"; ?>"><i class="fa fa-cube pr-2"></i> My Orders</a>
                                    <a class="dropdown-item" href="<?php echo esc_url( wc_logout_url() ); ?>"><i class="fa fa-sign-out-alt pr-2"></i> Log Out</a>
                                </div>
                                <?php
                                }
                                else{
                                ?>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="<?php echo get_site_url()."/my-account/"; ?>"><i class="far fa-user-circle pr-2"></i> حسابى </a>
                                    <a class="dropdown-item" href="<?php echo get_site_url()."/my-account/edit-address/"; ?>"><i class="fas fa-map-marker-alt pr-2"></i> العنوان </a>
                                    <a class="dropdown-item" href="<?php echo get_site_url()."/my-account/orders/"; ?>"><i class="fa fa-cube pr-2"></i> طلباتى</a>
                                    <a class="dropdown-item" href="<?php echo esc_url( wc_logout_url() ); ?>"><i class="fa fa-sign-out-alt pr-2"></i> تسجيل الخروج</a>
                                </div>
                                <?php
                                }
                                ?>
                            </li>

                        <?php } else{ ?> 

                            <a class="nav-link" href="<?php echo get_site_url()."/my-account/"; ?>">LOGIN</a>

                        <?php } ?>

                        </li>
                    </ul>
                </div>
            </nav>
        </header>
    </div>




<?php wc_print_notices(); ?>

