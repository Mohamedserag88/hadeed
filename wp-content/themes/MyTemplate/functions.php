<?php

require get_template_directory() . '/inc/ajax.php';
require get_template_directory() . '/inc/ajax_save_mail.php';

    function load_stylesheets(){

        wp_register_style('jquery' , 'https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css',array() ,1 ,'all');
        wp_enqueue_style('jquery');

        wp_register_style('bootstrap' , 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/css/bootstrap.min.css',array() ,1 ,'all');
        wp_enqueue_style('bootstrap');

        wp_register_style('all' , 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css',array() ,1 ,'all');
        wp_enqueue_style('all');

        if(get_locale() == 'en_AU'){
            wp_register_style('style' , get_template_directory_uri().'/css/style.css',array() ,1 ,'all');

        }
        else{
            wp_register_style('style' , get_template_directory_uri().'/css/style_ar.css',array() ,1 ,'all');
    
        }
        wp_enqueue_style('style');

        wp_register_style('animate' , get_template_directory_uri().'/css/animate.min.css',array() ,1 ,'all');
        wp_enqueue_style('animate');

        wp_register_style('swiper' , get_template_directory_uri().'/package/css/swiper.min.css',array() ,1 ,'all');
        wp_enqueue_style('swiper');

    }

    add_action('wp_enqueue_scripts' , 'load_stylesheets');

    // function addjs(){

    //     wp_register_script('popper' , 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js', array() ,1 ,1,1);
    //     wp_enqueue_script('popper');

    //     wp_register_script('jquery' , 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js', array() ,1 ,1,1);
    //     wp_enqueue_script('jquery');

    //     wp_register_script('bootstrap' , 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/js/bootstrap.min.js', array() ,1 ,1,1);
    //     wp_enqueue_script('bootstrap');

    //     wp_register_script('steps' , 'https://code.jquery.com/ui/1.12.1/jquery-ui.js', array() ,1 ,1,1);
    //     wp_enqueue_script('steps');

    //     wp_register_script('swiper' , get_template_directory_uri().'/package/js/swiper.min.js', array() ,1 ,1,1);
    //     wp_enqueue_script('swiper');

    //     wp_register_script('wow' , get_template_directory_uri().'/js/wow.min.js', array() ,1 ,1,1);
    //     wp_enqueue_script('wow');

    // }


    // add_action( 'wp_enqueue_scripts', 'addjs' );

add_action('phpmailer_init', 'send_smtp_email');
function send_smtp_email($phpmailer) {
    $phpmailer->isSMTP(); //switch to smtp
    $phpmailer->Host = 'pencilproduction.com';
    $phpmailer->SMTPAuth = true;
    $phpmailer->Port = '465';
    $phpmailer->Username = 'support@pencilproduction.com';
    $phpmailer->Password = 'pencil@2020';
    $phpmailer->SMTPSecure = 'ssl';
    $phpmailer->From = 'support@pencilproduction.com';
    $phpmailer->FromName = 'Hadeed';

    // $phpmailer->isSMTP(); //switch to smtp
    // $phpmailer->Host = 'smtp.hadidx.com';
    // $phpmailer->SMTPAuth = true;
    // $phpmailer->Port = '465';
    // $phpmailer->Username = 'support@hadidx.com';
    // $phpmailer->Password = '123456789';
    // $phpmailer->SMTPSecure = 'tls';
    // $phpmailer->From = 'support@hadidx.com';
    // $phpmailer->FromName = 'Hadeed';

    // $phpmailer->isSMTP(); //switch to smtp
    // $phpmailer->Host = 'smtp.gmail.com';
    // $phpmailer->SMTPAuth = true;
    // $phpmailer->Port = '587';
    // $phpmailer->Sender = 'info.hadidx@gmail.com';
    // $phpmailer->Username = 'info.hadidx@gmail.com';
    // $phpmailer->Password = 'vuvdyarsvmzukwly';
    // $phpmailer->SMTPSecure = 'tls';
    // $phpmailer->From = 'info.hadidx@gmail.com';
    // $phpmailer->FromName = 'Hadeed';

    // $phpmailer->Host       = "relay-hosting.secureserver.net";
    // $phpmailer->SMTPDebug  = 0;
    // // $phpmailer->Port       = 25;                   
    // // $phpmailer->SMTPSecure = "none";                 
    // // $phpmailer->SMTPAuth   = false;
    // $phpmailer->Port = '587';
    // $phpmailer->SMTPSecure = 'tls';
    // $phpmailer->SMTPAuth = true;
    // $phpmailer->From = 'info@hadidx.com';
    // $phpmailer->FromName = 'HadidX';

    // $phpmailer->Username   = "info@hadidx.com";
    // $phpmailer->Password   = "123456789";

    // echo "<pre>"; var_export($phpmailer); die;
}


add_action("template_redirect", 'redirection_function');
function redirection_function(){
    global $woocommerce;
    if( is_cart() && WC()->cart->cart_contents_count == 0){
        wp_safe_redirect( get_permalink( woocommerce_get_page_id( 'shop' ) ) );
    }

    if ( is_user_logged_in() && get_the_ID() == 209 ) {
        wp_redirect(get_site_url()."/my-account/");
        exit;
    }

    if ( is_user_logged_in() && get_the_ID() == 213 ) {
        wp_redirect(get_site_url()."/my-account/");
        exit;
    }
}



$theme = wp_get_theme('MyTemplate');
$storefront_version = $theme['Version'];

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if (!isset($content_width)) {
    $content_width = 980; /* pixels */
}

$storefront = (object) array(
    'version'    => $storefront_version,

    /**
     * Initialize all the things.
     */
    'main'       => require 'inc/class-storefront.php',
    'customizer' => require 'inc/customizer/class-storefront-customizer.php',
);

require 'inc/storefront-functions.php';
require 'inc/storefront-template-hooks.php';
require 'inc/storefront-template-functions.php';

if (class_exists('Jetpack')) {
    $storefront->jetpack = require 'inc/jetpack/class-storefront-jetpack.php';
}

if (storefront_is_woocommerce_activated()) {
    $storefront->woocommerce            = require 'inc/woocommerce/class-storefront-woocommerce.php';
    $storefront->woocommerce_customizer = require 'inc/woocommerce/class-storefront-woocommerce-customizer.php';

    require 'inc/woocommerce/class-storefront-woocommerce-adjacent-products.php';

    require 'inc/woocommerce/storefront-woocommerce-template-hooks.php';
    require 'inc/woocommerce/storefront-woocommerce-template-functions.php';
    require 'inc/woocommerce/storefront-woocommerce-functions.php';
}

if (is_admin()) {
    $storefront->admin = require 'inc/admin/class-storefront-admin.php';

    require 'inc/admin/class-storefront-plugin-install.php';
}

/**
 * NUX
 * Only load if wp version is 4.7.3 or above because of this issue;
 * https://core.trac.wordpress.org/ticket/39610?cversion=1&cnum_hist=2
 */
if (version_compare(get_bloginfo('version'), '4.7.3', '>=') && (is_admin() || is_customize_preview())) {
    require 'inc/nux/class-storefront-nux-admin.php';
    require 'inc/nux/class-storefront-nux-guided-tour.php';

    if (defined('WC_VERSION') && version_compare(WC_VERSION, '3.0.0', '>=')) {
        require 'inc/nux/class-storefront-nux-starter-content.php';
    }
}

/**
 * Note: Do not add any custom code here. Please use a custom plugin so that your customizations aren't lost during updates.
 * https://github.com/woocommerce/theme-customisations
 */

add_shortcode( 'wc_reg_form_bbloomer', 'bbloomer_separate_registration_form' );
   
function bbloomer_separate_registration_form() {
if ( is_admin() ) return;
    ob_start();
    if ( ! is_user_logged_in() ) {   
    // NOTE: THE FOLLOWING <FORM> IS COPIED FROM woocommerce/templates/myaccount/form-login.php
    // IF WOOCOMMERCE RELEASES AN UPDATE TO THAT TEMPLATE, YOU MUST CHANGE THIS ACCORDINGLY
    ?>
    
            <div class="container my-5">
                <div class="row pt-5 justify-content-around">
                    <div class="col-lg-5 wow fadeIn">
                        <p class="pt-5"><small class="bankGothFontOpacity"> SIGN UP </small></p>
                        <h5 class="bankGothFont">REGISTER YOUR ACCOUNT</h5>
                        <form method="post" class="woocommerce-form woocommerce-form-register register" <?php do_action( 'woocommerce_register_form_tag' ); ?> >

                        <?php do_action( 'woocommerce_register_form_start' ); ?>

                            <div class="form-group">
                                <label>NAME</label>
                                <input type="text" name="username" id="username" autocomplete="username" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>EMAIL</label>
                                <input type="email" name="email" id="email" autocomplete="email" value="<?php echo ( ! empty( $_POST['email'] ) ) ? esc_attr( wp_unslash( $_POST['email'] ) ) : ''; ?>" class="form-control">
                            </div>

                            <?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>

                            <div class="form-group">
                                <label>PASSWORD</label>
                                <input type="password" name="password" id="password" autocomplete="current-password"  class="form-control">
                            </div>

                            <?php else : ?>

                            <p><?php esc_html_e( 'A password will be sent to your email address.', 'woocommerce' ); ?></p>

                            <?php endif; ?>

                            <?php do_action( 'woocommerce_register_form' ); ?>


                            
                            <?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>
                            <button type="submit" class="btn btnContinue px-3 mt-4 rounded-0" name="register" value="<?php esc_attr_e( 'Register', 'woocommerce' ); ?>"><?php esc_html_e( 'REGISTER YOUR ACCOUNT', 'woocommerce' ); ?></button>
                                        
                            <p class="pt-5"><a href="#" class="bankGothFontOpacity">Have Accouunt?</a>
                            <a href="<?php echo get_site_url(); ?>/my-account" class="bankGothFont text-dark">LOGIN NOW</a></p>
                            
                            <?php do_action( 'woocommerce_register_form_end' ); ?>

                        </form>
                    </div>
                    <div class="col-lg-5">
                        <img src="<?php bloginfo('template_url'); ?>/images/imgLogin.jpg" class="img-fluid wow fadeIn">
                    </div>
                </div>
            </div>
            
    <?php
    // END OF COPIED HTML
    // ------------------
        
    }
    return ob_get_clean();
}

add_shortcode( 'wc_login_form_bbloomer', 'bbloomer_separate_login_form' );
function bbloomer_separate_login_form() {
if ( is_admin() ) return;
    ob_start();
    if ( ! is_user_logged_in() ) {   
    // NOTE: THE FOLLOWING <FORM> IS COPIED FROM woocommerce/templates/myaccount/form-login.php
    // IF WOOCOMMERCE RELEASES AN UPDATE TO THAT TEMPLATE, YOU MUST CHANGE THIS ACCORDINGLY
    ?>
    
    <div class="container my-5 logArabic">
			<div class="row pt-5 justify-content-around">
				<div class="col-lg-5 wow fadeIn">
					<p class="pt-5"><small class="bankGothFontOpacity"> SIGN IN </small></p>
					<h5 class="bankGothFont">LOGIN TO YOUR ACCOUNT</h5>
					 <form class="formCheckOut pt-2" method="post"> 

						<?php do_action( 'woocommerce_login_form_start' ); ?>

							<div class="form-group">
								<label>USERNAME</label>
								<input type="text" name="username" id="username" autocomplete="username" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" class="form-control">
							</div>
							<div class="form-group">
								<label>PASSWORD</label>
								<input type="password" name="password" id="password" autocomplete="current-password"  class="form-control">
							</div>
							
						<?php do_action( 'woocommerce_login_form' ); ?>

						<?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>
						<button type="submit" class="btn btnContinue px-3 mt-4 rounded-0" name="login" value="<?php esc_attr_e( 'LOGIN TO YOUR ACCOUNT', 'woocommerce' ); ?>"><?php esc_html_e( 'LOGIN TO YOUR ACCOUNT', 'woocommerce' ); ?></button>
				
						<p class="pt-5"><a href="<?=get_site_url();?>/register/" class="bankGothFontOpacity">DON’T HAVE AN ACCOUNT?</a>
						<a href="<?=get_site_url();?>/register/" class="bankGothFont text-dark">SIGN UP NOW</a></p>
						<p class="pb-2"><a href="<?php echo esc_url( wp_lostpassword_url() ); ?>" class="bankGothFont text-dark">FORGOT PASSWORD?</a></p><hr>
						
					</form>
					<button class="btn btn-block btnFace py-3 my-5 rounded-0"><i class="fab fa-facebook-f px-4"></i>LOGIN WITH FACEBOOK</button>
				</div>
				<div class="col-lg-5">
					<img src="<?php bloginfo('template_url'); ?>/images/imgLogin.jpg" class="img-fluid wow fadeIn">
				</div>
			</div>
		</div>
            
    <?php
    // END OF COPIED HTML
    // ------------------
        
    }
    return ob_get_clean();
}

add_shortcode( 'RenderHtml', 'RenderHtml' );
function RenderHtml($atts) {
    $dir = $atts["dir"] ?? "custom-pages/";
    ob_start();
    include($dir.$atts["url"].".php");
    $output = ob_get_contents();
    ob_end_clean();
    return $output;
}


add_filter('woocommerce_ship_to_different_address_checked', '__return_true', 999);

add_action( 'get_header', 'sk_conditionally_remove_wc_assets' );
/**
 * Unload WooCommerce assets on non WooCommerce pages.
 */
function sk_conditionally_remove_wc_assets() {

    // if WooCommerce is not active, abort.
    if ( ! class_exists( 'WooCommerce' ) ) {
        return;
    }

    // if this is a WooCommerce related page, abort.
    if ( is_woocommerce() || is_cart() || is_checkout() || is_page( array( 'my-account' ) ) ) {
        return;
    }

    remove_action( 'wp_enqueue_scripts', [ WC_Frontend_Scripts::class, 'load_scripts' ] );
    remove_action( 'wp_print_scripts', [ WC_Frontend_Scripts::class, 'localize_printed_scripts' ], 5 );
    remove_action( 'wp_print_footer_scripts', [ WC_Frontend_Scripts::class, 'localize_printed_scripts' ], 5 );

}


add_action( 'init', 'woocommerce_clear_cart_url' );
function woocommerce_clear_cart_url() {
	if ( isset( $_GET['clear-cart'] ) ) {
		global $woocommerce;
		$woocommerce->cart->empty_cart();
	}
}

/** to make field from billing is nullable */

// Billing and shipping addresses fields
// add_filter( 'woocommerce_default_address_fields' , 'filter_default_address_fields', 20, 1 );
// function filter_default_address_fields( $address_fields ) {
//     // Only on checkout page
//     if( ! is_checkout() ) return $address_fields;

//     // All field keys in this array
//     $key_fields = array('state','postcode');

//     // Loop through each address fields (billing and shipping)
//     foreach( $key_fields as $key_field )
//         $address_fields[$key_field]['required'] = false;

//     return $address_fields;
// }


/** add fees to cart total */

// function prefix_add_discount_line( $cart ) {

//     $discount = $cart->subtotal * 0.1;
  
//     $cart->add_fee( __( 'Down Payment', 'yourtext-domain' ) , -$discount );
  
//   }
//   add_action( 'woocommerce_cart_calculate_fees', 'prefix_add_discount_line' );


// add_action( 'woocommerce_review_order_before_order_total', 'custom_cart_total' );
// add_action( 'woocommerce_before_cart_totals', 'custom_cart_total' );
// function custom_cart_total() {

//     if ( is_admin() && ! defined( 'DOING_AJAX' ) )
//             return;

//     WC()->cart->total *= 0.25;
//     //var_dump( WC()->cart->total);
// }


// Hook in
add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );
// Our hooked in function - $fields is passed via the filter!
function custom_override_checkout_fields( $fields ) {
     $fields['billing']['billing_age'] = array(
        'label'     => __('Billing Age', 'woocommerce'),
        'placeholder'   => _x('age', 'placeholder', 'woocommerce'),
        'required'  => false,
        'class'     => array('form-row-wide'),
        'clear'     => true
     );

     $fields['billing']['billing_best_sport'] = array(
        'label'     => __('Best Sport', 'woocommerce'),
        'placeholder'   => _x('best sport', 'placeholder', 'woocommerce'),
        'required'  => false,
        'class'     => array('form-row-wide'),
        'clear'     => true
     );
    unset($fields['billing']['billing_postcode']);
     return $fields;
}


/**
 * Display field value on the order edit page
 */
// add_filter( 'woocommerce_checkout_fields' , 'bbloomer_remove_billing_postcode_checkout' );
 
// function bbloomer_remove_billing_postcode_checkout( $fields ) {
//   unset($fields['billing']['billing_postcode']);
//   return $fields;
// }

add_action( 'woocommerce_admin_order_data_after_billing_address', 'my_custom_checkout_field_display_admin_order_meta', 10, 1 );

function my_custom_checkout_field_display_admin_order_meta($order){
    echo '<p><strong>'.__('Age').':</strong> ' . get_post_meta( $order->get_id(), '_billing_age', true ) . '</p>';
    echo '<p><strong>'.__('Best Sport').':</strong> ' . get_post_meta( $order->get_id(), '_billing_best_sport', true ) . '</p>';
}
// function wpse_19692_registration_redirect() {
//     // if(WC()->cart->cart_contents_count != 0){
//     //     return home_url( '/cart' );

//     // }
//     // else{
//         return home_url( '/' );

//     // }
// }
// add_filter( 'registration_redirect', 'wpse_19692_registration_redirect' );


function custom_registration_redirect() {
    // wp_logout();
    if(WC()->cart->cart_contents_count != 0){ return home_url( '/cart' ); }
    else return home_url( '/' );
}
add_action('woocommerce_registration_redirect', 'custom_registration_redirect', 2);
