
<?php

    global $product;
 foreach($product->get_children() as $p){
    $product = get_product($p);
    // print_r($product);
    ?>
<div class="row">
    <div class="col-lg-8">
        <h3 class="bankGothFont"><?php echo $product->name;//the_title(); ?></h3>
        <input type="number" id="quantity_5e29952790ad8" class="input-text qty text" step="1" min="0" max="" name="quantity[<?php echo $p; ?>]" value="0" title="Qty" size="4" inputmode="numeric">

    </div>
    <div class="col-lg-4">
    
            <?php if( $product->is_on_sale() && !empty($product->get_sale_price()) && !empty($product->get_regular_price())) { ?>

            <h1 class="text-danger font-weight-bolder" style="font-family: square;"><?php echo woocommerce_price($product->get_sale_price());?></h1>
            <h5 class="bankGothFontOpacity">WAS <del> <?php echo woocommerce_price($product->get_regular_price()); ?> </del></h5>
        
        <?php }else{ ?>

            <h1 class="text-danger font-weight-bolder" style="font-family: square;"><?php echo  woocommerce_price($product->get_price());?></h1>

        <?php } ?>

    </div>
</div>


<?php } wp_reset_postdata(); ?>
<p class="bankGothFont text-danger pt-3"><img src="<?php bloginfo('template_url'); ?>/images/ambulance.svg"> FREE NEXT DAY DELIVERY  </p>
