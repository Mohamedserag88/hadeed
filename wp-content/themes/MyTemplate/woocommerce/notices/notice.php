<?php
/**
 * Show messages
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/notices/notice.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce/Templates
 * @version     3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! $messages ) {
	return;
}

?>



<?php  foreach ( $messages as $message ) :  ?>
<div class="container bg-dark alert pl-3 pr-2 pt-3 alert-warning bankGothFont alert-dismissible fade show" style="margin-top: 9rem;" role="alert">
	<p class=" txtDis text-uppercase"><i class="fa fa-info-circle fa-lg pr-3 pt-2"></i>
		
			<?php echo wc_kses_notice( $message ); ?>
			
		<button type="button" class="btn btnDismiss py-0 float-right" data-dismiss="alert" aria-label="Close">
			Dismiss
		</button>
	</p>
</div>

<?php endforeach; ?>