<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.8.0
 */

defined( 'ABSPATH' ) || exit;

do_action( 'woocommerce_before_cart' ); 

?>

<form class="woocommerce-cart-form " action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">
	<?php do_action( 'woocommerce_before_cart_table' ); ?>

	<?php do_action( 'woocommerce_before_cart_contents' ); ?>

	<div class="container shopArabic py-5">
        <div class="row rowArabic pt-5">
            <div class="col-lg-8 col-md-12">
                <h4 class="bankGothFont text-center pt-5">YOUR SHOPPING CART</h4>
                <p class="text-center"><small class="bankGothFontOpacity">YOU CAN START TRACKING YOUR ITEMS, WE ALREADY
                        SENT YOU AN EMAIL WITH THE PURCHASE RECIEPT, HOPE YOU ENJOY OUR QUALITY AND HAPPY
                        WORKOUT !
                    </small></p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                <div class="congrats pt-5 wow fadeIn">

						
						<?php
						foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
							$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
							$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

							if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
								$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
								?>
								<div class="row justify-content-around border-bottom pb-2">
									<div class="col-lg-7">
										<div class="media">
											<?php 
												$image = wp_get_attachment_image_src( get_post_thumbnail_id( $product_id ), 'single-post-thumbnail' );
											?> 
											<img src="<?php echo $image[0]; ?>"  class="mr-3" width="80px" height="80px">
											
											<div class="media-body">
												<h6 class="mt-2 commit"><?php echo $_product->get_name(); ?></h6>
												<p class="bankGothFont">

													<?php 
														echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key ); // PHPCS: XSS ok.
													?>
												</p>
											</div>
										</div>
									</div>
									<div class="col-lg-3 col-md-5 col-8">
										<div class="item">
											<div class="form-inline pt-1">
												<div class="quantity">
													
													<button class="btn btnContinue minus-btn px-3 " type="button"
														name="button">-</button>
													<input type="text" class="form-control qty" id="qty" name="cart[<?php echo $cart_item_key; ?>][qty]" value="<?php echo $cart_item['quantity']; ?>"
														style="width: 55px;height:40px">
													<button class="btn plus-btn btnContinue " type="button" name="button">+</button>

												</div>
											</div>
										</div>
									</div>
								
									<div class="col-lg-2 col-md-1 col-2">
										<a href="<?php echo esc_url( wc_get_cart_remove_url( $cart_item_key ) ); ?>" class="btn bg-white"><img src="<?php bloginfo('template_url'); ?>/images/btnDelete.svg"></a>
									</div>
								</div>

								<?php
							}
						}
						?>



					<?php do_action( 'woocommerce_cart_contents' ); ?>
                    <a href="?clear-cart" class="btn btnContinue editAR bg-white text-dark mt-3 pt-2 ml-3 border-dark"><img src="<?php bloginfo('template_url'); ?>/images/btnDelete.svg" class="pr-2"> 
                        CLEAR CART </a>
					<input type="hidden" id="woocommerce-cart-nonce" name="woocommerce-cart-nonce" value="20e01a306f">
					<input type="hidden" name="_wp_http_referer" value="<?php echo esc_url( wc_get_cart_url()); ?>">
					<button type="submit" class="btn btnContinue editAR mt-3 ml-3 border-dark float-right updateCart" name="update_cart" value="Update cart">Update cart</button>
					<?php do_action( 'woocommerce_cart_actions' ); ?>
					<?php wp_nonce_field( 'woocommerce-cart', 'woocommerce-cart-nonce' ); ?>
					
				</div>
            </div>
            <div class="col-lg-4">
                <div class="shopCart mt-5 px-4 py-4 wow fadeIn">
					<h4 class="commit">CART TOTALS</h4>
					
					<hr>
					
					<?php do_action( 'woocommerce_before_cart_collaterals' ); ?>

					<div class="cart-collaterals">
						<?php
							/**
							 * Cart collaterals hook.
							 *
							 * @hooked woocommerce_cross_sell_display
							 * @hooked woocommerce_cart_totals - 10
							 */
							do_action( 'woocommerce_cart_collaterals' );
						?>
					</div>

					<?php do_action( 'woocommerce_after_cart' ); ?>


                </div>
            </div>
        </div>
    </div>
	<?php do_action( 'woocommerce_after_cart_table' ); ?>
</form>


