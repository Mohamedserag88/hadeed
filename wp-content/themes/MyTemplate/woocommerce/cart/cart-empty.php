<?php
/**
 * Empty cart page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart-empty.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

defined( 'ABSPATH' ) || exit;

/*
 * @hooked wc_empty_cart_message - 10
 */
// do_action( 'woocommerce_cart_is_empty' );

if ( wc_get_page_id( 'shop' ) > 0 ) : ?>
	<div class="container py-5">
        <div class="row pt-5">
            <div class="col-lg-12 col-md-12 pt-3">
                <h4 class="bankGothFont text-center pt-5">YOUR SHOPPING CART IS EMPTY</h4>
                <p class="text-center"><small class="bankGothFontOpacity">YOU CAN START TRACKING YOUR ITEMS, WE ALREADY
                        SENT YOU AN EMAIL WITH THE PURCHASE RECIEPT, HOPE YOU ENJOY OUR QUALITY AND HAPPY
                        WORKOUT !
                    </small>
                </p>
                <p class="text-center pt-3 mb-5"><a href="<?php echo get_site_url().'/shop'?>" class="btn btnShop px-5 text-white text-uppercase"> Shop Now</a></p>
            </div>
        </div>
    </div>
<?php endif; ?>
