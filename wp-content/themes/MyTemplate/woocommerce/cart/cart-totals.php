<?php
/**
 * Cart totals
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart-totals.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 2.3.6
 */

defined( 'ABSPATH' ) || exit;

?>
<div class="cart_totals <?php echo ( WC()->customer->has_calculated_shipping() ) ? 'calculated_shipping' : ''; ?>">

	<?php do_action( 'woocommerce_before_cart_totals' ); ?>

	<!-- <h2><?php esc_html_e( 'Cart totals', 'woocommerce' ); ?></h2> -->


		<p class="have bankGothFont" data-toggle="collapse" data-target="#collapseA">
			Have a Voucher? <span class="float-right">
			<i class="fa fa-chevron-down"></i></span>
		</p>
		<div id="collapseA" class="collapse">
		<form action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">
			<div class="form__coupon row">
				<input type="text" name="coupon_code" class="form-control offset-lg-1 col-lg-7" placeholder="Coupon code">
				<button type="submit" class="btn btnContinue col-lg-3" style="margin-left:5px;height:37px;float:right" name="apply_coupon" value="<?php esc_attr_e( 'Apply', 'woocommerce' ); ?>">Apply</button>
			</div>
		</form>
		</div>
		<?php foreach ( WC()->cart->get_coupons() as $code => $coupon ) : ?>
				<h6><?php wc_cart_totals_coupon_label( $coupon ); ?></h6>
				<h6><?php wc_cart_totals_coupon_html( $coupon ); ?></h6>
		<?php endforeach; ?>

		<hr>

		<h6 class="subt tot">Subtotal <span class="float-right"><?php wc_cart_totals_subtotal_html(); ?></span></h6>
	
		

		<?php if ( WC()->cart->needs_shipping() && WC()->cart->show_shipping() ) : ?>
			<hr>
			<?php do_action( 'woocommerce_cart_totals_before_shipping' ); ?>

				<h6 class="subt tot"><?php  wc_cart_totals_shipping_html(); ?></h6>

			<?php //do_action( 'woocommerce_cart_totals_after_shipping' ); ?>

		<?php //elseif ( WC()->cart->needs_shipping() && 'yes' === get_option( 'woocommerce_enable_shipping_calc' ) ) : ?>

			
		
				<?php //woocommerce_shipping_calculator(); ?>
			

		<?php endif; ?>

		<hr>

		<?php foreach ( WC()->cart->get_fees() as $fee ) : ?>

			
			<h6 class="tot"><?php echo esc_html( $fee->name ); ?> <span class="float-right bankGothFont"><?php wc_cart_totals_fee_html( $fee ); ?></span></h6>

			<hr>
		<?php endforeach; ?>

		<?php
		if ( wc_tax_enabled() && ! WC()->cart->display_prices_including_tax() ) {
			$taxable_address = WC()->customer->get_taxable_address();
			$estimated_text  = '';

			if ( WC()->customer->is_customer_outside_base() && ! WC()->customer->has_calculated_shipping() ) {
				/* translators: %s location. */
				$estimated_text = sprintf( ' <small>' . esc_html__( '(estimated for %s)', 'woocommerce' ) . '</small>', WC()->countries->estimated_for_prefix( $taxable_address[0] ) . WC()->countries->countries[ $taxable_address[0] ] );
			}

			if ( 'itemized' === get_option( 'woocommerce_tax_total_display' ) ) {
				foreach ( WC()->cart->get_tax_totals() as $code => $tax ) { // phpcs:ignore WordPress.WP.GlobalVariablesOverride.OverrideProhibited
					?>

					<h6 class="tot"><?php echo esc_attr( $tax->label ); ?> <span class="float-right bankGothFont"><?php echo wp_kses_post( $tax->formatted_amount ); ?></span></h6>
					<hr>
					<?php
				}
			} else {
				?>

				<h6 class="tot"><?php echo esc_attr( WC()->countries->tax_or_vat() ); ?> <span class="float-right bankGothFont"><?php wc_cart_totals_taxes_total_html(); ?></span></h6>
				<hr>
				<?php
			}
		}
		?>

		<?php do_action( 'woocommerce_cart_totals_before_order_total' ); ?>
			<!-- <div id="product_total_price">
				<p class="price">21212</p>
			</div> -->

			<h6 class="tot"><?php esc_attr_e( 'Total', 'woocommerce' ); ?> <span class="float-right bankGothFont"><?php wc_cart_totals_order_total_html(); ?></span></h6>

		<?php do_action( 'woocommerce_cart_totals_after_order_total' ); ?>


	<div class="wc-proceed-to-checkout">
		
		<?php do_action( 'woocommerce_proceed_to_checkout' ); ?>

	</div>

	<?php do_action( 'woocommerce_after_cart_totals' ); ?>

</div>
