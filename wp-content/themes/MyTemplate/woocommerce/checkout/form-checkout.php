<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

//  do_action( 'woocommerce_before_checkout_form', $checkout );

// If checkout registration is disabled and not logged in, the user cannot checkout.
if ( ! is_user_logged_in() ) {
	// echo esc_html( apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) ) );
	wp_redirect(get_site_url()."/my-account");
}

?>

<form name="checkout" method="post" class="formCheckOut woocommerce-checkout" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">

	<div class="container py-5">
        <div class="row rowCheckArabic py-5">
            <div class="col-lg-8">
                <ul class="nav nav-pills nav-pills2 pt-5" role="tablist">
                    <li class="nav-item">
                        <a id="customerInfoTab" class="nav-link active" data-toggle="pill" href="#customerInfo">01 CUSTOMER INFO</a>
                    </li>
                    <li class="nav-item">
                        <a id="deliveryTab" class="nav-link" data-toggle="pill" href="#shopInfo"> 02 DELIVERY INFO </a>
                    </li>
                    <li class="nav-item">
                        <a id="paymentTab" class="nav-link" data-toggle="pill" href="#paymentSelection"> 03 PAYMENT SELECTION </a>
                    </li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content pb-3">
					<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

                    <div id="customerInfo" class="container tab-pane active"><br>
						<h4 class="commit pt-4">Customer Information</h4>
						
						<?php if ( $checkout->get_checkout_fields() ) : ?>
						<?php do_action( 'woocommerce_checkout_billing' ); ?>
                        <?php endif; ?>

                        <div class="row pt-3">
                            <div class="col-sm-6 col-12 pt-3">
                                <a href="<?php echo get_site_url()."/shop/"; ?>" class="back"><i class="fa fa-long-arrow-alt-left pr-2 mt-1"></i> Return to Shop</a>
                            </div>
                            <div class="col-sm-6 pt-3 col-12">
                                <a onclick="toggleActive('deliveryTab')" data-toggle="pill" href="#shopInfo" class="btn btnContinue btnCheckDeliv btn-block text-left pt-2">CONTINUE SHIPPING<i class="fa fa-long-arrow-alt-right d-flex float-right pt-2"></i></a>
                            </div>
                        </div> 

					</div>
					
                    <div id="shopInfo" class="container tab-pane fade"><br>
                    



                        <?php if ( WC()->cart->needs_shipping() && WC()->cart->show_shipping() ) : ?>
                            <?php do_action( 'woocommerce_review_order_before_shipping' ); ?>

                            <?php wc_cart_totals_shipping_html(); ?>

                            <?php do_action( 'woocommerce_review_order_after_shipping' ); ?>

                        <?php endif; ?>


                        
                   
                        
						<?php //if ( $checkout->get_checkout_fields() ) : ?>
						<?php //do_action( 'woocommerce_checkout_shipping' ); ?>
						<?php //endif; ?>

						
                        <div class="row pt-4">
                            <div class="col-sm-6 col-12 pt-3">
                                <a href="<?php echo get_site_url()."/shop/"; ?>" class="back"><i class="fa fa-long-arrow-alt-left pr-2 mt-1"></i> Return to Shop</a>
                            </div>
                            <div class="col-sm-6 col-12 pt-2">
                                <a  onclick="toggleActive('paymentTab')" href="#paymentSelection" data-toggle="pill" class="btn btnContinue btnCheckDeliv btn-block text-left pt-2">CONTINUE PAYMENT<i class="fa fa-long-arrow-alt-right d-flex float-right pt-2"></i></a>
                            </div>
                        </div>
					</div>

					<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>

                    <div id="paymentSelection" class="container tab-pane fade"><br>
                       

                        <?php
                        if ( ! is_ajax() ) {
                            do_action( 'woocommerce_review_order_before_payment' );
                        }
                        if ( WC()->cart->needs_payment() ) : ?>
                                <?php
                                if ( ! empty(WC()->payment_gateways->get_available_payment_gateways() ) ) {
                                    foreach ( WC()->payment_gateways->get_available_payment_gateways() as $gateway ) {
                                        wc_get_template( 'checkout/payment-method.php', array( 'gateway' => $gateway ) );
                                    }
                                 } else {
                                     echo '<div class="alert alert-danger">' . apply_filters( 'woocommerce_no_available_payment_methods_message', WC()->customer->get_billing_country() ? esc_html__( 'Sorry, it seems that there are no available payment methods for your state. Please contact us if you require assistance or wish to make alternate arrangements.', 'woocommerce' ) : esc_html__( 'Please fill in your details above to see available payment methods.', 'woocommerce' ) ) . '</div>'; // @codingStandardsIgnoreLine
                                 }
                                ?>
                        <?php endif; ?>


                       <?php wc_get_template( 'checkout/terms.php' ); ?>

                        
                        <div class="row pt-3">
                            <div class="col-sm-6 col-12 pt-3">
                                <a href="<?php echo get_site_url()."/shop/"; ?>" class="back"><i class="fa fa-long-arrow-alt-left pr-2 mt-1"></i> Return to
                                    Shop</a>
                            </div>
                            <div class="col-sm-6 pt-3 col-12">

                            <?php //do_action( 'woocommerce_review_order_before_submit' ); ?>

                            <?php //echo apply_filters( 'woocommerce_order_button_html', '<button type="submit" class="button alt" name="woocommerce_checkout_place_order" id="place_order" value="' . esc_attr( $order_button_text ) . '" data-value="' . esc_attr( $order_button_text ) . '">' . esc_html( $order_button_text ) . '</button>' ); // @codingStandardsIgnoreLine ?>

                                <button type="submit"  class="btn btnContinue btn-block text-left" name="woocommerce_checkout_place_order"
                                 id="place_order" >
                                 Confirm Your Order</button>
                            </div>
                        </div>

                        <?php do_action( 'woocommerce_review_order_after_submit' ); ?>

                        <?php wp_nonce_field( 'woocommerce-process_checkout', 'woocommerce-process-checkout-nonce' ); ?>
                        
                        <?php 
                        if ( ! is_ajax() ) {
                            do_action( 'woocommerce_review_order_after_payment' );
                        }
                        ?>
                           

                        
                    </div>
                </div>
			</div>

			<?php do_action( 'woocommerce_checkout_before_order_review_heading' ); ?>

            <div class="col-lg-4">
                <div class="shopCart mt-5 px-3 py-4">
                    <h4 class="commit">Shopping Cart <span class="badge badge-danger d-flext float-right"><?php echo WC()->cart->get_cart_contents_count(); ?></span></h4><hr>
					
					<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>

					<div  class="woocommerce-checkout-review-order">
						<?php  do_action( 'woocommerce_checkout_order_review' ); ?>
					</div>

					<?php  do_action( 'woocommerce_checkout_after_order_review' ); ?>

                </div>
			</div>


        </div>
	</div>
	
</form>

<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>


<script>
    function toggleActive(id) {
        var ids = ['customerInfoTab','deliveryTab','paymentTab'];
        ids.forEach( (singleID)  => {
            if(id == singleID) {
               document.getElementById(singleID).classList.add("active");
            } else {
                document.getElementById(singleID).classList.remove("active");
            }
        });
    }
</script>