<?php
/**
 * Checkout billing information form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-billing.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 * @global WC_Checkout $checkout
 */

defined( 'ABSPATH' ) || exit;
?>

<div class="woocommerce-billing-fields">
	<?php if ( wc_ship_to_billing_address_only() && WC()->cart->needs_shipping() ) : ?>

		<h3><?php esc_html_e( 'Billing &amp; Shipping', 'woocommerce' ); ?></h3>

	<?php else : ?>

		<h3><?php //  esc_html_e( 'Billing details', 'woocommerce' ); ?></h3>

	<?php endif; ?>

	<?php do_action( 'woocommerce_before_checkout_billing_form', $checkout ); ?>

	<div class="woocommerce-billing-fields__field-wrapper">
		<?php
		$fields = $checkout->get_checkout_fields( 'billing' );
		// echo '<pre>';
		// print_r($fields);
		// echo '</pre>';
		// echo $checkout->get_value('additional_fields');
		// foreach ( $fields as $key => $field ) {
			// woocommerce_form_field( $key, $field, $checkout->get_value( $key ) );
			?>
			<div class="row pt-4 pb-3">
				<div class="col">
					<label for="First"><?php echo $fields['billing_first_name']['label']; ?></label>
					<input type="text" name="billing_first_name" value="<?php echo $checkout->get_value('billing_first_name'); ?>" class="form-control">
				</div>
				<div class="col">
					<label for="First"><?php echo $fields['billing_last_name']['label']; ?></label>
					<input type="text" name="billing_last_name" value="<?php echo $checkout->get_value('billing_last_name'); ?>" class="form-control">
				</div>
			</div>
			<div class="form-group">
				<label><?php echo $fields['billing_address_1']['label']; ?></label>
				<input type="text" name="billing_address_1" value="<?php echo $checkout->get_value('billing_address_1'); ?>" class="form-control">
			</div>
			<!-- <div class="form-group">
				<label><?php echo $fields['billing_address_2']['label']; ?></label>
				<input type="text" name="billing_address_2" value="<?php echo $checkout->get_value('billing_address_2'); ?>" class="form-control">
			</div> -->
			
			<div class="row pt-3">
				
				<div class="col">
					<label for="First"><?php echo $fields['billing_state']['label']; ?></label>
					<input type="text"  name="billing_state" value="<?php echo $checkout->get_value('billing_state'); ?>" class="form-control">
				</div>
			</div>
			<div class="row">
				<div class="col">
					<label><?php echo $fields['billing_country']['label']; ?></label>
					<?php 
						global $woocommerce;
						$countries_obj   = new WC_Countries();
						$countries   = $countries_obj->__get('countries');
					?>				
					<select class="form-control"name="billing_country">
						<option disabled selected>Selct Your Country</option>
						<?php foreach($countries as $key => $country){ ?>
						<option <?php if($key == "EG"){ echo "selected"; } ?> value="<?php echo $key; ?>"> <?php echo $country; ?> </option>
						<?php } ?>
					</select>
				</div>
				<div class="col">
					<label for="First"><?php echo $fields['billing_city']['label']; ?></label>
					<input type="text" name="billing_city" value="<?php echo $checkout->get_value('billing_city'); ?>" class="form-control">
				</div>
			</div>
			

			<div class="row pt-3">
				<div class="col">
					<label for="First"><?php echo $fields['billing_email']['label']; ?></label>
					<div class="input-group mb-3">
						<input type="email" name="billing_email" value="<?php echo $checkout->get_value('billing_email'); ?>" class="form-control">
						<div class="input-group-append">
						</div>
					</div>
				</div>
				<div class="col">
					<label for="First"><?php echo $fields['billing_phone']['label']; ?></label>
					<input type="text" name="billing_phone" value="<?php echo $checkout->get_value('billing_phone'); ?>" class="form-control">
				</div>
			</div>


				
			<!-- <button class="btn bg-white text-primary mt-4 btnAddress"> + Add Additional Information </button> -->
			   
					<?php
					// $field_name = "billing_age";
					// $field= $fields[$field_name];?>
					<!-- <div class="form-group">
						<label><?= $field['label']; ?></label>
						<input type="<?= $field['type']; ?>" name="<?= $field_name; ?>" value="<?=  $checkout->get_value($field_name);?>" class="form-control">
					</div> -->


					<?php 
					// $field_name = "billing_best_sport";
					// $field= $fields[$field_name];?>
					<!-- <div class="form-group">
						<label><?= $field['label']; ?></label>
						<input type="<?= $field['type']; ?>" name="<?= $field_name; ?>" value="<?=  $checkout->get_value($field_name);?>" class="form-control">
					</div> -->


						
		<?php 
		?>
	</div>

	<?php do_action( 'woocommerce_after_checkout_billing_form', $checkout ); ?>
</div>


                            
                            
