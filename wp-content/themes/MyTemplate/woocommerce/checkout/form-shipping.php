<?php
/**
 * Checkout shipping information form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-shipping.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 * @global WC_Checkout $checkout
 */

defined( 'ABSPATH' ) || exit;
?>
<div class="woocommerce-shipping-fields">
	<?php if ( true === WC()->cart->needs_shipping_address() ) : ?>

		<!-- <h3 id="ship-to-different-address">
			<label class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox">
				<input id="ship-to-different-address-checkbox" class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox" <?php checked( apply_filters( 'woocommerce_ship_to_different_address_checked', 'shipping' === get_option( 'woocommerce_ship_to_destination' ) ? 1 : 0 ), 1 ); ?> type="checkbox" name="ship_to_different_address" value="1" /> <span><?php esc_html_e( 'Ship to a different address?', 'woocommerce' ); ?></span>
			</label>
		</h3> -->
		<button type="button" class="btn bg-white text-primary mt-4 btnAddress"> + Add New Address </button>

		<div class="shipping_address">

			<?php do_action( 'woocommerce_before_checkout_shipping_form', $checkout ); ?>

			<div class="woocommerce-shipping-fields__field-wrapper">
				<?php
				$fields = $checkout->get_checkout_fields( 'shipping' );
				// echo '<pre>';
				// print_r($fields);
				// echo '</pre>';
				// foreach ( $fields as $key => $field ) {
				// 	woocommerce_form_field( $key, $field, $checkout->get_value( $key ) );
				// }
				?>
                <div class="formCheckOut formDisplay" id="test">

				<div class="row pt-4 pb-3">
					<div class="col">
						<label for="First"><?php echo $fields['shipping_first_name']['label']; ?></label>
						<input type="text" name="shipping_first_name" value="<?php echo $checkout->get_value('shipping_first_name'); ?>" class="form-control">
					</div>
					<div class="col">
						<label for="First"><?php echo $fields['shipping_last_name']['label']; ?></label>
						<input type="text" name="shipping_last_name" value="<?php echo $checkout->get_value('shipping_last_name'); ?>" class="form-control">
					</div>
				</div>

				<div class="form-group">
					<label><?php echo $fields['shipping_address_1']['label']; ?></label>
					<input type="text" name="shipping_address_1" value="<?php echo $checkout->get_value('shipping_address_1'); ?>" class="form-control">
				</div>

				<div class="form-group">
					<label><?php echo $fields['shipping_address_2']['label']; ?></label>
					<input type="text" name="shipping_address_2" value="<?php echo $checkout->get_value('shipping_address_2'); ?>" class="form-control">
				</div>
				
				<div class="row pt-3">
					<!-- <div class="col">
						<label for="First"><?php echo $fields['shipping_postcode']['label']; ?></label>
						<div class="input-group mb-3">
							<input type="text" name="shipping_postcode" value="<?php echo $checkout->get_value('shipping_postcode'); ?>"  class="form-control">
							<div class="input-group-append">
								<span class="input-group-text bg-white" id="basic-addon2">FIND</span>
							</div>
						</div>
					</div> -->
					<div class="col">
						<label for="First"><?php echo $fields['shipping_state']['label']; ?></label>
						<input type="text"  name="shipping_state" value="<?php echo $checkout->get_value('shipping_state'); ?>" class="form-control">
					</div>
				</div>

				<div class="row">
					<div class="col">
						<label><?php echo $fields['shipping_country']['label']; ?></label>
						<select class="form-control" name="shipping_country">
							<option disabled selected>Selct Your Country</option>
							<option> Egypt </option>
						</select>
					</div>
					<div class="col">
						<label for="First"><?php echo $fields['shipping_city']['label']; ?></label>
						<select class="form-control" name="shipping_city">
							<option disabled selected>Selct Your City</option>
							<option> Cairo </option>
						</select>
					</div>
				</div>							
				 
				</div>
			</div>

			<?php do_action( 'woocommerce_after_checkout_shipping_form', $checkout ); ?>

		</div>

	<?php endif; ?>
</div>
<div class="woocommerce-additional-fields">
	<?php do_action( 'woocommerce_before_order_notes', $checkout ); ?>

	<?php if ( apply_filters( 'woocommerce_enable_order_notes_field', 'yes' === get_option( 'woocommerce_enable_order_comments', 'yes' ) ) ) : ?>

		<?php //if ( ! WC()->cart->needs_shipping() || wc_ship_to_shipping_address_only() ) : ?>

			<h3><?php //esc_html_e( 'Additional information', 'woocommerce' ); ?></h3>

		<?php //endif; ?>

		<div class="woocommerce-additional-fields__field-wrapper">
			<?php 		
			// print_r( $checkout->get_checkout_fields( 'order' ))	;
			foreach ( $checkout->get_checkout_fields( 'order' ) as $key => $field ) :
			?>
				<!-- <div class="form-group">
					<label><?php echo $field['label']; ?></label>
					<input type="text" name="<?php echo $key; ?>" value="<?php echo $checkout->get_value($key); ?>" placeholder="<?php echo $field['placeholder']; ?>" class="form-control">
				</div> -->
				<?php //woocommerce_form_field( $key, $field, $checkout->get_value( $key ) ); ?>
			<?php endforeach; ?>
		</div>

	<?php endif; ?>

	<?php do_action( 'woocommerce_after_order_notes', $checkout ); ?>
</div>
