<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );

global $product;

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
 do_action( 'woocommerce_before_main_content' );

 global $post;

// $image =  get_bloginfo('template_url')."/images/cat1.jpg";


if (function_exists('get_wp_term_image')){
    $image = get_wp_term_image(get_queried_object()->term_id);
    if(!$image){
        $thumbnail_id = get_woocommerce_term_meta( get_queried_object()->term_id, 'thumbnail_id', true );
        $image        = wp_get_attachment_url( $thumbnail_id );
    }
    
}
else{
    $thumbnail_id = get_woocommerce_term_meta( get_queried_object()->term_id, 'thumbnail_id', true );
    $image        = wp_get_attachment_url( $thumbnail_id );
}
?>

<style>
.ctegoryHead2 {
	background-image: url("<?php echo $image; ?>");
}
</style>
    <!-- =========================================================== -->
    <div class="ctegoryHead2 mt-5">
        <div style="background: rgba(0, 0, 0, 0.5)" class="pt-3">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7 pb-5">
                        <div class="pt-5 px-4">
                            <h5 class="pt-4 bankGothFontOpacity text-white">SHOP</h5>
                            <h2 class="bankGothFont text-white"><?php woocommerce_page_title(); ?></h2>
                            <p>

                                <?php
                                

                                function get_parent_terms($term) {
                                    if ($term->parent > 0) {
                                        $term = get_term_by("id", $term->parent, "product_cat");
                                    }
                                    else return $term;
                                }

                                $term_list = get_the_terms($post->ID, "product_cat");
                                // print_r($term_list);
                                if($term_list){
                                $count = count($term_list);

                                foreach($term_list as $key => $term){
                                        $cat = get_parent_terms($term);
                                        $cat_id = (int)$cat->term_id;
                                        $cat_name= (string)$cat->name;

                                        if($key != $count-1){
                                            echo " / ";
                                        }
                                ?>
                                        <a class="bankGothFontOpacity text-white" href="<?php echo esc_url(get_term_link ($cat_id, 'product_cat'))?>"><?php echo $cat_name; ?></a>  

                                <?php } } ?>

                                
                            </p>
                            <h6 class="pt-3 pb-3 text-white bankGothFontOpacity"><?php do_action( 'woocommerce_archive_description' ); ?></h6>
                        </div><br><br>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ================================================================ -->
    <div class="jumbotron bg-white mb-0">
        <div class="container border-bottom pb-5">
            <h3 class="commit">Shop LIKE A PROFESSIONAL ATHLETE</h3>
            <p class="entire"><small> That’s why we have a wide range of designs – from classic to modern to wherever your mind takes youy </small></p>
			   <!-- <?php
                $args = array(
                        'taxonomy'     => 'product_cat',
                        'orderby'      => 'id',
                        'order'        => 'ASC',
                        'show_count'   => 0,
                        'pad_counts'   => 0,
                        'hierarchical' => 1,
                        'title_li'     => '',
                        'hide_empty'   => 0 ,
                        'parent'       => 0
                );
				$all_categories = get_categories( $args );
                //  print_r($all_categories); 

            ?>  -->
            <div class="row justify-content-around pt-4 ">
                <!-- <?php
 
					foreach($all_categories as $key => $category){
                        if($key < 3){

						$thumbnail_id = get_woocommerce_term_meta( $category->term_id, 'thumbnail_id', true );
						$image        = wp_get_attachment_url( $thumbnail_id );
				?> -->

					<!-- <div class="col-lg-4 col-md-4 col-sm-6 col-12 pt-2">
                    <a href="<?php echo get_term_link($category); ?>">
						<div class="cardsSingle" style="height:150px;background-image: url('<?php echo $image; ?>');">
								<div class="bottom-right text-left">
									<h6 class="commit text-white mb-0"><?php echo $category->cat_name; ?></h6>
									<p class="text-white"><small> <?php echo mb_strimwidth($category->description, 0, 40, "..."); ?> </small></p>
								</div>
						</div>
                        </a>
					</div>

				<?php }} ?> -->
            </div>

          
            <div class="row pt-5 editArabic">
                
                <div class="col-lg-3">
                    <form method="get" action="" class="form_filter" id="form_filter">
                        <div class="divHide">
                            <h5 class="txtMons  pt-4">Filter Products</h5>

                            <p class="pt-4 colapse" data-toggle="collapse" data-target="#collapseType"><small class="txtMons font-weight-bold">PRICE</small>
                                <i class="far fa-minus-square float-right pt-1"></i>
                            </p>
                            <div id="collapseType" class="collapse show">

                                    <p class="pt-4 txtMons font-weight-bold">
                                        <label for="amount font-weight-bolder">Price per Unit</label>
                                        <input type="text" id="amount" readonly style="border:0; color:red; font-weight:bold;">
                                    </p>
                                    <div id="slider-range"></div>

                            </div>

                            

                            <?php 
                                $all = get_object_taxonomies( array( 'post_type' => 'product') );
                                if(in_array('pa_color' , $all)){
                                
                            ?>
                                <p class="pt-4 colapse" data-toggle="collapse" data-target="#collapseTypeColor">
                                    <small class="txtMons font-weight-bold">COLOR</small>
                                    <i class="far fa-minus-square float-right pt-1"></i>
                                </p>
                                <div id="collapseTypeColor" class="collapse show">
                                    <?php 
                                        $terms = get_terms([
                                            'taxonomy' => 'pa_color',
                                            'hide_empty' => false,
                                        ]);
                                        
                                        foreach($terms as $term){

                                        $colors=$_GET['filter_color'];
                                        $colors_array = explode(',' , $colors);
                                        $checked = "";
                                        if(in_array($term->slug , $colors_array)){
                                            $checked= "checked";
                                        }

                                    ?>
                                        <div class="custom-control custom-checkbox my-1 mr-sm-2">
                                            <input type="checkbox" name="filter_color" <?php echo $checked; ?> class="custom-control-input" id="<?php echo $term->slug; ?>" value="<?php echo $term->slug; ?>">
                                            <label class="custom-control-label" for="<?php echo $term->slug; ?>"><?php echo $term->name; ?></label>
                                        </div>
                                    <?php } ?>
                                </div>

                            <?php } ?>

                            <?php 
                                $all = get_object_taxonomies( array( 'post_type' => 'product') );
                                if(in_array('pa_size' , $all)){
                                
                            ?>
                                <p class="pt-4 colapse" data-toggle="collapse" data-target="#collapseTypeSize"><small class="txtMons font-weight-bold">SIZE</small>
                                    <i class="far fa-minus-square float-right pt-1"></i>
                                </p>
                                <div id="collapseTypeSize" class="collapse show">
                                    <?php 
                                        $terms = get_terms([
                                            'taxonomy' => 'pa_size',
                                            'hide_empty' => false,
                                        ]);
                                        
                                        foreach($terms as $term){

                                        $size=$_GET['filter_size'];
                                        $size_array = explode(',' , $size);
                                        $checked = "";
                                        if(in_array($term->slug , $size_array)){
                                            $checked= "checked";
                                        }
    
                                    ?>
                                        <div class="custom-control custom-checkbox my-1 mr-sm-2">
                                            <input type="checkbox" name="filter_size" <?php echo $checked; ?> class="custom-control-input" id="<?php echo $term->slug; ?>" value="<?php echo $term->slug; ?>">
                                            <label class="custom-control-label" for="<?php echo $term->slug; ?>"><?php echo $term->name; ?></label>
                                        </div>
                                    <?php } ?>
                                </div>

                            <?php } ?>




                          
                            <?php 
                                $args = array(
                                'taxonomy'     => 'product_cat',
                                'orderby'      => 'id',
                                'order'        => 'ASC',
                                'hide_empty'   => 0 ,
                                'parent'       => get_queried_object()->term_id
                                );
                                $all_categories = get_categories( $args );
                                // print_r($all_categories);
                            ?>

                                <p class="pt-4 colapse" data-toggle="collapse" data-target="#collapseTypeCat"><small class="txtMons font-weight-bold">CATEGORY</small>
                                    <i class="far fa-minus-square float-right pt-1"></i>
                                </p>
                                <div id="collapseTypeCat" class="collapse show">
                                    <?php 
                                        
                                        foreach($all_categories as $cat){

                                    ?>
                                    <p class="pl-2 pb-1" >
                                    <a class="text-dark txtMons" href="<?php echo get_term_link ($cat->term_id, 'product_cat');?>"><?php echo $cat->name; ?></a>
                                    </p>
                                    <?php } ?>
                                </div>


                        </div>
                        <a href="#" class="btn shopButton btnFill px-4" data-toggle="modal" data-target="#myModal">Filter Products</a>
                        <div class="modal" id="myModal">
                            <div class="modal-dialog">
                                <div class="modal-content pt-5">
                                    <div class="modal-header pt-5">
                                        <p class="ml-auto pt-2" data-dismiss="modal">Close <i class="fa fa-window-close"></i></p>
                                    </div>
                                    <div class="modal-body">


                                    <p class="pt-4 colapse" data-toggle="collapse" data-target="#collapseStyle">
                                            <small class="txtMons font-weight-bold">PRICE</small>
                                            <i class="far fa-minus-square float-right pt-1"></i>
                                        </p>
                                        <div id="collapseStyle" class="collapse show">

                                            <p class="pt-4 txtMons font-weight-bold">
                                                <label for="amount font-weight-bolder">Price per Unit</label>
                                                <input type="text" id="amount2" readonly style="border:0; color:#f6931f; font-weight:bold;">
                                            </p>
                                            <div id="slider-range2"></div>

                                        </div>


                                    <?php 
                                        $all = get_object_taxonomies( array( 'post_type' => 'product') );
                                        if(in_array('pa_color' , $all)){
                                        
                                    ?>
                                        <p class="colapse" data-toggle="collapse" data-target="#collapseType2">
                                            <small class="txtMons font-weight-bold">COLOR</small>
                                            <i class="far fa-minus-square float-right pt-1"></i>
                                        </p>
                                        <div id="collapseType2" class="collapse show">
                                            <?php 
                                                $terms = get_terms([
                                                    'taxonomy' => 'pa_color',
                                                    'hide_empty' => false,
                                                ]);
                                                
                                                foreach($terms as $term){

                                                $colors=$_GET['filter_color'];
                                                $colors_array = explode(',' , $colors);
                                                $checked = "";
                                                if(in_array($term->slug , $colors_array)){
                                                    $checked= "checked";
                                                }

                                            ?>
                                                <div class="custom-control custom-checkbox my-1 mr-sm-2">
                                                    <input type="checkbox" name="filter_color" <?php echo $checked; ?> class="custom-control-input" id="<?php echo $term->slug; ?>" value="<?php echo $term->slug; ?>">
                                                    <label class="custom-control-label" for="<?php echo $term->slug; ?>"><?php echo $term->name; ?></label>
                                                </div>
                                            <?php } ?>
                                        </div>

                                    <?php } ?>




                                        <?php 
                                            $all = get_object_taxonomies( array( 'post_type' => 'product') );
                                            if(!in_array('pa_size' , $all)){
                                            
                                        ?>
                                            <p class="pt-4 colapse" data-toggle="collapse" data-target="#collapseMain1">
                                                <small  class="txtMons font-weight-bold">SIZE</small>
                                                <i class="far fa-minus-square float-right pt-1"></i>
                                            </p>
                                            <div id="collapseMain1" class="collapse show">
                                                <?php 
                                                    $terms = get_terms([
                                                        'taxonomy' => 'pa_size',
                                                        'hide_empty' => false,
                                                    ]);
                                                    
                                                    foreach($terms as $term){

                                                ?>
                                                    <div class="custom-control custom-checkbox my-1 mr-sm-2">
                                                        <input type="checkbox" class="custom-control-input" id="<?php echo $term->slug; ?>" value="<?php echo $term->slug; ?>">
                                                        <label class="custom-control-label" for="<?php echo $term->slug; ?>"><?php echo $term->name; ?></label>
                                                    </div>
                                                <?php } ?>
                                            </div>

                                        <?php } ?>

                                        
                                   
                                        

                                        <?php 
                                            $args = array(
                                            'taxonomy'     => 'product_cat',
                                            'orderby'      => 'id',
                                            'order'        => 'ASC',
                                            'hide_empty'   => 0 ,
                                            );
                                            $all_categories = get_categories( $args );
                                            // print_r($all_categories);
                                        ?>

                                            <p class="pt-4 colapse" data-toggle="collapse" data-target="#collapseMain2">
                                                <small class="txtMons font-weight-bold">CATEGORY</small>
                                                <i class="far fa-minus-square float-right pt-1"></i>
                                            </p>
                                            <div id="collapseMain2" class="collapse show">
                                                <?php 
                                                    
                                                    foreach($all_categories as $cat){

                                                ?>
                                                   <p class="pl-2 pb-1" >
                                                    <a class="text-dark txtMons" href="<?php echo get_term_link ($cat->term_id, 'product_cat');?>"><?php echo $cat->name; ?></a>
                                                    </p>
                                                <?php } ?>
                                            </div>



                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <?php
                    if ( woocommerce_product_loop() ) {
                ?>
                <div class="col-lg-9">
                        <div class="row pt-3">
                            <div class="col-lg-8 col-sm-6 col-12">
                                <h5 class="bankGothFont"><?php woocommerce_page_title(); ?></h5>
                                <p class="bankGothFontOpacity"><?php echo mb_strimwidth( get_the_excerpt(), 0, 100, "..."); ?></p>
                            </div>

                            <div class="col-lg-4 col-sm-6 col-12">
					<?php 
									do_action( 'woocommerce_before_shop_loop' );
					?>
                            </div>
                        </div>
					<?php
							
							woocommerce_product_loop_start();

						if ( wc_get_loop_prop( 'total' ) ) { ?>

							<div class="row py-4 justify-content-around">

                                <?php
                                
                                function so_27975262_product_query( $q ){ 
                                    $q->set( array('tax_query' => array( array(
                                        'taxonomy'      => 'products_type',
                                        'field'         => 'slug', // can be 'term_id', 'slug' or 'name'
                                        'terms'         => 'short',
                                    ),),)); 
                                }
                                add_action( 'woocommerce_product_query', 'so_27975262_product_query' ); 

								while ( have_posts() ) { the_post();

									global $product;

									$image = wp_get_attachment_image_src( get_post_thumbnail_id( $product->id ), 'single-post-thumbnail' );
                                    $attachment_ids = $product->get_gallery_image_ids();
                                    $fullImage =  wp_get_attachment_url($attachment_ids[0], 'full') ? wp_get_attachment_url($attachment_ids[0], 'full'): $image;
									/**
									 * Hook: woocommerce_shop_loop.
									 */
									do_action( 'woocommerce_shop_loop' );
									?>

									<div class="col-lg-4 col-md-6 col-sm-6 col-12 pt-3 pb-3">
                                        <a href="<?php the_permalink(); ?>"> 
                                            <div class="text-center newArrival">
                                                <img src="<?php echo $image[0]; ?>" class="mx-auto img-fluid d-block swapImage" data-swapImage="<?php echo  $fullImage; ?>">
                                                <?php if ( $product->is_on_sale() ){?>
                                                    <p class="top-right text-white">SALE</p>
                                                <?php } ?>
                                            </div>
                                        </a>
                                        <p class="commit text-center mb-0 pt-2"><?php the_title(); ?></p>
                                        <p class="text-center">
                                            <a href="<?php the_permalink(); ?>" class="text-danger text-center salary">
                                                <?php // echo  $product->get_price_html(); ?>
                                                <?php echo woocommerce_price($product->get_price()); ?>
                                            </a>
                                        </p>   
									</div>

								<?php
								} 
								
								?>
							</div>
									
					<?php 

						}
							woocommerce_product_loop_end();
					?>
                </div>
                <?php
                } else { ?>
                    <div class="col-lg-9">
                        <?php do_action( 'woocommerce_no_products_found' ); ?>
                    </div>
                <?php } ?>
            </div>
            
        </div>
    </div>



    <div class="container">
        <div class="collection">
            <h4 class="entire">COLLECTION</h4>
            <h3 class="commit">BEST SELLERS</h3>
        </div>
        <div class="swiper-container container pb-5 pt-5"><br>
            <div class="swiper-wrapper">
            <?php
                $args = array(
                    'posts_per_page'   => 10,
                    'orderby'          => 'date',
                    'order'            => 'DESC',
                    'post_type'        => 'product'
                ); 

                $random_products = get_posts( $args );

                foreach ( $random_products as $post ) : setup_postdata( $post ); 
                global $product;
                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $product->id ), 'single-post-thumbnail' );
                $attachment_ids = $product->get_gallery_image_ids();
                $fullImage =  wp_get_attachment_url($attachment_ids[0], 'full') ? wp_get_attachment_url($attachment_ids[0], 'full'): $image;
                ?>
                <div class="swiper-slide">
                    <div class="row py-3 justify-content-around">
                        <div class="col-lg-9 col-md-6 col-sm-12 col-12 pt-3">
                            <a href="<?php the_permalink(); ?>"> 
                                <div class="text-center newArrival">
                                    <img src="<?php echo $image[0]; ?>" data-swapImage="<?php echo  $fullImage; ?>" class="mx-auto img-fluid d-block swapImage" >
                                    <?php if ( $product->is_on_sale() ){?>
                                        <p class="top-right text-white">SALE</p>
                                    <?php } ?>
                                </div>
                            </a>
                            <p class="commit text-center mb-0"><?php the_title(); ?></p>
                            <p class="text-center">
                                <a href="<?php the_permalink(); ?>" class="text-danger text-center salary">
                                    <?php // echo  $product->get_price_html(); ?>
                                    <?php echo woocommerce_price($product->get_price()); ?>
                                </a>
                            </p>   
                        </div>
                    </div>
                </div>
            <?php endforeach; 
            wp_reset_postdata();
            ?>
                
            </div>
            <!-- Add Pagination -->
            <div class="swiper-pagination">
                <img src="<?php bloginfo('template_url'); ?>/images/next.svg">
            </div>
            <div class="swiper-button-next"><img src="<?php bloginfo('template_url'); ?>/images/right.svg"></div>
            <div class="swiper-button-prev"><img src="<?php bloginfo('template_url'); ?>/images/left.svg"></div>
        </div><br>
    </div>


<?php
get_footer( 'shop' );
