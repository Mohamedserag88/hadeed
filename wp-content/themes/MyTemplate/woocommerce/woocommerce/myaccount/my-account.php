<?php
/**
 * My Account page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

defined( 'ABSPATH' ) || exit;

global $current_user;

$user = $current_user;
/**
 * My Account navigation.
 *
 * @since 2.6.0
 */
// do_action( 'woocommerce_account_navigation' ); ?>

<!-- <div class="woocommerce-MyAccount-content"> -->
	<?php
		/**
		 * My Account content.
		 *
		 * @since 2.6.0
		 */
		// do_action( 'woocommerce_account_content' );

		if ( ! is_user_logged_in() ) {
			// echo esc_html( apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) ) );
			wp_redirect(get_bloginfo('siteurl')."/my-account");
		}
		
		// wc_print_notices();

	?>
<!-- </div> -->

<?php do_action( 'woocommerce_before_edit_account_form' ); ?>


	<?php do_action( 'woocommerce_edit_account_form_start' ); ?>


	<div class="container my-5">
        <div class="row pt-5 justify-content-around">
            <div class="col-lg-5 wow fadeIn acountAr">
                <p class="pt-5"><small class="bankGothFontOpacity"> Details </small></p>
                <h5 class="bankGothFont">MY ACCOUNT</h5>
				<form class="edit-account formCheckOut pt-2" action="" method="post" <?php do_action( 'woocommerce_edit_account_form_tag' ); ?> >

                    <div class="row">
                        <div class="col">
                            <label for="account_first_name"><?php esc_html_e( 'FIRST NAME', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
                            <input type="text" class="form-control"  name="account_first_name" id="account_first_name" autocomplete="given-name" value="<?php echo esc_attr( $user->first_name ); ?>">
                        </div>
                        <div class="col">
							<label for="account_last_name"><?php esc_html_e( 'LAST NAME', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
							<input type="text" class="form-control" name="account_last_name" id="account_last_name" autocomplete="family-name" value="<?php echo esc_attr( $user->last_name ); ?>" />
						</div>
					</div>

					<div class="row">
                        <div class="col">
							<label for="account_display_name"><?php esc_html_e( 'Display name', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
							<input type="text" class="form-control" name="account_display_name" id="account_display_name" value="<?php echo esc_attr( $user->display_name ); ?>" /> <span><em><?php esc_html_e( 'This will be how your name will be displayed in the account section and in reviews', 'woocommerce' ); ?></em></span>
                        </div>
                        <div class="col">
							<label for="account_email"><?php esc_html_e( 'Email address', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
							<input type="email" class="form-control" name="account_email" id="account_email" autocomplete="email" value="<?php echo esc_attr( $user->user_email ); ?>" />
						</div>
					</div>

                    <div class="form-group pt-3">
						<label for="password_current"><?php esc_html_e( 'Current password (leave blank to leave unchanged)', 'woocommerce' ); ?></label>
						<input type="password" class="form-control" name="password_current" id="password_current" autocomplete="off" />
                    </div>
                    <div class="form-group">
						<label for="password_1"><?php esc_html_e( 'New password (leave blank to leave unchanged)', 'woocommerce' ); ?></label>
						<input type="password" class="form-control" name="password_1" id="password_1" autocomplete="off" />
                    </div>
                    <div class="form-group">
						<label for="password_2"><?php esc_html_e( 'Confirm new password', 'woocommerce' ); ?></label>
						<input type="password" class="form-control" name="password_2" id="password_2" autocomplete="off" />
					</div>
					
					<?php do_action( 'woocommerce_edit_account_form' ); ?>

						<?php wp_nonce_field( 'save_account_details', 'save-account-details-nonce' ); ?>
						<button type="submit" class="btn btnContinue px-5 mt-4 rounded-0" name="save_account_details" value="<?php esc_attr_e( 'Save changes', 'woocommerce' ); ?>"><?php esc_html_e( 'Save changes', 'woocommerce' ); ?></button>
						<input type="hidden" name="action" value="save_account_details" />

					<?php do_action( 'woocommerce_edit_account_form_end' ); ?>
                    
                </form>
            </div>
            <div class="col-lg-5 pt-5">
                <img src="<?php bloginfo('template_url'); ?>/images/registration.jpg" class="img-fluid wow fadeIn">
            </div>
        </div>
    </div>

<?php do_action( 'woocommerce_after_edit_account_form' ); ?>




		