<?php
/**
 * Edit address form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-edit-address.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

$page_title = ( 'billing' === $load_address ) ? esc_html__( 'Billing address', 'woocommerce' ) : esc_html__( 'Shipping address', 'woocommerce' );

do_action( 'woocommerce_before_edit_account_address_form' ); ?>

<?php if ( ! $load_address ) : ?>
	<?php  wc_get_template( 'myaccount/my-address.php' ); ?>
<?php else : ?>

	<div class="container delivery pt-5 mb-5">

			<form method="post" class="formCheckOut">

				<h3  class="bankGothFont pt-5 mb-5"><?php echo apply_filters( 'woocommerce_my_account_edit_address_title', $page_title, $load_address ); ?></h3><?php // @codingStandardsIgnoreLine ?>

				<!-- <div class="woocommerce-address-fields"> -->
					<?php //do_action( "woocommerce_before_edit_address_form_{$load_address}" ); ?>

					<!-- <div class="woocommerce-address-fields__field-wrapper"> -->
						<?php
					// 	echo "<pre>";
					//  print_r($address);
					//  echo "</pre>";
						// foreach ( $address as $key => $field ) {
						// 	 echo $field['value'];
						// 	 echo $key;
						// 	// woocommerce_form_field( $key, $field, wc_get_post_data_by_key( $key, $field['value'] ) );
						// }
						?>
					<!-- </div> -->

					<?php //do_action( "woocommerce_after_edit_address_form_{$load_address}" ); ?>

					<p>
						<!-- <button type="submit" class="button" name="save_address" value="<?php esc_attr_e( 'Save address', 'woocommerce' ); ?>"><?php esc_html_e( 'Save address', 'woocommerce' ); ?></button>
						<?php //wp_nonce_field( 'woocommerce-edit_address', 'woocommerce-edit-address-nonce' ); ?>
						<input type="hidden" name="action" value="edit_address" /> -->
					</p>
				<!-- </div> -->

					<div class="row pt-4 pb-3">
						<div class="col">
							<label for="First">First Name</label>
							<input type="text" name="<?php echo $load_address;?>_first_name" value="<?php echo $address[$load_address.'_first_name']['value']; ?>" class="form-control">
						</div>
						<div class="col">
							<label for="First">Last Name</label>
							<input type="text" name="<?php echo $load_address;?>_last_name" value="<?php echo $address[$load_address.'_last_name']['value']; ?>" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<label>ADDRESS 1</label>
						<input type="text" name="<?php echo $load_address;?>_address_1" value="<?php  echo $address[$load_address.'_address_1']['value']; ?>" class="form-control">
					</div>
					<div class="form-group">
						<label>ADDRESS 2</label>
						<input type="text" name="<?php echo $load_address;?>_address_2" value="<?php  echo $address[$load_address.'_address_2']['value']; ?>" class="form-control">
					</div>

							
					<div class="row">
						<div class="col">	
							<?php 
								global $woocommerce;
								$countries_obj   = new WC_Countries();
								$countries   = $countries_obj->__get('countries');
							?>				
							<label>Country</label>
							<select class="form-control" name="<?php echo $load_address;?>_country">
								<option disabled selected>Selct Your Country</option>

								<?php
								
								foreach($countries as $key => $country){ ?>
								<option <?php echo $address[$load_address.'_country']['value']== $key ?  "selected" : ""; ?> value="<?php echo $key; ?>"> <?php echo $country; ?> </option>
								<?php } ?>
							</select>
							

						</div>
						<div class="col">
							<label for="First">City</label>
							<input type="text" name="<?php echo $load_address;?>_city" value="<?php echo $address[$load_address.'_city']['value']; ?>" class="form-control">

						</div>
					</div>
					<?php if($load_address == "billing"){ ?>
					<div class="row pt-4 pb-3">
						<div class="col">
							<label for="First">Email</label>
							<input type="text" name="<?php echo $load_address;?>_email" value="<?php echo $address[$load_address.'_email']['value']; ?>" class="form-control">
						</div>
						<div class="col">
							<label for="First">Phone number</label>
							<input type="text" name="<?php echo $load_address;?>_phone" value="<?php echo $address[$load_address.'_phone']['value']; ?>"  class="form-control">
						</div>
					</div>
					<?php } ?>

					<div class="row pt-3">
						<div class="col">
							<label for="First">POSTAL CODE</label>
							<div class="input-group mb-3">
								<input type="text" name="<?php echo $load_address;?>_postcode" value="<?php echo $address[$load_address.'_postcode']['value']; ?>"  class="form-control">
							</div>
						</div>
						<div class="col">
							<label for="First">State</label>
							<input type="text" name="<?php echo $load_address;?>_state" value="<?php echo $address[$load_address.'_state']['value']; ?>" class="form-control">
						</div>
					</div>
				
					<p>
						<button type="submit" class="button" name="save_address" value="<?php esc_attr_e( 'Save address', 'woocommerce' ); ?>">
						<?php esc_html_e( 'Save address', 'woocommerce' ); ?></button>
						<?php wp_nonce_field( 'woocommerce-edit_address', 'woocommerce-edit-address-nonce' ); ?>
						<input type="hidden" name="action" value="edit_address" />
					</p>
			</form>
	</div>

<?php endif; ?>

<?php do_action( 'woocommerce_after_edit_account_address_form' ); ?>
