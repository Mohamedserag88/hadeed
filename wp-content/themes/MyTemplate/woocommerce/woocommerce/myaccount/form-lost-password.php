<?php
/**
 * Lost password form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-lost-password.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.2
 */

defined( 'ABSPATH' ) || exit;

do_action( 'woocommerce_before_lost_password_form' );
?>

		<div class="container my-5">
			<div class="row pt-5 justify-content-around">
				<div class="col-lg-5 wow fadeIn">
					<p class="pt-5"><small class="bankGothFontOpacity"> FORGOT PASSWORD </small></p>
					<h5 class="bankGothFont">Lost your password?</h5>
					 <form class="formCheckOut pt-2" method="post"> 

						<?php do_action( 'woocommerce_login_form_start' ); ?>

							<div class="form-group">
								<label>USERNAME Or Email</label>
								<input type="text"name="user_login" id="user_login" autocomplete="username" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" class="form-control">
							</div>
						
						<?php do_action( 'woocommerce_lostpassword_form' ); ?>

						<p class="woocommerce-form-row form-row">
							<input type="hidden" name="wc_reset_password" value="true" />
							<button type="submit"  class="btn btnContinue px-3 mt-4 rounded-0" value="<?php esc_attr_e( 'Reset password', 'woocommerce' ); ?>"><?php esc_html_e( 'Reset password', 'woocommerce' ); ?></button>
						</p>

						<?php wp_nonce_field( 'lost_password', 'woocommerce-lost-password-nonce' ); ?>



						            
						<p class="pt-5"><a href="#" class="bankGothFontOpacity">Have Accouunt?</a>
						<a href="<?php echo get_site_url(); ?>/my-account" class="bankGothFont text-dark">LOGIN NOW</a></p>
						
						
					</form>
					<!-- <button class="btn btn-block btnFace py-3 my-5 rounded-0"><i class="fab fa-facebook-f px-4"></i>LOGIN WITH FACEBOOK</button> -->
				</div>
				<div class="col-lg-5">
					<img src="<?php bloginfo('template_url'); ?>/images/imgLogin.jpg" class="img-fluid wow fadeIn">
				</div>
			</div>
		</div>

<?php
do_action( 'woocommerce_after_lost_password_form' );
