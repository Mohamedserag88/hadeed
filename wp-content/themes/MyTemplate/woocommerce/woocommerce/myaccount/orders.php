<?php
/**
 * Orders
 *
 * Shows orders on the account page.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/orders.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;


$customer_orders = get_posts(
	apply_filters(
		'woocommerce_my_account_my_orders_query',
		array(
			'numberposts' => $order_count,
			'meta_key'    => '_customer_user',
			'meta_value'  => get_current_user_id(),
			'post_type'   => wc_get_order_types('view-orders'),
			'post_status' => array_keys(wc_get_order_statuses()),
		)
	)
);

?>


<div class="container py-5 pt-5 mt-5"><br><br>
        <h6 class="bankGothFontOpacity txtRight pt-5">HISTORY</h6>
		<h3 class="bankGothFont txtRight">MY ORDERS</h3>
		

	<?php   
	  if ($customer_orders) :

		foreach ($customer_orders as $customer_order) {
			$order = wc_get_order($customer_order);
			$item_count = $order->get_item_count();
			$actions = wc_get_account_orders_actions($order);
	?>


        <div class="card cardOrders mt-5">
            <div class="card-header bg-white py-0">
                <div class="row">
                    <div class="col-lg-3 col-sm-4 col-12 border-right">
                        <p class="pt-3 pb-1 bankGothFont">ORDER ID: <span class="text-danger"> <?php echo _x('#', 'hash before order number', 'woocommerce') . $order->get_order_number(); ?> </span class=""></p>
                    </div>
                    <div class="col-lg-3 col-sm-4 col-12">
                        <p class="pt-3 pb-1 bankGothFont">PAID BY : <?php echo wp_kses_post( $order->get_payment_method_title() ); ?></p>
                    </div>
                    <div class="col-lg-6 col-sm-4 col-12 border-left">
                        <p class="text-rightT pt-3 pb-1 bankGothFont">DATE: <span class="text-danger"> <?php echo esc_html(wc_format_datetime($order->get_date_created())); ?> </span></p>
                    </div>
                </div>
			</div>
			
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6">
						<p class="ml-2 bankGothFontOpacity">ITEMS</p>
						
						<?php
							if (!$order) {
								return;
							}

							$order_items  = $order->get_items(apply_filters('woocommerce_purchase_order_item_types', 'line_item'));                                                    
							foreach ($order_items as $item_id => $item) {
								$product = $item->get_product();
						?>
						<!-- <a href="<?php echo $product_permalink; ?>"> -->
                        <div class="media pt-3">
                            <img src="<?php echo wp_get_attachment_image_url( $product->get_image_id(), 'full' );?>" class="mr-3" width="13%" height="13%">
                            <div class="media-body">

							<?php 
								$qty          = $item->get_quantity();
								$refunded_qty = $order->get_qty_refunded_for_item($item_id);

								if ($refunded_qty) {
									$qty_display = '<del>' . esc_html($qty) . '</del> <ins>' . esc_html($qty - ($refunded_qty * -1)) . '</ins>';
								} else {
									$qty_display = esc_html($qty);
								}
							   ?>

							  <h6 class="mt-1 commit"><?php echo  $qty_display ." X " .   $item->get_name(); ?></h6>
							  
							 
							  <p><?php echo $order->get_formatted_line_subtotal($item); ?></p>
							  <?php
								// echo apply_filters( 'woocommerce_order_item_name', $product_permalink ? sprintf( '<a href="%s">%s</a>', $product_permalink, $item->get_name() ) : $item->get_name(), $item, $is_visible ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
								// echo apply_filters('woocommerce_order_item_quantity_html', ' <strong class="product-quantity">' . sprintf('&times;&nbsp;%s', $qty_display) . '</strong>', $item); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
							?>
                            </div>
						</div>
						<!-- </a> -->

						<?php } ?>
						
                    </div>
                    <div class="col-lg-3">
                        <p class="text-center bankGothFontOpacity">STATUS</p>
                        <h5 class="py-5 text-center bankGothFont text-success"><?php echo  $order->get_status(); ?></h5>
                    </div>
                    <div class="col-lg-3 text-center">
					<p class="bankGothFontOpacity">Sell Date </p> 
                        <!-- <h6 class="pt-4 bankGothFont">WEDNESDAY</h6> -->
                        <h4 class="bankGothFont"> <?php echo esc_html(wc_format_datetime($order->get_date_created())); ?> </h4>
                    </div>
                </div> 
			</div>
			
            <div class="card-footer bg-white">
                <div class="row">
                    <div class="col-lg-9">
                        <h6 class="pt-3 bankGothFont">ORDER GRAND TOTAL</h6>
                    </div>
                    <div class="col-lg-3 text-center">
                        <p class="mb-0 bankGothFont">TOTAL</p>
                        <h3 class="pt-0 bankGothFont">
							<?php  echo $order->get_formatted_order_total(); ?>
						</h3>
                    </div>
                </div>
            </div>
		</div>
		
	<?php 
		}

	 endif;
	?>
       
    </div>