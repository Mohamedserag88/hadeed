<?php
/**
 * My Orders - Deprecated
 *
 * @deprecated 2.6.0 this template file is no longer used. My Account shortcode uses orders.php.
 * @package WooCommerce/Templates
 */

defined( 'ABSPATH' ) || exit;

$my_orders_columns = apply_filters(
	'woocommerce_my_account_my_orders_columns',
	array(
		'order-number'  => esc_html__( 'Order', 'woocommerce' ),
		'order-date'    => esc_html__( 'Date', 'woocommerce' ),
		'order-status'  => esc_html__( 'Status', 'woocommerce' ),
		'order-total'   => esc_html__( 'Total', 'woocommerce' ),
		'order-actions' => '&nbsp;',
	)
);

$customer_orders = get_posts(
	apply_filters(
		'woocommerce_my_account_my_orders_query',
		array(
			'numberposts' => $order_count,
			'meta_key'    => '_customer_user',
			'meta_value'  => get_current_user_id(),
			'post_type'   => wc_get_order_types( 'view-orders' ),
			'post_status' => array_keys( wc_get_order_statuses() ),
		)
	)
);

if ( $customer_orders ) : ?>

	<h2><?php echo apply_filters( 'woocommerce_my_account_my_orders_title', esc_html__( 'Recent orders', 'woocommerce' ) ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></h2>

	<table class="shop_table shop_table_responsive my_account_orders">

		<thead>
			<tr>
				<?php foreach ( $my_orders_columns as $column_id => $column_name ) : ?>
					<th class="<?php echo esc_attr( $column_id ); ?>"><span class="nobr"><?php echo esc_html( $column_name ); ?></span></th>
				<?php endforeach; ?>
			</tr>
		</thead>

		<tbody>
			<?php
			foreach ( $customer_orders as $customer_order ) :
				$order      = wc_get_order( $customer_order ); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.OverrideProhibited
				$item_count = $order->get_item_count();
				?>
				<tr class="order">
					<?php foreach ( $my_orders_columns as $column_id => $column_name ) : ?>
						<td class="<?php echo esc_attr( $column_id ); ?>" data-title="<?php echo esc_attr( $column_name ); ?>">
							<?php if ( has_action( 'woocommerce_my_account_my_orders_column_' . $column_id ) ) : ?>
								<?php do_action( 'woocommerce_my_account_my_orders_column_' . $column_id, $order ); ?>

							<?php elseif ( 'order-number' === $column_id ) : ?>
								<a href="<?php echo esc_url( $order->get_view_order_url() ); ?>">
									<?php echo _x( '#', 'hash before order number', 'woocommerce' ) . $order->get_order_number(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
								</a>

							<?php elseif ( 'order-date' === $column_id ) : ?>
								<time datetime="<?php echo esc_attr( $order->get_date_created()->date( 'c' ) ); ?>"><?php echo esc_html( wc_format_datetime( $order->get_date_created() ) ); ?></time>

							<?php elseif ( 'order-status' === $column_id ) : ?>
								<?php echo esc_html( wc_get_order_status_name( $order->get_status() ) ); ?>

							<?php elseif ( 'order-total' === $column_id ) : ?>
								<?php
								/* translators: 1: formatted order total 2: total order items */
								printf( _n( '%1$s for %2$s item', '%1$s for %2$s items', $item_count, 'woocommerce' ), $order->get_formatted_order_total(), $item_count ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
								?>

							<?php elseif ( 'order-actions' === $column_id ) : ?>
								<?php
								$actions = wc_get_account_orders_actions( $order );

								if ( ! empty( $actions ) ) {
									foreach ( $actions as $key => $action ) { // phpcs:ignore WordPress.WP.GlobalVariablesOverride.OverrideProhibited
										echo '<a href="' . esc_url( $action['url'] ) . '" class="button ' . sanitize_html_class( $key ) . '">' . esc_html( $action['name'] ) . '</a>';
									}
								}
								?>
							<?php endif; ?>
						</td>
					<?php endforeach; ?>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
<?php endif; ?>



<div class="container py-5"><br><br>
        <h6 class="bankGothFontOpacity txtRight pt-5">HISTORY</h6>
		<h3 class="bankGothFont txtRight">MY ORDERS</h3>
		

	<?php   
	  if ($customer_orders) :

		foreach ($customer_orders as $customer_order) {
			$order = wc_get_order($customer_order);
			$item_count = $order->get_item_count();
			$actions = wc_get_account_orders_actions($order);
	?>


        <div class="card cardOrders mt-5">
            <div class="card-header bg-white py-0">
                <div class="row">
                    <div class="col-lg-3 col-sm-4 col-12 border-right">
                        <p class="pt-3 pb-1 bankGothFont">ORDER ID: <span class="text-danger"> <?php echo _x('#', 'hash before order number', 'woocommerce') . $order->get_order_number(); ?> </span class=""></p>
                    </div>
                    <div class="col-lg-3 col-sm-4 col-12">
                        <p class="pt-3 pb-1 bankGothFont">PAID BY : <?php echo wp_kses_post( $order->get_payment_method_title() ); ?></p>
                    </div>
                    <div class="col-lg-6 col-sm-4 col-12 border-left">
                        <p class="text-rightT pt-3 pb-1 bankGothFont">DATE: <span class="text-danger"> <?php echo esc_html(wc_format_datetime($order->get_date_created())); ?> </span></p>
                    </div>
                </div>
			</div>
			
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6">
						<p class="ml-2 bankGothFontOpacity">ITEMS</p>
						
						<?php
							if (!$order) {
								return;
							}

							$order_items  = $order->get_items(apply_filters('woocommerce_purchase_order_item_types', 'line_item'));                                                    
							foreach ($order_items as $item_id => $item) {
								$product = $item->get_product();
						?>
						<!-- <a href="<?php echo $product_permalink; ?>"> -->
                        <div class="media pt-3">
                            <img src="<?php echo wp_get_attachment_image_url( $product->get_image_id(), 'full' );?>" class="mr-3" width="13%" height="13%">
                            <div class="media-body">

							<?php 
								$qty          = $item->get_quantity();
								$refunded_qty = $order->get_qty_refunded_for_item($item_id);

								if ($refunded_qty) {
									$qty_display = '<del>' . esc_html($qty) . '</del> <ins>' . esc_html($qty - ($refunded_qty * -1)) . '</ins>';
								} else {
									$qty_display = esc_html($qty);
								}
							   ?>

							  <h6 class="mt-1 commit"><?php echo  $qty_display ." X " .   $item->get_name(); ?></h6>
							  
							 
							  <p><?php echo $order->get_formatted_line_subtotal($item); ?></p>
							  <?php
								// echo apply_filters( 'woocommerce_order_item_name', $product_permalink ? sprintf( '<a href="%s">%s</a>', $product_permalink, $item->get_name() ) : $item->get_name(), $item, $is_visible ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
								// echo apply_filters('woocommerce_order_item_quantity_html', ' <strong class="product-quantity">' . sprintf('&times;&nbsp;%s', $qty_display) . '</strong>', $item); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
							?>
                            </div>
						</div>
						<!-- </a> -->

						<?php } ?>
						
                    </div>
                    <div class="col-lg-3">
                        <p class="text-center bankGothFontOpacity">STATUS</p>
                        <h5 class="py-5 text-center bankGothFont text-success"><?php echo  $order->get_status(); ?></h5>
                    </div>
                    <div class="col-lg-3 text-center">
					<p class="bankGothFontOpacity">Sell Date </p> 
                        <!-- <h6 class="pt-4 bankGothFont">WEDNESDAY</h6> -->
                        <h4 class="bankGothFont"> <?php echo esc_html(wc_format_datetime($order->get_date_created())); ?> </h4>
                    </div>
                </div> 
			</div>
			
            <div class="card-footer bg-white">
                <div class="row">
                    <div class="col-lg-9">
                        <h6 class="pt-3 bankGothFont">ORDER GRAND TOTAL</h6>
                    </div>
                    <div class="col-lg-3 text-center">
                        <p class="mb-0 bankGothFont">TOTAL</p>
                        <h3 class="pt-0 bankGothFont">
							<?php  echo $order->get_formatted_order_total(); ?>
						</h3>
                    </div>
                </div>
            </div>
		</div>
		
	<?php 
		}

	 endif;
	?>
       
    </div>