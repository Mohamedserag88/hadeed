<?php get_header();

$cat = get_term_by( 'id', $_GET['cat'], 'product_cat' );

//  print_r($cat);

?>
    <!-- =========================================================== -->
    <div class="ctegoryHead pt-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 pb-5">
                    <div class="introduction pt-5 pb-5">
                        <!-- <h5 class="pt-4">SHOP</h5> -->
                        <h2 class="pt-3"><?php echo $cat->name; ?></h2>
                        <h6 class="pt-3 pb-3"><?php echo $cat->description; ?></h6>
                    </div><br><br>
                </div>
            </div>
        </div>
    </div>
    <!-- ================================================================ -->
    <div class="jumbotron bg-white mb-0">
        <div class="container">
            <h3 class="commit text-center">OUR BOLDEST COLLECTIONS</h3>
            <p class="entire text-center">your entire life is on the line, try to take it more lightly</p>
            <?php
                $i = 1;
                $args = array(
                        'taxonomy'     => 'product_cat',
                        'orderby'      => 'id',
                        'order'        => 'ASC',
                        'show_count'   => 0,
                        'pad_counts'   => 0,
                        'hierarchical' => 1,
                        'title_li'     => '',
                        'hide_empty'   => 0 ,
                        'parent'       => $_GET['cat']
                );
                $all_categories = get_categories( $args );
                
                        
            ?> 
            <div class="row justify-content-around pt-4">
                <?php  
                    foreach ($all_categories as $key => $cat) { 
                    $thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true ); 
                    $image = wp_get_attachment_url( $thumbnail_id );
                ?>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-12 pt-2">
                        <div class="cards">
                        <a href="<?php echo get_term_link ($cat->term_id, 'product_cat');?>">

                            <img src="<?php echo $image; ?>" width="100%" height="400px">
                            <div class="bottom-right text-left">
                                <h3 class="commit text-white mb-0"><?php echo $cat->name; ?></h3>
                                <p class="text-white"><?php echo $cat->description; ?></p>
                            </div>
                        </a>
                        </div>
                    </div>
                <?php
                    } 

                ?>
            </div>
        </div>
    </div>

    <!-- ========================================[best sellers]==================================== -->
            <?php 

                $meta_query  = WC()->query->get_meta_query();
                $tax_query   = WC()->query->get_tax_query();
                $tax_query[] = array(
                    'taxonomy' => 'product_visibility',
                    'field'    => 'name',
                    'terms'    => 'featured',
                    'operator' => 'IN',
                );
                
                $args = array(
                    'post_type'           => 'product',
                    'post_status'         => 'publish',
                    'posts_per_page'      => 10,
                    'meta_query'          => $meta_query,
                    'tax_query'           => $tax_query,
                );
                
                $featured_query = new WP_Query( $args );
                    
                if ($featured_query->have_posts()) : ?>
                
                <div class="container pt-5">
                    <div class="collection">
                        <h4 class="entire">COLLECTION</h4>
                        <h3 class="commit">BEST SELLERS</h3>
                    </div>
                    <div class="swiper-container container pb-5 pt-5"><br>
                        <div class="swiper-wrapper">

                <?php
                    while ($featured_query->have_posts()) : 
                    
                        $featured_query->the_post();
                        
                        $product = get_product( $featured_query->post->ID );
                        // global $product;
                        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $product->id ), 'single-post-thumbnail' );
                   
                ?>

                        <div class="swiper-slide">
                            <div class="row py-3 justify-content-around">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-12 pt-3">
                                <a href="<?php the_permalink(); ?>"> 
                                    <div class="text-center newArrival">
                                        <img src="<?php echo $image[0]; ?>" class="mx-auto img-fluid d-block">
                                        <?php if ( $product->is_on_sale() ){?>
                                            <p class="top-right text-white">SALE</p>
                                        <?php } ?>
                                    </div>
                                </a>
                                <p class="commit text-center mb-0"><?php the_title(); ?></p>
                                <p class="text-center">
                                    <a href="<?php the_permalink(); ?>" class="text-danger text-center salary">
                                        <?php // echo  $product->get_price_html(); ?>
                                        <?php echo woocommerce_price($product->get_price()); ?>
                                    </a>
                                </p>
                                </div>
                            </div>
                        </div>

                <?php
                
                    endwhile;
                ?>
                        </div>
                        <!-- Add Pagination -->
                        <div class="swiper-pagination">
                            <img src="<?php echo get_bloginfo('template_url');?>/images/next.svg">
                        </div>
                        <div class="swiper-button-next"><img src="<?php echo get_bloginfo('template_url');?>/images/right.svg"></div>
                        <div class="swiper-button-prev"><img src="<?php echo get_bloginfo('template_url');?>/images/left.svg"></div>
                    </div><br>
                </div>
                
                <?php

                endif;
                
                ?>

       

    <!---------------------------- Footer --------------------------------->
    <?php get_footer(); ?>


</body>

</html>