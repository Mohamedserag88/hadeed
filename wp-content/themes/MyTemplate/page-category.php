<?php get_header(); 
?>
    <!-- ================================================================ -->
    <div class="jumbotron bg-white mb-0 catgs">
        <div class="container">
        <h3 class="commit text-center pt-5">OUR BOLDEST COLLECTIONS</h3>
            <p class="entire text-center">your entire life is on the line, try to take it more lightly</p>
            <?php
                $args = array(
                        'taxonomy'     => 'product_cat',
                        'orderby'      => 'id',
                        'order'        => 'ASC',
                        'show_count'   => 0,
                        'pad_counts'   => 0,
                        'hierarchical' => 1,
                        'title_li'     => '',
                        'hide_empty'   => 0 ,
                );
                $all_categories = get_categories( $args );
                
            ?> 
            <div class="row pt-5">
                <div class="col-lg-7 pt-4">
                    <?php 
                    foreach ($all_categories as $key => $cat) { 
                        $thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true ); 
                        $image = wp_get_attachment_url( $thumbnail_id );
                        if($key == 0){
                           
                        ?>
                            <div class="cards2">
                                <a href="<?php echo get_term_link ($cat->term_id, 'product_cat');?>">
                                    <img src="<?php echo $image; ?>" class="img-fluid">
                                
                                <div class="bottom-right2 text-left">
                                    <h3 class="commit text-white mb-0"><?php echo $cat->name; ?></h3>
                                    <p class="text-white"><?php echo mb_strimwidth($cat->description, 0, 100, "..."); ?></p>
                                </div>
                                </a>
                            </div>
                        <?php }
                        if($key == 1){ 
                        ?>
                            <div class="cards2 mt-2 pt-4">
                            <a href="<?php echo get_term_link ($cat->term_id, 'product_cat');?>">
                                <img src="<?php echo $image; ?>" class="img-fluid">
                                <div class="bottom-right text-left">
                                    <h3 class="commit text-white mb-0"><?php echo $cat->name; ?></h3>
                                    <p class="text-white"><?php echo mb_strimwidth($cat->description, 0, 100, "..."); ?></p>
                                </div>
                            </a>
                            </div> 
                        <?php } 
                    } ?>
                </div>
                <?php
                foreach ($all_categories as $key => $cat) { 
                    $thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true ); 
                    $image = wp_get_attachment_url( $thumbnail_id );
                
                if($key == 2){  ?>
                <div class="col-lg-5">
                    <div class="cards2 pt-4">
                        <a href="<?php echo get_term_link ($cat->term_id, 'product_cat');?>">

                        <img src="<?php echo $image; ?>" class="img-fluid">
                        <div class="bottom-right text-left">
                            <h3 class="commit text-white mb-0"><?php echo $cat->name; ?></h3>
                            <p class="text-white pr-2"><?php echo mb_strimwidth($cat->description, 0, 100, "..."); ?></p>
                        </div>
                        </a>
                    </div>
                </div>
                <?php }  } ?>
            </div>
            <div class="row pt-4">
                <?php  
                    foreach ($all_categories as $key => $cat) { 
                    $thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true ); 
                    $image = wp_get_attachment_url( $thumbnail_id );
                        if($key != 0 && $key != 1 && $key != 2 && strtolower($cat->name) != 'others'){
                ?>



                        <div class="col-lg-12 col-md-12 col-12 pt-2">
                            <div class="cards">
                                <a href="<?php echo get_term_link ($cat->term_id, 'product_cat');?>">
                                    <img src="<?php echo $image; ?>" class="imgFull img-fluid">
                                    <div class="bottom-right text-left">
                                        <h3 class="commit text-white mb-0"><?php echo $cat->name; ?></h3>
                                        <p class="text-white"><?php echo mb_strimwidth($cat->description, 0, 100, "..."); ?></p>
                                    </div>
                                </a> 
                            </div>
                        </div>

                <?php
                        }
                    } 

                ?>
            </div>
        </div>
    </div>

    <!-- ============================================================================ -->
    <!-- <div class="footer">
    <div class="jumbotron bg-white jumbotronFooter mb-0">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6 pt-3">
                        <div class="">
                            <h6>MEN’S COLLECTION</h6>
                            <p><a href="#">Hoodies</a></p>
                            <p><a href="#">Shirts</a></p>
                            <p><a href="#">Gymwear</a></p>
                            <p><a href="#">Shorts</a></p>
                            <p><a href="#">Equipment</a></p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6 pt-3">
                        <div class="">
                            <h6>WOMEN’S COLLECTION</h6>
                            <p><a href="#">Tank Tops</a></p>
                            <p><a href="#">Shirts</a></p>
                            <p><a href="#">Sweatshirts</a></p>
                            <p><a href="#">Shops</a></p>
                            <p><a href="#">Training Gear</a></p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6 pt-3">
                        <div class="">
                            <h6> ABOUT US </h6>
                            <p><a href="#">Hoodies</a></p>
                            <p><a href="#">Shirts</a></p>
                            <p><a href="#">Gymwear</a></p>
                            <p><a href="#">Shorts</a></p>
                            <p><a href="#">Equipment</a></p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-6 pt-3">
                        <div class="">
                            <h6>ATHLETES</h6>
                            <p><a href="#">Tank Tops</a></p>
                            <p><a href="#">Shirts</a></p>
                            <p><a href="#">Sweatshirts</a></p>
                            <p><a href="#">Shops </a></p>
                            <p><a href="#">Training Gear</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   -->
    <div class="container pt-5">
        <div class="collection">
            <h4 class="entire">COLLECTION</h4>
            <h3 class="commit">BEST SELLERS</h3>
        </div>
        <div class="swiper-container container pb-5 pt-5"><br>
            <div class="swiper-wrapper">
            <?php
                $args = array(
                    'posts_per_page'   => 10,
                    'orderby'          => 'date',
                    'order'            => 'DESC',
                    'post_type'        => 'product'
                ); 

                $random_products = get_posts( $args );

                foreach ( $random_products as $post ) : setup_postdata( $post ); 
                global $product;
                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $product->id ), 'single-post-thumbnail' );
                $attachment_ids = $product->get_gallery_image_ids();
                $fullImage =  wp_get_attachment_url($attachment_ids[0], 'full') ? wp_get_attachment_url($attachment_ids[0], 'full'): $image;
            ?>
                <div class="swiper-slide">
                    <div class="row py-3">
                        <div class="col-lg-9 col-md-6 col-sm-12 col-12 pt-3">
                            <a href="<?php the_permalink(); ?>"> 
                                <div class="text-center newArrival">
                                    <img src="<?php echo $image[0]; ?>" data-swapImage="<?php echo  $fullImage; ?>" class="mx-auto img-fluid d-block swapImage">
                                    <?php if ( $product->is_on_sale() ){?>
                                        <p class="top-right text-white">SALE</p>
                                    <?php } ?>
                                </div>
                            </a>
                            <p class="commit text-center pt-2 mb-0"><?php the_title(); ?></p>
                            <p class="text-center">
                                <a href="<?php the_permalink(); ?>" class="text-danger text-center salary">
                                    <?php // echo  $product->get_price_html(); ?>
                                    <?php echo woocommerce_price($product->get_price()); ?>
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
                <?php endforeach; 
                wp_reset_postdata();
                ?>
            </div>
            <!-- Add Pagination -->
            <div class="swiper-pagination">
                <img src="<?php bloginfo('template_url'); ?>/images/next.svg">
            </div>
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
        </div><br>
	</div>

    <!---------------------------- Footer --------------------------------->
    <?php get_footer(); ?>

</body>

</html>